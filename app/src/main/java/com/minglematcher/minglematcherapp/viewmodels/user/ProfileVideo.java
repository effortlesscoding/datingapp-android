package com.minglematcher.minglematcherapp.viewmodels.user;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;

import java.io.File;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class ProfileVideo implements Parcelable {

    public class LocalData {
        public String videoPath;
        public String thumbnailPath;
        public double uploadProgress = -1;
        public double thumbnailUploadProgress = -1;
    }

    long _id;
    public String fileName;
    public String thumbnail;
    Boolean uploaded;
    public String storagePath;
    long updatedAt;
    public long createdAt;

    LocalData localData;

    public LocalData getLocalData() {
        if (localData == null) {
            if (fileName == null) {
                fileName = "";
            }
            if (thumbnail == null) {
                thumbnail = "";
            }
            localData = new LocalData();
            File fThumbnail = new File(MingleMatcherApplication.getContext().getFilesDir() +
                    VideoDownloadService.PROFILE_DIR, thumbnail);
            if (fThumbnail.exists()) {
                localData.thumbnailPath = fThumbnail.getAbsolutePath();
            }

            File fVideo = new File(MingleMatcherApplication.getContext().getFilesDir() +
                    VideoDownloadService.PROFILE_DIR, fileName);
            if (fVideo.exists()) {
                localData.videoPath = fVideo.getAbsolutePath();
            }
        }
        return localData;
    }
    public ProfileVideo(long videoId, File localVideoPath, File localThumbnailPath) {
        this._id = videoId;
        this.uploaded = false;
        if (localVideoPath != null) {
            this.getLocalData().videoPath = localVideoPath.getAbsolutePath();
        }
        if (localThumbnailPath != null) {
            this.getLocalData().thumbnailPath = localThumbnailPath.getAbsolutePath();
        }
        this.getLocalData().uploadProgress = 0.0;
        this.getLocalData().thumbnailUploadProgress = 0.0;
    }

    protected ProfileVideo(Parcel in) {
        fileName = in.readString();
        int i = in.readInt();
        if (i == 0) {
            uploaded = false;
        } else if (i == 1) {
            uploaded = true;
        }
        storagePath = in.readString();
    }

    public long getId() {
        return _id;
    }

    public String getLocalThumbnailPath() {
        if (localData == null) {
            localData = new LocalData();
        }
        if (localData.thumbnailPath == null) {
            localData.thumbnailPath = new File(MingleMatcherApplication.getContext().getFilesDir() +
                    VideoDownloadService.PROFILE_DIR, fileName).getAbsolutePath();
        }
        return localData.thumbnailPath;
    }
    public String getServerPath() {
        Log.i("PROFILE_VIDEO", storagePath + fileName);
        return storagePath + fileName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fileName);
        if (uploaded) {
            dest.writeInt(1);
        } else {
            dest.writeInt(0);
        }
        dest.writeString(storagePath);
    }

    public static final Creator<ProfileVideo> CREATOR = new Creator<ProfileVideo>() {
        @Override
        public ProfileVideo createFromParcel(Parcel in) {
            return new ProfileVideo(in);
        }

        @Override
        public ProfileVideo[] newArray(int size) {
            return new ProfileVideo[size];
        }
    };
}
