package com.minglematcher.minglematcherapp.viewmodels.proper;

import com.minglematcher.minglematcherapp.models.user.User;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class UserVM extends User<PersonalInfoVM, ProfileVM, ProfileMatchVM> {

    private static UserVM mUserData;
    private UserVM() {

    }


    /*** ***/
    public static UserVM getCurrentUser() {
        return mUserData;
    }

    public static void setUser(UserVM userData) {
        mUserData = userData;
        // Prep the model to be a view model :)
        mUserData.profile.fillEmptyProfileVideos();
    }

}
