package com.minglematcher.minglematcherapp.viewmodels.user;

/**
 * Created by AnhAcc on 2/15/2016.
 */
/**
 * Created by AnhAcc on 12/5/2015.
 */
public class User {
    // I think this should be moved to activity
    private static UserData mUserData;

    private User() {

    }
    /*** ***/
    public static UserData getCurrentUser() {
        return mUserData;
    }

    public static void setUser(UserData userData) {
        if (userData != null) {
            mUserData = userData;
            mUserData.getProfile().fillEmptyProfileVideos();
        }
        // Prep the model to be a view model :)
    }

}