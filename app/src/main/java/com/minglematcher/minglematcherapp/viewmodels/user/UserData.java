package com.minglematcher.minglematcherapp.viewmodels.user;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by AnhAcc on 2/16/2016.
 */
public class UserData {
    long _id;
    int gender;
    long facebookId;
    long reviewdUpTo;
    ProfilePersonalData profile;

    List<ProfileSnippetData> profilesToReview;
    List<ProfileMatchData> profilesMatched;

    public UserData() {
        profilesToReview = new ArrayList<>();
        profilesMatched = new ArrayList<>();
    }

    public void fillMatchesData(ProfileSnippetData[] rawProfiles) {
        for (ProfileMatchData pmd : profilesMatched) {
            for (ProfileSnippetData psd : rawProfiles) {
                if (psd.getId() == pmd.getId()) {
                    pmd.setProfile(psd);
                }
            }
        }
    }

    public boolean isMale() {
        return gender == 0;
    }

    public boolean isFemale() {
        return gender == 1;
    }

    public void addMatch(ProfileMatchData userMatchedWith) {
        profilesMatched.add(userMatchedWith);
    }

    public void setProfilesToReview(ProfileSnippetData[] profiles) {
        profilesToReview = Arrays.asList(profiles);
    }

    public long get_id() {
        return _id;
    }

    public long getFacebookId() {
        return facebookId;
    }

    public long getReviewdUpTo() {
        return reviewdUpTo;
    }

    public List<ProfileSnippetData> getProfilesToReview() {
        return profilesToReview;
    }

    public List<ProfileMatchData> getProfilesMatched() {
        return profilesMatched;
    }

    public ProfilePersonalData getProfile() {
        return profile;
    }

    public boolean isProfileComplete() {
        return (profile != null) && (profile.isComplete());
    }

     /**
     * Called only if FileNotFound exception, I guess.
     * @param psd
     */
    public void removeProfileSnippetFromReview(ProfileSnippetData psd) {
        for (int i = 0; i < profilesToReview.size(); i++) {
            if (profilesToReview.get(i).getFacebookId() == psd.getFacebookId()) {
                profilesToReview.remove(i);
                break;
            }
        }
    }
}
