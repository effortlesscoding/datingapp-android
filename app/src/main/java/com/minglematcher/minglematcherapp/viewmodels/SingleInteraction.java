package com.minglematcher.minglematcherapp.viewmodels;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class SingleInteraction implements Serializable {
    public enum Mode {
        Text, Video, Picture, Audio
    };

    private boolean isByCurrentUser;
    private Mode mode;
    private String interaction;
    private Date chatDate;
    private String mediaSource;
    private boolean inAssetsFolder;
    private String thumbnail;

    public SingleInteraction(boolean isByCurrentUser, boolean inAssetsFolder, String mediaFile) {
        this.isByCurrentUser = isByCurrentUser;
        this.mode = Mode.Video;
        this.interaction = "Delivered";
        this.inAssetsFolder = inAssetsFolder;
        this.chatDate = new Date();
        this.mediaSource= mediaFile;
    }
    public SingleInteraction(boolean isByCurrentUser, Mode mode, String interaction) {
        this.isByCurrentUser = isByCurrentUser;
        this.mode = mode;
        this.interaction = interaction;
        this.chatDate = new Date();
    }

    public String getInteraction() {
        return interaction;
    }

    public Mode getMode() {
        return mode;
    }

    public boolean isByCurrentUser() {
        return isByCurrentUser;
    }

    public String getChat() {
        return interaction;
    }

    public String getMediaSource() { return mediaSource; }

    public String getSentTimeAgo() {
        Date now = new Date();
        long duration  = now.getTime() - chatDate.getTime();

        long totalDiffSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
        long hours = TimeUnit.SECONDS.toHours(totalDiffSeconds);
        long minutes = TimeUnit.SECONDS.toMinutes(totalDiffSeconds - hours * 3600);
        long seconds = totalDiffSeconds - hours * 3600 - minutes * 60;
        String ago = "";
        if (hours > 0) {
            ago += hours + "h ";
        }
        if (minutes > 0) {
            ago += hours + "m ";
        }
        if (seconds > 0) {
            ago += seconds + "s";
        }
        return ago;
    }

    public boolean isInAssetsFolder() {
        return inAssetsFolder;
    }


}
