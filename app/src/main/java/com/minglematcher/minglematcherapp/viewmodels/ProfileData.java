package com.minglematcher.minglematcherapp.viewmodels;

import android.util.Log;

import com.minglematcher.minglematcherapp.BuildConfig;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class ProfileData implements Serializable {



    public final String KEY = "match";
    private static int sCurrentCount = 0;
    private int mId;
    private int mAge;
    private int mHeight;
    private String mName;
    private String mLocation;
    private EReligion mReligion;
    private List<String> mAboutMe;
    private List<String> mDatePreferences;
    private List<String> mResponses;
    private boolean mWillMatch;
    private String mProfilePhoto;
    private String mOccupation;
    private int mProfileVideo;
    private String mTagline;
    private List<ProfileVideoData> mProfileVideos;

    public ProfileData(int height, int profileVideo, String occupation, String name, EReligion religion,
                       String[] aboutMe, String[] idealPartner, String tagline,
                       String location, int mAge, boolean willMatch, String[] responses, String profilePhoto,
                       List<ProfileVideoData> videos) {
        this.mHeight = height;
        this.mName = name;
        this.mProfileVideos = videos;
        this.mReligion = religion;
        this.mAboutMe = Arrays.asList(aboutMe);
        this.mDatePreferences = Arrays.asList(idealPartner);
        this.mLocation = location;
        this.mId = sCurrentCount++;
        this.mAge = mAge;
        this.mWillMatch = willMatch;
        this.mResponses = Arrays.asList(responses);
        this.mProfilePhoto = profilePhoto;
        this.mOccupation = occupation;
        this.mProfileVideo = profileVideo;
        this.mTagline = tagline;
    }

    public List<ProfileVideoData> getProfileVideos() {
        return mProfileVideos;
    }

    public String getTagline() {
        return mTagline;
    }

    public int getId() {
        return mId;
    }

    public List<String> getAbout() {
        return mAboutMe;
    }

    public List<String> getDatePreferences() {
        return mDatePreferences;
    }

    public int getHeight() {
        return mHeight;
    }

    public String getVideo() {
//        return "https://00e9e64bac65a523c85c1f9f485acf1ac2e21f7f6a3e9eba49-apidata.googleusercontent.com/download/storage/v1_internal/b/snapflirt-1203/o/reply_1459933908104.mp4?qk=AD5uMEvVBHs4LCOq1Rmyl9W7W74BENKA5wZp5VXJqFsGac3U8J6kHW_GnJa9w0s-zqYEDD9SgkOaWGdzH04bhPJFF9Lsez9cXNeJezrQGPAk_seYZnETFo6IZd67BWD4ivt7OhMm8Bu5Ax-s0k2yI2IaftWZ0JNh40N59Mo7Kv3HxyGDm08YDt_aXS9tBhSpKM23Au2gYKbP9I5ApRtPOcUsLdi58mek_SZk0JyBB5fTeJeum2Izzh6IkKJnuCfJzDPiZOll29VeektMSEyuXpSObc6fyfkmfN6vsoPLiGy4_uc6fazlmSOszCNV8cdDSASUtOav1aXvEWPWyS0GVV9oq38lyQQ5O3Y_ttKiROQ-AFK359Xum6__fYTb2KLpz42SbxrBBxJTxvkP1JL1GxUALmE7vOVNSA08MkUtS4_0xWKp1-NS7SsyBHd356mw3Qzj0VDqbLBqdEt78DoeDrBH5YdTUPaJGYOrrUelfDKJJHNA3RJvaifCaZ18g762HeQ5qjXspJQ3d0M1iY6y80Lqfi3itZ_nItKttzCekI0-dAA1ce3wPf-Ps4pbHEEnRGwJ-dSlXXwBvzcuYb-2EWC_r38_OHGetj2cP2BYx5gvz_Q2aWiisEX3zHMdJ-3-pWBsiYrtJsBG7VCMxfZ0CLwPEWubTNyScPs-gV4SvKPdkd66Gsms7ZhNcLo2d34TE5ik0PRbzSRg9nLgLfvdwIzi49AtcJ58zG4hb4wYAd3c1jR4nLL-ya5XlhK4HSX0lvmyfc7fn9lihGbFC1kBGFMB_mEyTVF_eA99qUAM8UKBzsYadd-GB2XAOb392-pyl1ffWqXQLkuIzDS50GvIzGQ8oz8FB1kcNQ";
        return "android.resource://" + BuildConfig.APPLICATION_ID + "/" + mProfileVideo;
    }

    public EReligion getReligion() {
        return mReligion;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getProfilePhoto() {
        Log.i("IMAGE_LOAD", mProfilePhoto);
        return mProfilePhoto;
    }

    public String getName() {
        return mName;
    }

    public int getAge() {
        return mAge;
    }

    public boolean isWillMatch() {
        return mWillMatch;
    }

    public String getOccupation() {
        return mOccupation;
    }

    public String getMockResponse(int i) {
        if (this.mResponses != null && this.mResponses.size() >=  i + 1) {
            return this.mResponses.get(i);
        } else {
            return null;
        }
    }
}


