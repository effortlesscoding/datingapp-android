package com.minglematcher.minglematcherapp.viewmodels.proper;

import android.os.Parcel;
import android.os.Parcelable;

import com.minglematcher.minglematcherapp.models.user.ProfileVideo;

import java.io.File;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ProfileVideoVM extends ProfileVideo implements Parcelable {

    public class LocalMetadata {
        public String videoPath;
        public String thumbnailPath;
        public double uploadProgress = -1;
        public double thumbnailUploadProgress = -1;
    }

    public ProfileVideoVM(long id) {
        this._id = id;
    }

    public LocalMetadata localData = new LocalMetadata();

    public ProfileVideoVM(long videoId, File localVideoPath, File localThumbnailPath) {
        this._id = videoId;
        this.uploaded = false;
        if (localVideoPath != null) {
            this.localData.videoPath = localVideoPath.getAbsolutePath();
        }
        if (localThumbnailPath != null) {
            this.localData.thumbnailPath = localThumbnailPath.getAbsolutePath();
        }
        this.localData.uploadProgress = 0.0;
        this.localData.thumbnailUploadProgress = 0.0;
    }

    protected ProfileVideoVM(Parcel in) {
    }

    public static final Creator<ProfileVideoVM> CREATOR = new Creator<ProfileVideoVM>() {
        @Override
        public ProfileVideoVM createFromParcel(Parcel in) {
            return new ProfileVideoVM(in);
        }

        @Override
        public ProfileVideoVM[] newArray(int size) {
            return new ProfileVideoVM[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }



}
