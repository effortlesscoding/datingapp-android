package com.minglematcher.minglematcherapp.viewmodels.user;

/**
 * Created by AnhAcc on 4/9/2016.
 */
public class UserProfileData {
    String firstName;
    String lastName;
    String email;
    String videoPath;
}
