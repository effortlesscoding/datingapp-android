package com.minglematcher.minglematcherapp.viewmodels.user;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.activities.user.actions.ActionProfileVideoFinishConfirmed;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 4/9/2016.
 */
public class ProfilePersonalData implements Serializable, Parcelable {
    public static int MAX_PROFILE_VIDEOS_TO_SHOW = 6;
    String firstName;
    String lastName;
    String email;
    List<ProfileVideo> videos = new ArrayList<>();
    int age;

    public void confirmProfileVideo(ActionProfileVideoFinishConfirmed action) {
        for (int i = 0; i < videos.size(); i++) {
            if (videos.get(i).getId() == action.getData()._id) {
                videos.get(i).uploaded = action.getData().uploaded;
                videos.get(i).fileName = action.getData().fileName;
                videos.get(i).storagePath = action.getData().storagePath;
                videos.get(i).thumbnail = action.getData().thumbnail;
                videos.get(i).updatedAt = action.getData().updatedAt;
                break;
            }
        }
    }

    public boolean isComplete() {
        boolean atLeastOneVideoUploaded = false;
        for (ProfileVideo pv : videos) {
            if (pv.uploaded) {
                atLeastOneVideoUploaded = true;
            }
        }
        return (videos.size() > 0) && atLeastOneVideoUploaded;
    }
    public void fillEmptyProfileVideos() {
        if (videos.size() < MAX_PROFILE_VIDEOS_TO_SHOW) {
            for (int i = videos.size(); i < MAX_PROFILE_VIDEOS_TO_SHOW; i++) {
                videos.add(new ProfileVideo(i, null, null));
            }
        }
    }

    public void addProfileVideo(long id, File videoPath, File thumbnailPath) {
        ProfileVideo pv = new ProfileVideo(id, videoPath, thumbnailPath);
    }
    public String getName() {
        return firstName + " " + lastName;
    }

    public List<ProfileVideo> getVideos() {
        return videos;
    }


    public String getVideoServerPath() {
        if (videos.size() > 0) {
            return videos.get(0).getServerPath();
        } else {
            return null;
        }
    }

    public String getVideoFilePath() {
        if (videos != null && videos.size() > 0) {
            String path = MingleMatcherApplication.getContext().getFilesDir() +
                    File.separator + videos.get(0).getServerPath();
            Log.i("PROFILE_DATA", "Path is " + path);
            return path;
        } else {
            return null;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeList(videos);
        dest.writeInt(age);
    }

    public ProfilePersonalData() {
    }
    protected ProfilePersonalData(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        Object[] objects = in.readArray(ProfileVideo[].class.getClassLoader());
        if (objects != null) {
            videos = new ArrayList<>();
            for (int i = 0; i < objects.length; i++) {
                if (objects[i] instanceof ProfileVideo) {
                    videos.add((ProfileVideo)objects[i]);
                }
            }
        } else {
            videos = new ArrayList<>();
        }
        age = in.readInt();
    }

    public static final Creator<ProfilePersonalData> CREATOR = new Creator<ProfilePersonalData>() {
        @Override
        public ProfilePersonalData createFromParcel(Parcel in) {
            return new ProfilePersonalData(in);
        }

        @Override
        public ProfilePersonalData[] newArray(int size) {
            return new ProfilePersonalData[size];
        }
    };
}
