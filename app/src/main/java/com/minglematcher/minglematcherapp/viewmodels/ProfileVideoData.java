package com.minglematcher.minglematcherapp.viewmodels;

import android.net.Uri;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by AnhAcc on 3/28/2016.
 */
public class ProfileVideoData implements Serializable {

    private String mVideoUri;
    private String mShortDescription;
    private String mDescription;
    private Date mUploadDate;

    public ProfileVideoData(String uri, String sDesc, String desc) {
        mVideoUri = uri;
        mShortDescription = sDesc;
        mDescription = desc;
        mUploadDate = null;
    }

    public Uri getVideoUri() {
        return Uri.parse(mVideoUri);
    }

    public String getShortDescription() {
        return mShortDescription;
    }

    public String getDescription() {
        return mDescription;
    }

    public Date getUploadDate() {
        return mUploadDate;
    }

}
