package com.minglematcher.minglematcherapp.viewmodels.proper;

import android.content.Context;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.models.user.Profile;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;

import java.io.File;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ProfileVM extends Profile<PersonalInfoVM> {
    public ProfileVM() {

    }

    /**
     * Checks if the video has been pre-downloaded
     * @return
     */
    public boolean areProfileVideosLocallyAvailable() {
        String path = getFirstProfileVideoPath();
        if (path != null) {
            return new File(path).exists();
        } else {
            return false;
        }
    }

    public String getFirstProfileVideoPath() {
        if (this.profile.videos.size() == 0) return null;
        String directoryPath = MingleMatcherApplication.getContext().getFilesDir()
                + VideoDownloadService.PROFILE_DIR;
        return this.profile.videos.get(0).localData.videoPath;
    }


}
