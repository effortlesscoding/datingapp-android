package com.minglematcher.minglematcherapp.viewmodels;

import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class MatchesHistory {
    private static MatchesHistory instance;


    private List<MatchInteractions> matches;
    private List<MatchInteractions> activityHistory;

    private MatchesHistory() {
        matches = new ArrayList<MatchInteractions>();
        activityHistory = new ArrayList<MatchInteractions>();
    }

    public List<MatchInteractions> getMatches() {
        return matches;
    }

    public MatchInteractions findInteractionById(int id) {
        for (MatchInteractions m : matches) {
            if (m.getMatch().getFacebookId() == id) {
                return m;
            }
        }
        return null;
    }

    public List<MatchInteractions> getActivityHistory() {
        return activityHistory;
    }

    /**
     * Use it when you "like" somebody, but it is not an immediate "Match"
     * @param md
     */
    public void addActivity(ProfileSnippetData md) {
        activityHistory.add(new MatchInteractions(md, false));
    }

    /**
     * Use it when you match immediately
     * @param md
     */
    public void addMatch(ProfileSnippetData md) {
        MatchInteractions mi = new MatchInteractions(md, true);
        matches.add(mi);
        activityHistory.add(mi);
    }


    public static MatchesHistory getInstance() {
        if (instance == null) {
            instance = new MatchesHistory();
        }
        return instance;
    }
}

