package com.minglematcher.minglematcherapp.viewmodels.user;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 4/16/2016.
 */
public class Conversation {

    private List<ConversationMessage> mMessages;
    private DateTime mCreationDate;
    public Conversation() {
        mMessages = new ArrayList<>();
        mCreationDate = new DateTime();
    }

    public Duration getCreatedAgo() {
        DateTime currentTime = new DateTime();

        Duration duration = new Duration(mCreationDate, currentTime);
        //Interval interval = new Interval(new Date(), mCreationDate);
        return duration;
    }

    public DateTime getCreationDate() {
        return mCreationDate;
    }

    public List<ConversationMessage> getMessages() {
        return mMessages;
    }
}
