package com.minglematcher.minglematcherapp.viewmodels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by AnhAcc on 12/28/2015.
 */
public class PhotoAlbum extends PhotoItem {
    private List<PhotoItem> photos;
    public static final String CHOSEN_ALBUM_POSITION = "cap";

    public PhotoAlbum(String title, String photoPath, PhotoItem[] photos) {
        super(title, photoPath);
        this.photos = new ArrayList<PhotoItem>(Arrays.asList(photos));
    }

    public PhotoItem[] getPhotosArray() {
        PhotoItem[] pis = new PhotoItem[photos.size()];
        for (int i = 0; i < photos.size(); i++) {
            pis[i] = photos.get(i);
        }
        return pis;
    }

    public List<PhotoItem> getPhotos() {
        return photos;
    }
}
