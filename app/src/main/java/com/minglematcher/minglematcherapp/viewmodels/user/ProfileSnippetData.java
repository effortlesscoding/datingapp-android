package com.minglematcher.minglematcherapp.viewmodels.user;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;

import java.io.File;
import java.io.Serializable;

/**
 * Created by AnhAcc on 4/9/2016.
 */
public class ProfileSnippetData implements Serializable, Parcelable {
    public static final int GENDER_MALE = 0;
    public static final int GENDER_FEMALE = 1;
    long _id;
    long facebookId;
    int gender;
    ProfilePersonalData profile;
    long conversationId;
    long updatedAt;
    long createdAt;
    private Conversation mConversationMessages;

    public Conversation getConversationMessages() {
        if (mConversationMessages == null) {
            mConversationMessages = new Conversation();
        }
        return mConversationMessages;
    }

    public long getId() {
        return _id;
    }

    public String getProfilePhoto() {
        return "female_user.jpg";
    }

    public String getFileName() {
        return facebookId + ".mp4";
    }

    public long getFacebookId() {
        return facebookId;
    }

    public ProfilePersonalData getProfile() {
        return profile;
    }
    /**
     * Checks if the video has been pre-downloaded
     * @return
     */
    public boolean isVideoLocallyDownloaded(Context c) {
        File f = new File(getLocalVideoPath());
        return f.exists();
    }

    private String getLocalVideoFilename() {
        return facebookId + ".mp4";
    }

    public String getLocalVideoPath() {
        File f = new File(MingleMatcherApplication.getContext().getFilesDir() + VideoDownloadService.PROFILE_DIR,
                getLocalVideoFilename());
        return f.getAbsolutePath();
    }

    public boolean willMatch() {
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(facebookId);
        dest.writeInt(gender);
        dest.writeParcelable(profile, flags);
    }

    public ProfileSnippetData(long id) {
        this._id = id;
    }

    protected ProfileSnippetData(Parcel in) {
        facebookId = in.readLong();
        gender = in.readInt();
        profile = in.readParcelable(ProfilePersonalData.class.getClassLoader());
    }

    public static final Creator<ProfileSnippetData> CREATOR = new Creator<ProfileSnippetData>() {
        @Override
        public ProfileSnippetData createFromParcel(Parcel in) {
            return new ProfileSnippetData(in);
        }

        @Override
        public ProfileSnippetData[] newArray(int size) {
            return new ProfileSnippetData[size];
        }
    };
}
