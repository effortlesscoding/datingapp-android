package com.minglematcher.minglematcherapp.viewmodels;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public enum EReligion {
    Christian, Atheist, Buddhist, Muslim
}
