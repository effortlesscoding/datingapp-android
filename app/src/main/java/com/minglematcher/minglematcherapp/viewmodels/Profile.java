package com.minglematcher.minglematcherapp.viewmodels;

/**
 * Created by AnhAcc on 2/17/2016.
 */
public class Profile {
    private String mFirstName;
    private String mLastName;
    private String mOccupation;
    private String mTagline;
    private String mVideoUrl;
    private int mAge;

    public Profile(String fName, String lName, String tagline, String occupation,
                   String videoUrl, int age) {
        this.mFirstName = fName;
        this.mLastName = lName;
        this.mTagline = tagline;
        this.mOccupation = occupation;
        this.mVideoUrl = videoUrl;
        this.mAge = age;
    }


    public int getAge() {
        return mAge;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getOccupation() {
        return mOccupation;
    }

    public String getTagline() {
        return mTagline;
    }

    public String getVideoUrl() {
        return mVideoUrl;
    }
}
