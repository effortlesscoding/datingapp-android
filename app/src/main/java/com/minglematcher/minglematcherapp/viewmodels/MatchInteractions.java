package com.minglematcher.minglematcherapp.viewmodels;

import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;
import com.minglematcher.minglematcherapp.viewmodels.user.User;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class MatchInteractions implements Serializable {
    public static class MatchException extends Exception {
        public MatchException(String message) {
            super(message);
        }
    }

    public enum State {
        Liked, Matched, Bantered, InRapport
    };
    private ProfileSnippetData withWhom;
    private State state;
    private List<SingleInteraction> interactions;

    public MatchInteractions(ProfileSnippetData md, boolean isMatch) {
        this.withWhom = md;
        this.state = State.Liked;
        this.interactions = new ArrayList<SingleInteraction>();
        if (isMatch) {
            state = State.Matched;
        } else {
            state = State.Liked;
        }
    }

    public void sendText(String text) throws MatchException {
        if (state == State.InRapport) {
            interactions.add(new SingleInteraction(true, SingleInteraction.Mode.Text, text));
        } else {
            throw new MatchException("No texts until rapport");
        }
    }

    public void receiveText(String text) throws MatchException {
        if (state == State.InRapport) {
            interactions.add(new SingleInteraction(false, SingleInteraction.Mode.Text, text));
        } else {
            throw new MatchException("No texts until rapport");
        }
    }

    public void sendVideoToMatch(String videoUri) throws MatchException {
        if (state != State.Liked) {
            UserData user = User.getCurrentUser();
            if (user.isMale()) {
                if (this.interactions.size() == 0) {
                    setBantered();
                }
                this.interactions.add(new SingleInteraction(true, SingleInteraction.Mode.Video, videoUri));
            } else if (user.isFemale()) {
                if (this.interactions.size() == 0) {
                    throw new MatchException("A girl cannot make the first move.");
                } else if (this.interactions.size() == 1) {
                    setInRapport();
                }
                this.interactions.add(new SingleInteraction(true, SingleInteraction.Mode.Video, videoUri));
            }
        } else {
            throw new MatchException("You cannot interact with somebody unless there is a match.");
        }
    }

    public void receiveVideo(String videoUri) throws MatchException {
        if (state != State.Liked) {
            UserData user = User.getCurrentUser();
            if (user.isMale()) {
                if (interactions.size() == 0) {
                    throw new MatchException("A girl cannot make the first move.");
                } else if (interactions.size() == 1) {
                    setInRapport();
                }
                interactions.add(new SingleInteraction(false, SingleInteraction.Mode.Video, videoUri));
            } else if (user.isFemale()) {
                if (interactions.size() == 0) {
                    setBantered();
                }
                interactions.add(new SingleInteraction(false, SingleInteraction.Mode.Video, videoUri));
            }
        } else {
            throw new MatchException("You cannot talk if not matched.");
        }
    }

    public void setMatched() {
        if (this.state == State.Liked) {
            this.state = State.Matched;
        }
    }

    public void setBantered() {
        if (this.state == State.Matched) {
            this.state = State.Bantered;
        }
    }

    public void setInRapport() {
        if (this.state == State.Bantered) {
            this.state = State.InRapport;
        }
    }

    public ProfileSnippetData getMatch() {
        return withWhom;
    }

    public State getState() {
        return state;
    }

    public List<SingleInteraction> getInteractions() { return interactions; }

    public void add(SingleInteraction si) {
        interactions.add(si);
        if (state == State.Matched) {
            setBantered();
        } else if (state == State.Bantered) {
            setInRapport();
        }
    }

}
