package com.minglematcher.minglematcherapp.viewmodels.user;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.joda.time.DateTime;

import java.io.File;

/**
 * Created by AnhAcc on 5/1/2016.
 */
public class ProfileMatchData {
    long _id;
    long updatedAt;
    long createdAt;
    String conversationId;
    Conversation mConversation;
    ProfileSnippetData profile;

    public void setProfile(ProfileSnippetData ppd) {
        profile = ppd;
    }

    public long getId() {
        return _id;
    }

    // if profile is null, then return NULL :P
    public ProfileSnippetData getProfile() {
        return profile;
    }
    /**
     * This is used for conversations tab.
     * @return
     */
    public File getFirstProfileLocal() {
        if (profile == null) { return null;}
        ProfilePersonalData ppd = profile.getProfile();
        if (ppd == null) { return null; }
        if (ppd.videos.size() == 0) { return null; }
        File thumbnail = new File(ppd.videos.get(0).getLocalThumbnailPath());
        if (!thumbnail.exists()) { return null; }
        return thumbnail;
    }

    public void setConversation(Conversation c) {
        mConversation = c;
    }

    public DateTime getMatchDate() {
        DateTime _startDate = new DateTime(createdAt * 1000L);
        return _startDate;
    }
}
