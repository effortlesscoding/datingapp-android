package com.minglematcher.minglematcherapp.viewmodels;

/**
 * Created by AnhAcc on 2/15/2016.
 */
public class PhotoItem {

    protected String title;
    protected String photo;
    public static final String CHOSEN_ITEM_POSITION = "cip";

    public PhotoItem(String title, String photo) {
        this.title = title;
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public String getPhotoPath() {
        return photo;
    }

}

