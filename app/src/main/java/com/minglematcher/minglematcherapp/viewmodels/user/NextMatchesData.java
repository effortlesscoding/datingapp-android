package com.minglematcher.minglematcherapp.viewmodels.user;

/**
 * Created by AnhAcc on 2/17/2016.
 */
public class NextMatchesData {
    String name;
    String surname;
    String occupation;
    int age;
    String location;
    String photo;
    String videoUrl;
    String tagline;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getOccupation() {
        return occupation;
    }

    public int getAge() {
        return age;
    }

    public String getLocation() {
        return location;
    }

    public String getPhoto() {
        return photo;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getTagline() {
        return tagline;
    }

    public String getGender() {
        return gender;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public boolean isDrinking() {
        return drinking;
    }

    String gender;
    boolean smoking;
    boolean drinking;
}
