package com.minglematcher.minglematcherapp.viewmodels.proper;

import android.os.Parcel;
import android.os.Parcelable;

import com.minglematcher.minglematcherapp.models.user.PersonalInformation;

import java.io.Serializable;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class PersonalInfoVM extends PersonalInformation<ProfileVideoVM> implements Serializable, Parcelable {
    public static int MAX_PROFILE_VIDEOS_TO_SHOW = 6;


    public void fillEmptyProfileVideos() {
        if (videos.size() < MAX_PROFILE_VIDEOS_TO_SHOW) {
            for (int i = videos.size(); i < MAX_PROFILE_VIDEOS_TO_SHOW; i++) {
                videos.add(new ProfileVideoVM(i));
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected PersonalInfoVM(Parcel in) {
    }

    public static final Creator<PersonalInfoVM> CREATOR = new Creator<PersonalInfoVM>() {
        @Override
        public PersonalInfoVM createFromParcel(Parcel in) {
            return new PersonalInfoVM(in);
        }

        @Override
        public PersonalInfoVM[] newArray(int size) {
            return new PersonalInfoVM[size];
        }
    };
}
