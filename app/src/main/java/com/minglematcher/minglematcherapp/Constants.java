package com.minglematcher.minglematcherapp;

/**
 * Created by AnhAcc on 3/25/2016.
 */
public class Constants {

    public static class AppSettings {
        public static final int MAX_DOWNLOAD_AT_A_TIME = 3;
        public static final String API_HOST = "http://ec2-52-63-212-198.ap-southeast-2.compute.amazonaws.com:3000";
        //public static final String API_HOST = "http://10.2.15.12:3000";
    }

    public static final String keyProfileData = "PROFILE_DATA";
    public static final int tagVideoProfileLineItem = 0;

    public static final int activityResultVideoRecordingComplete = 88;

    public static final int intentRequestCodeRecordVideo = 90;

    public static final String bundleKeyVideoFilePath = "1";

    public static final String sharedPreferencesName = "savedPreferences";
    public static final String sharedPreferencesKeyAccessToken = "accessToken";
}
