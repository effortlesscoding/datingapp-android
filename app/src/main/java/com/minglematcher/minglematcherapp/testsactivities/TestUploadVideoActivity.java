package com.minglematcher.minglematcherapp.testsactivities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.Constants;
import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.user.UserProfileVideoRecordingActivity;
import com.minglematcher.minglematcherapp.services.ServerApiService;
import com.minglematcher.minglematcherapp.services.requests.StartVideoUploadRequest;
import com.minglematcher.minglematcherapp.services.responses.StartVideoUploadResponse;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhAcc on 3/26/2016.
 */
public class TestUploadVideoActivity extends AppCompatActivity { //implements IGoogleCloudServiceDelegate {
    private String TAG = "TEST_YOUTUBE_UPLOAD";
    private TextView mStatusText;
    private TextView mUploadStatusText;
    private Button mRecordButton;
    private Button mUploadButton;
    private String mFilePath;
/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        Log.i(TAG, "RESULT_REQUEST: " + requestCode);
        Log.i(TAG, "RESULT_CODE" + resultCode);
        if (requestCode == Constants.intentRequestCodeRecordVideo &&
                resultCode == Constants.activityResultVideoRecordingComplete) {
            String fileName = (String)data.getExtras().get(Constants.bundleKeyVideoFilePath);
            Log.i(TAG, "We got a video file recorded at : " + fileName);
            mStatusText.setText("File is at : " + fileName);
            // There is a bug that if your Android is plugged into a USB, it will not save a new file right away
            mFilePath = fileName;
        }
    }

    private File mFile = null;
    private void uploadVideo() {
        // RISK - We will not be able to "Stream" the video :)
        // Step 1. Get upload Url (ask the server for it)
        if (mFilePath != null) {
            try {
                Log.i(TAG, "Opening a file again at : " + mFilePath);
                mFile = new File(mFilePath);
                if (mFile.exists()) {
                    ServerApiService apiService = ServerApiService.getInstance();
                    StartVideoUploadRequest request = new StartVideoUploadRequest(mFile.getName(), "video/mp4", mFile.length());
                    mUploadStatusText.setText("Getting upload url...");
                    apiService.startVideoUpload(request, new Callback() {
                        @Override
                        public void onResponse(Call call, Response response) {
                            if (response.body() instanceof StartVideoUploadResponse) {
                                StartVideoUploadResponse vidResponse = (StartVideoUploadResponse) response.body();
                                String location = vidResponse.getData().getLocation();
                                Log.i("VIDEO_UPLOAD", "Location is: " + location);
                                GoogleCloudService service = GoogleCloudService.getInstance();
                                mUploadStatusText.setText("Upload started to :" + location);
                                service.uploadVideo(location, mFile, TestUploadVideoActivity.this);
                            } else {
                                mUploadStatusText.setText("Getting upload url did not finish well.");
                            }
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {
                            t.printStackTrace();
                            mUploadStatusText.setText("Getting upload url failed.");
                        }
                    });
                } else {
                    Log.e(TAG, "The file DOES NOT exist!" + mFilePath);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_uploadyoutubeactivity);
        mStatusText = (TextView) findViewById(R.id.atyProgress);
        mUploadStatusText = (TextView) findViewById(R.id.atyUploadProgress);
        mRecordButton = (Button)findViewById(R.id.atyRecordButton);
        mUploadButton = (Button)findViewById(R.id.atyUploadButton);
        mFilePath = Environment.getExternalStorageDirectory() + File.separator + "dating" + File.separator + "reply_1459054038484.mp4";
        bindUI();
    }

    private void bindUI() {
        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestUploadVideoActivity.this, UserProfileVideoRecordingActivity.class);
                startActivityForResult(intent, Constants.intentRequestCodeRecordVideo);
            }
        });

        mUploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Initialise a service to upload a Youtube video ?!
                if (mFilePath != null) {
                    uploadVideo();
                }
            }
        });
    }

    @Override
    public void onChunkReceived(double percent) {
        final double p = percent;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mUploadStatusText.setText("Upload " + (p * 100) + "%");
            }
        });
    }

    @Override
    public void onFailure() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mUploadStatusText.setText("Upload failed.");
            }
        });
    }

    @Override
    public void onSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mUploadStatusText.setText("Upload successfully finished.");
            }
        });
    }*/
}
