package com.minglematcher.minglematcherapp.testsactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.ui.CircularProgressBar;

/**
 * Created by AnhAcc on 4/25/2016.
 */
public class TestPieProgressActivity extends AppCompatActivity {

  //  private ImageView mProgressView;
    //private ImageView mImageView;
    private Button mProgressButton;
    private CircularProgressBar mPieProgress;
    private int progress = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_pieprogress);
        mPieProgress = (CircularProgressBar) findViewById(R.id.tppPieChart);
        mProgressButton = (Button) findViewById(R.id.tppPercentageButton);
        mProgressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPieProgress.setProgress(progress);
                progress = progress + 10;
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {

        }
    }
}
