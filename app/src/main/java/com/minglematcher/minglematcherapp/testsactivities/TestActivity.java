package com.minglematcher.minglematcherapp.testsactivities;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.VideoView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.ui.VideoTextureView;

/**
 * Created by AnhAcc on 2/17/2016.
 */
public class TestActivity extends Activity {
    private VideoView mVideoView;
    private VideoTextureView mVtv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_video_activity);
        mVtv = (VideoTextureView)findViewById(R.id.tvVideo);
        mVideoView = (VideoView) findViewById(R.id.tvVideoView);
        playVideo();
    }

    private void playVideo() {
        String url = "https://00e9e64baca46721972b8fda49b46682511c12d167018a0089-apidata.googleusercontent.com/download/storage/v1_internal/b/snapflirt-1203/o/mockmales%2Fdan_profile.mp4?qk=AD5uMEuXvUDGzwYU2fVdztJHJZ0iLJgLhvJWH6G77d6bi4E0YO-jxRuOHTuRCwECQaV5meVsLMFrhWsAzETp20jgtenQymOdxjfX76cJC0xw7RACBai7wwPeuVsbD-1FiwcoXdZDUagJx3RD0hYc7Vp2GmidE9Z0SWAKowDomyTZ-PdgfzPiMb7lomFNK6IRlP9RmaGJigxVJqsP4k9vlFcQowiv8d3B2v9cpnz4ju-7LVNYfg3wk78B17cmCMQabQXf8gf51UXgDOdvOPr5YwlmJi39wedaV-RViJnHeApWI3m-chT1t-lrrXNPsC0h-oyHdliUBe_q4M6hHr0MoCPBgGnTXvefX1LP3vsAbjPOtlmYNXaJB91AyjaWDvGl3Hr1cpO1FrvOsFVWkdssELHw9syQshe62Gk0NPSWMqUOtR80PzFM-H6J3xTV_4kX7NHPBIAE43i3PUvwJdSApNSl3EgvyiqRt26AkYHieZ_8f2wTthzAECSOEwDxn4eCwxhSk5yXQr3tYzaE-Ci4dk95MiMS_xWczuwU_Yk6xORehvTZJzTuNIXasODxTXXlRQuOsEjCWYLOXw4XgCDYGma4yfT_aZ4twrwN2xX02WoAkXA2BxNXoz_0abbsPyROvM56mTVDZY4UBY4DdhmJ14LQuVS025n3G2e54xoI9CGXjxl9TwMkUSlb1YL0eHgXqAndubZPrj5Ye7cgJ3ru9H-lgqCm8YBe1lnSiLz9YP6G-knzknrrHBM3jlWSatQhxCFbWbm-l1bjHK5nL1kJQIR44D6BS-0XeyDDi46Fsyl8awBaKqcYfBGScTWfG0pSgC2pASrIo-YY_ZLnpUDShj-9g_uW1L1Mdg";
        mVtv.initVideo(url);
        /*mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                //mVideoView.start();
            }
        });
        mVideoView.setVideoURI(Uri.parse(url));
        mVideoView.start();
        mVideoView.setRotation(75);*/
    }
}
