package com.minglematcher.minglematcherapp.testsactivities;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.VideoView;

import com.minglematcher.minglematcherapp.R;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by AnhAcc on 2/18/2016.
 */
public class TestDownloadActivity extends Activity {
    private VideoView mVideoView;
    private class DownloadAsyncTask extends AsyncTask<String, Integer, Boolean> {
        private Context mc;
        private String mFileAbs;
        public DownloadAsyncTask(Context c) {
            this.mc = c;
        }
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                String fileName = params[1];
                URL url = new URL(params[0]); //you can write here any link
                File file = new File(mc.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
                long startTime = System.currentTimeMillis();
                mFileAbs = file.getAbsolutePath();
                Log.d("DownloadManager", "download to : " + file.getAbsolutePath());
                Log.d("DownloadManager", "download begining");
                Log.d("DownloadManager", "download url:" + url);
                Log.d("DownloadManager", "downloaded file name:" + fileName);
                        /* Open a connection to that URL. */
                URLConnection ucon = url.openConnection();
                String contentLengthStr = ucon.getHeaderField("content-length");
                Log.d("DownloadManager", contentLengthStr);
                        /*
                         * Define InputStreams to read from the URLConnection.
                         */
                InputStream is = ucon.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);

            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                //We create an array of bytes
                byte[] data = new byte[Integer.parseInt(contentLengthStr)];
                int current = 0;

                while ((current = bis.read(data,0,data.length)) != -1){
                    buffer.write(data,0,current);
                }

                FileOutputStream fos = new FileOutputStream(file);
                fos.write(buffer.toByteArray());
                fos.close();

                Log.d("DownloadManager", "download ready in"
                        + ((System.currentTimeMillis() - startTime) / 1000)
                        + " sec");
                return true;
            } catch (IOException e) {
                Log.e("DownloadManager", "Error: " + e);
            }
            return false;
        }


        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Log.d("TESTDOWNLOADACTIVITY!", "DONE! " + mFileAbs);
            mVideoView.setVideoURI(Uri.parse(mFileAbs));
            mVideoView.start();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_download_activity);
        mVideoView = (VideoView) findViewById(R.id.tdVideo);
        DownloadAsyncTask task = new DownloadAsyncTask(this);
        String url = "https://www.googleapis.com/download/storage/v1/b/snapflirt-1203/o/mockmales%2Fbrandon_profile.mp4?generation=1455628917733000&alt=media";
        task.execute(url, "test.mp4");
    }

    public void DownloadFromUrl(String fileName) {  //this is the downloader method


    }
}
