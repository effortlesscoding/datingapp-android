package com.minglematcher.minglematcherapp.testsactivities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;
import com.minglematcher.minglematcherapp.services.parameters.VideoDownloadParams;

import java.io.File;

/**
 * Created by AnhAcc on 4/9/2016.
 */
public class TestPredownloadVideosActivity extends Activity {
    private ImageView mImageOne;
    private ImageView mImageTwo;
    private class DownloadReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String string = bundle.getString(VideoDownloadService.KEY_PROFILE_DOWNLOAD_PARAMS);
                int resultCode = bundle.getInt(VideoDownloadService.RESULT);
                if (resultCode == RESULT_OK) {
                    Toast.makeText(TestPredownloadVideosActivity.this,
                            "Download complete. Download URI: " + string,
                            Toast.LENGTH_LONG).show();
                    setImage(string);
                } else {
                    Toast.makeText(TestPredownloadVideosActivity.this, "Download failed",
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    private BroadcastReceiver mReceiver;


    private void setImage(String path) {
        File imgFile = new File(path);
        Log.i("VIDEO_DOWNLOAD", "Checking image at : " + path);
        if(imgFile.exists()){
            long fileSize = imgFile.length();
            Log.i("VIDEO_DOWNLOAD", "The image size : " + fileSize);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            if (bitmap != null) {
                if (mImageOne.getDrawable() == null) {
                    mImageOne.setImageBitmap(bitmap);
                } else {
                    mImageTwo.setImageBitmap(bitmap);
                }
            } else {
                Log.i("VIDEO_DOWNLOAD", "The image is null.");
            }

        } else {
            Log.i("VIDEO_DOWNLOAD", "File does not exist.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_video_download_activity);
        File dir = new File(getFilesDir(), "profiles");
        if (dir.exists()) {
            File[] files = dir.listFiles();
            Log.i("VIDEO_DOWNLOAD", "FILES THAT EXIST:");
            for (int k = 0; k < files.length; k++) {
                File f = files[k];
                Log.i("VIDEO_DOWNLOAD", f.getAbsolutePath());
            }
        }
        // Create mock profiles urls
        mImageTwo = (ImageView)findViewById(R.id.testDownloadVideoImage1);
        mImageOne = (ImageView)findViewById(R.id.testDownloadVideoImage2);
        VideoDownloadParams[] profileUrls = new VideoDownloadParams[] {
                //new VideoDownloadParams("http://sites.psu.edu/siowfa15/wp-content/uploads/sites/29639/2015/10/cat.jpg", "cat.jpg"),
                //new VideoDownloadParams("http://www.happydogbaltimore.com/communities/6/004/009/138/266/images/4548560901_294x301.png", "4548560901_294x301.png")

        };
        mReceiver = new DownloadReceiver();
        registerReceiver(mReceiver, new IntentFilter(VideoDownloadService.NOTIFICATION));
        Intent intent = new Intent(this, VideoDownloadService.class);
        Bundle b = new Bundle();
        b.putSerializable(VideoDownloadService.KEY_DOWNLOAD_PARAMS, profileUrls);
        intent.putExtras(b);
        startService(intent);
    }
}
