package com.minglematcher.minglematcherapp.services.responses;

import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

/**
 * Created by AnhAcc on 2/15/2016.
 */
public class LoginResponse extends BaseResponse {
    UserData data;

    public UserData getData() {
        return data;
    }
}
