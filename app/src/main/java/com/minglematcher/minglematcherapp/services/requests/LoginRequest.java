package com.minglematcher.minglematcherapp.services.requests;

/**
 * Created by AnhAcc on 2/15/2016.
 */
public class LoginRequest {
    String access_token;

    public LoginRequest(String accessToken) {
        this.access_token = accessToken;
    }
}
