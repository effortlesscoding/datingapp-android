package com.minglematcher.minglematcherapp.services.responses;

/**
 * Created by AnhAcc on 5/1/2016.
 */
public class PassResponse {
    public static class PassResult {
        boolean isPassed;
    }
    PassResult data;
}
