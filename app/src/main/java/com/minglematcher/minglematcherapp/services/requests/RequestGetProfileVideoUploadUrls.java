package com.minglematcher.minglematcherapp.services.requests;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class RequestGetProfileVideoUploadUrls {
    long profileVideoId;
    long videoContentLength;
    long imageContentLength;
    public RequestGetProfileVideoUploadUrls(long videoId, long videoContentLength,
                                            long thumbnailContentLength) {
        profileVideoId = videoId;
        this.videoContentLength = videoContentLength;
        this.imageContentLength = thumbnailContentLength;
    }
}
