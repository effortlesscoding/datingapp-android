package com.minglematcher.minglematcherapp.services.uploading;

import android.os.AsyncTask;
import android.support.annotation.MainThread;
import android.util.Log;

import com.minglematcher.minglematcherapp.dispatcher.ActionsDispatcher;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadDone;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadError;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadProgress;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class VideoUploadTask extends AsyncTask<UploadParams, Double, String> {

    private String TAG = "VideoUploadTask";

    private class UploadTask {

        private void handleResponse(Response response) {
            if (response.code() == 308) {
                if (mCurrentOffset < mFileSize) {
                    pingProgress((double)mCurrentOffset / (double)mFileSize);
                    sendNextChunk();
                } else {
                    pingError("308 Respnose code but current offset is off");
                }
            } else if (response.code() == 200) {
                if (mCurrentOffset == mFileSize) {
                    pingSuccess();
                } else {
                    pingError("Did not finish uploading the file.");
                }
            } else {
                pingError("Unknown response code : " + response.code());
            }
        }

        private final int UPLOAD_CHUNK_SIZE = 262144;
        private final int TIMEOUT = 20;
        private long mFileSize;
        private String mCurrentUploadUrl;
        private FileInputStream mFileISPointer;
        private long mCurrentOffset = 0;
        private String mMimeType;

        public UploadTask(UploadParams params) throws FileNotFoundException {
            if (params.file.exists()) {
                mFileSize = params.file.length();
            }
            mCurrentUploadUrl = params.uploadUrl;
            mMimeType = params.mimeType;
            mFileISPointer = new FileInputStream(params.file);
            mCurrentOffset = 0;
        }


        private void sendChunk(long startByte, long endByte, byte[] fileChunkBuffer) {
            try {
                MediaType BYTES = MediaType.parse(mMimeType);
                mFileISPointer.read(fileChunkBuffer);

                RequestBody body = RequestBody.create(BYTES, fileChunkBuffer);
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(loggingInterceptor)
                        .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                        .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                        .build();
                Request request = null;
                if (startByte > 0 || endByte != mFileSize) {
                    request = new Request.Builder()
                            .url(mCurrentUploadUrl)
                            .header("Content-Type", mMimeType)
                            .header("Content-Range", "bytes " + startByte + "-" + (endByte - 1) +
                                    "/" + mFileSize + "")
                            .put(body) //PUT
                            .build();
                } else {
                    request = new Request.Builder()
                            .url(mCurrentUploadUrl)
                            .header("Content-Type", mMimeType)
                            .header("Content-Length", (endByte - startByte) + "")
                            .put(body) //PUT
                            .build();
                }
                Response response = client.newCall(request).execute();
                handleResponse(response);
            } catch (IOException e) {
                e.printStackTrace();
                pingError(e.getMessage());
            } catch (Exception ge) {
                ge.printStackTrace();
                pingError(ge.getMessage());
            }
        }

        private void pingError(String reason) {
            if (!isImageUploaded) {
                Log.e(TAG, "Thumbnail upload failed for this reason : " + reason);
                mDispatcher.post(mThumbnailErrorPingAction.buildError(reason));
            } else if (!isVideoUploaded) {
                Log.e(TAG, "Video upload failed for this reason : " + reason);
                mDispatcher.post(mThumbnailErrorPingAction.buildError(reason));
            } else {
                Log.e(TAG, "Upload failed for this reason : " + reason);
            }
            cancel(true);
        }

        private void pingProgress(double progress) {
            publishProgress(progress);
        }

        private void pingSuccess() {
            publishProgress(1.0);
        }


        private void sendNextChunk() {
            long startOffset = mCurrentOffset;
            long remainingBytes = mFileSize - mCurrentOffset;
            byte[] fileChunkBuffer = null;
            if (remainingBytes < UPLOAD_CHUNK_SIZE) {
                fileChunkBuffer = new byte[(int)remainingBytes];
                mCurrentOffset += remainingBytes;
            } else {
                fileChunkBuffer = new byte[UPLOAD_CHUNK_SIZE];
                mCurrentOffset += UPLOAD_CHUNK_SIZE;
            }

            sendChunk(startOffset, mCurrentOffset, fileChunkBuffer);
        }

        public void upload() {
            sendNextChunk();
        }
    }

    private ActionsDispatcher mDispatcher = ActionsDispatcher.getBus();
    private boolean isImageUploaded = false;
    private boolean isVideoUploaded = false;
    private ActionUploadProgress mVideoProgressAction;
    private ActionUploadError mVideoErrorPingAction;
    private ActionUploadDone mVideoDoneAction;
    private ActionUploadProgress mThumbnailProgressAction;
    private ActionUploadError mThumbnailErrorPingAction;
    private ActionUploadDone mThumbnailDoneAction;

    private VideoUploadTask() {

    }

    public static VideoUploadTask build() {
        return new VideoUploadTask();
    }

    public VideoUploadTask setVideoActions(ActionUploadProgress progressAction,
                                           ActionUploadError errorAction,
                                           ActionUploadDone doneAction) {

        mVideoProgressAction = progressAction;
        mVideoErrorPingAction = errorAction;
        mVideoDoneAction = doneAction;
        return this;
    }


    public VideoUploadTask setThumbnailActions(ActionUploadProgress progressAction,
                                               ActionUploadError errorAction,
                                               ActionUploadDone doneAction) {
        mThumbnailProgressAction = progressAction;
        mThumbnailErrorPingAction = errorAction;
        mThumbnailDoneAction = doneAction;
        return this;
    }

    @Override
    protected String doInBackground(UploadParams... params) {
        if (params.length != 2) {
            return "Wrong parameters";
        }

        UploadParams videoParams = params[0];
        UploadParams thumbnailParams = params[1];
        try {
            new UploadTask(thumbnailParams).upload();
            isImageUploaded = true;
            mDispatcher.post(mThumbnailDoneAction);
            new UploadTask(videoParams).upload();
            isVideoUploaded = true;
            mDispatcher.post(mVideoDoneAction);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    @MainThread
    protected void onProgressUpdate(Double... values) {
        double progress = values[0];
        if (progress == 1.0) {
            if (isImageUploaded && !isVideoUploaded) {
                mDispatcher.post(mThumbnailDoneAction);
            } else if (isImageUploaded && isVideoUploaded) {
                mDispatcher.post(mVideoDoneAction);
            }
        } else {
            if (!isImageUploaded) {
                mDispatcher.post(mThumbnailProgressAction.buildProgress(progress));
            } else if (!isVideoUploaded) {
                mDispatcher.post(mVideoProgressAction.buildProgress(progress));
            }
        }
    }


}
