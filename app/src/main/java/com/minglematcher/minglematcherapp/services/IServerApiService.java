package com.minglematcher.minglematcherapp.services;

import com.minglematcher.minglematcherapp.services.requests.FinishUploadVideoRequest;
import com.minglematcher.minglematcherapp.services.requests.GetProfilesInfoRequest;
import com.minglematcher.minglematcherapp.services.requests.LikeProfileRequest;
import com.minglematcher.minglematcherapp.services.requests.LoginRequest;
import com.minglematcher.minglematcherapp.services.requests.PassProfileRequest;
import com.minglematcher.minglematcherapp.services.requests.RequestGetProfileVideoUploadUrls;
import com.minglematcher.minglematcherapp.services.requests.StartVideoUploadRequest;
import com.minglematcher.minglematcherapp.services.responses.BaseResponse;
import com.minglematcher.minglematcherapp.services.responses.FinishUploadVideoResponse;
import com.minglematcher.minglematcherapp.services.responses.GetProfilesInfoResponse;
import com.minglematcher.minglematcherapp.services.responses.LikeResponse;
import com.minglematcher.minglematcherapp.services.responses.LoginResponse;
import com.minglematcher.minglematcherapp.services.responses.NextProfilesResponse;
import com.minglematcher.minglematcherapp.services.responses.GetSignedUrlResponse;
import com.minglematcher.minglematcherapp.services.responses.PassResponse;
import com.minglematcher.minglematcherapp.services.responses.ResponseGetProfileVideoUploadUrls;
import com.minglematcher.minglematcherapp.services.responses.StartVideoUploadResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by AnhAcc on 2/16/2016.
 */
public interface IServerApiService {
    @POST("/api/v1/user")
    Call<LoginResponse> getUser(@Body LoginRequest request);

    @POST("/api/v1/user/getUsers")
    Call<GetProfilesInfoResponse> getProfilesInformation(@Body GetProfilesInfoRequest request);

    @POST("/api/v1/user/matching/like")
    Call<LikeResponse> likeProfile(@Body LikeProfileRequest idToLike);

    @POST("/api/v1/user/matching/pass")
    Call<PassResponse> passProfile(@Body PassProfileRequest idToPass);


    @POST("/api/v1/storage/startVideoUpload")
    Call<ResponseGetProfileVideoUploadUrls> getProfileVideoUploadUrls
            (@Body RequestGetProfileVideoUploadUrls request);

    @POST("/api/v1/storage/startVideoUpload")
    @Deprecated
    Call<StartVideoUploadResponse> startVideoUpload(@Body StartVideoUploadRequest request);

    @POST("/api/v1/storage/finishProfileVideoUpload")
    Call<FinishUploadVideoResponse> finishVideoUpload(@Body FinishUploadVideoRequest passId);

    @GET("/my/nextMatches")
    Call<NextProfilesResponse> nextMatches();

    @GET("/api/v1/storage/signedUrl")
    Call<GetSignedUrlResponse> getSignedUrlForResource(@Query("fileName") String resource);
}
