package com.minglematcher.minglematcherapp.services.uploading;

import java.io.File;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class UploadParams {
    public File file;
    public String mimeType;
    public String uploadUrl;

    public UploadParams(File file, String mimeType, String url) {
        this.file = file;
        this.mimeType = mimeType;
        this.uploadUrl = url;
    }
}
