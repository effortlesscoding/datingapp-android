package com.minglematcher.minglematcherapp.services.requests;

/**
 * Created by AnhAcc on 4/16/2016.
 */
public class PassProfileRequest {

    long idToPass;

    public PassProfileRequest(long id) {
        idToPass = id;
    }
}
