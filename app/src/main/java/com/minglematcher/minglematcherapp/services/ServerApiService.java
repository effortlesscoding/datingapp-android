package com.minglematcher.minglematcherapp.services;

import com.minglematcher.minglematcherapp.Constants;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileMatchData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;
import com.minglematcher.minglematcherapp.services.requests.FinishUploadVideoRequest;
import com.minglematcher.minglematcherapp.services.requests.GetProfilesInfoRequest;
import com.minglematcher.minglematcherapp.services.requests.LikeProfileRequest;
import com.minglematcher.minglematcherapp.services.requests.LoginRequest;
import com.minglematcher.minglematcherapp.services.requests.PassProfileRequest;
import com.minglematcher.minglematcherapp.services.requests.RequestGetProfileVideoUploadUrls;
import com.minglematcher.minglematcherapp.services.responses.FinishUploadVideoResponse;
import com.minglematcher.minglematcherapp.services.responses.GetProfilesInfoResponse;
import com.minglematcher.minglematcherapp.services.responses.GetSignedUrlResponse;
import com.minglematcher.minglematcherapp.services.responses.LikeResponse;
import com.minglematcher.minglematcherapp.services.responses.LoginResponse;
import com.minglematcher.minglematcherapp.services.responses.NextProfilesResponse;
import com.minglematcher.minglematcherapp.services.responses.PassResponse;
import com.minglematcher.minglematcherapp.services.responses.ResponseGetProfileVideoUploadUrls;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by AnhAcc on 2/16/2016.
 */
public class ServerApiService {

    private static ServerApiService mInstance;
    private IServerApiService mService;
    private String mToken = null;
    public static ServerApiService getInstance() {
        if (mInstance == null) {
            mInstance = new ServerApiService();
        }
        return mInstance;
    }

    private ServerApiService() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(interceptor);
        clientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                if (mToken != null) {
                    Request request = chain.request()
                            .newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
                    return chain.proceed(request);
                } else {
                    return chain.proceed(chain.request());
                }
            }
        });
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.AppSettings.API_HOST)
                .client(clientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mService = retrofit.create(IServerApiService.class);
    }

    public void likeProfile(long id, Callback cb) {
        Call<LikeResponse> call = mService.likeProfile(new LikeProfileRequest(id));
        call.enqueue(cb);
    }

    public void passProfile(long id, Callback cb) {
        Call<PassResponse> call = mService.passProfile(new PassProfileRequest(id));
        call.enqueue(cb);
    }

    public void setAccessTokenHeader(String token) {
        mToken = token;
    }

    public void loginToFb(String fbToken, Callback cb) {
        Call<LoginResponse> call = mService.getUser(new LoginRequest(fbToken));
        call.enqueue(cb);
    }


    public void getProfilesMatchesInformation(List<ProfileMatchData> ids, Callback cb) {
        Call<GetProfilesInfoResponse> call = mService
                .getProfilesInformation(new GetProfilesInfoRequest().setIds(ids));
        call.enqueue(cb);
    }

    public void getProfilesInformation(List<ProfileSnippetData> ids, Callback cb) {
        Call<GetProfilesInfoResponse> call = mService
                .getProfilesInformation(new GetProfilesInfoRequest(ids));
        call.enqueue(cb);
    }

    public void getNextProfiles(Callback cb) {
        Call<NextProfilesResponse> call = mService.nextMatches();
        call.enqueue(cb);
    }

    public void getProfileVideoUploadUrls(RequestGetProfileVideoUploadUrls request, Callback cb) {
        Call<ResponseGetProfileVideoUploadUrls> call = mService.getProfileVideoUploadUrls(request);
        call.enqueue(cb);
    }

    public void finishVideoUpload(FinishUploadVideoRequest request, Callback cb) {
        Call<FinishUploadVideoResponse> call = mService.finishVideoUpload(request);
        call.enqueue(cb);
    }

    public void getSignedUrlForResource(String resourcePath, Callback<GetSignedUrlResponse> callback) {
        Call<GetSignedUrlResponse> call = mService.getSignedUrlForResource(resourcePath);
        call.enqueue(callback);
    }

    public String getSignedUrlForResourceSync(String resource) {
        Call<GetSignedUrlResponse> call = mService.getSignedUrlForResource(resource);
        try {
            retrofit2.Response<GetSignedUrlResponse> response = call.execute();
            Object body = response.body();
            if (body instanceof GetSignedUrlResponse) {
                GetSignedUrlResponse responseBody = (GetSignedUrlResponse) body;
                String urlString = responseBody.getData();
                //Log.i("VIDEOD_DOWNLOAD", "Signed url is :" + urlString);
                //URL url = new URL(urlString);
                return urlString;
            } else {
                return null;
            }
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
