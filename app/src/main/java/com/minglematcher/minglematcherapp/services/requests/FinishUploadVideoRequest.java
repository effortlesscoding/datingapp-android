package com.minglematcher.minglematcherapp.services.requests;

/**
 * Created by AnhAcc on 4/23/2016.
 */
public class FinishUploadVideoRequest {
    long profileVideoId;

    public FinishUploadVideoRequest(long videoId) {
        profileVideoId = videoId;
    }
}
