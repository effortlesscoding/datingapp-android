package com.minglematcher.minglematcherapp.services.responses;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileMatchData;

/**
 * Created by AnhAcc on 5/1/2016.
 */
public class LikeResponse extends BaseResponse {
    public static class LikeResult {
        boolean isLiked;
        boolean isMutual;
        ProfileMatchData matchedProfile;
        public boolean isLiked() {
            return isLiked;
        }

        public ProfileMatchData getMatchData() {
            return matchedProfile;
        }

        public boolean isMutual() {
            return isMutual;
        }
    }
    LikeResult data;

    public LikeResult getData() {
        return data;
    }
}
