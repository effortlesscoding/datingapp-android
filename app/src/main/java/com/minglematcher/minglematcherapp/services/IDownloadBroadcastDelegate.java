package com.minglematcher.minglematcherapp.services;

import com.minglematcher.minglematcherapp.services.parameters.VideoDownloadParams;

/**
 * Created by AnhAcc on 4/10/2016.
 */
public interface IDownloadBroadcastDelegate {
    void onDownloaded(VideoDownloadParams params);
    void onDownloadFailed(VideoDownloadParams params, String reason);
}
