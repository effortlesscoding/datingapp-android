package com.minglematcher.minglematcherapp.services.responses;

import com.minglematcher.minglematcherapp.viewmodels.user.NextMatchesData;

/**
 * Created by AnhAcc on 2/17/2016.
 */
public class NextProfilesResponse extends BaseResponse {
    NextMatchesData[] data;

    public NextMatchesData[] getData() {
        return data;
    }
}
