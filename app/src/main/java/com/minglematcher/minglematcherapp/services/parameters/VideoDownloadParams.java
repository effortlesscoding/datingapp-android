package com.minglematcher.minglematcherapp.services.parameters;

import android.os.Parcel;
import android.os.Parcelable;

import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;

import java.io.File;

/**
 * Created by AnhAcc on 4/9/2016.
 */
public class VideoDownloadParams implements Parcelable {
    public String videoUrl;
    public String videoLocalFilePath;
    public String localVideoFileName;
    private ProfileSnippetData mProfileSource;
    private String mFullPath;

    public VideoDownloadParams(ProfileSnippetData psd) {
        this.videoUrl = psd.getProfile().getVideoServerPath();
        this.videoLocalFilePath = psd.getProfile().getVideoFilePath();
        this.localVideoFileName = psd.getFileName();
        mProfileSource = psd;
    }


    public void setFullPath(File fullPath) {
        mFullPath = fullPath.getAbsolutePath();
    }

    public ProfileSnippetData getProfileSnippetData() {
        return mProfileSource;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoUrl);
        dest.writeString(localVideoFileName);
        dest.writeString(mFullPath);
        dest.writeParcelable(mProfileSource, flags);
    }

    protected VideoDownloadParams(Parcel in) {
        videoUrl = in.readString();
        localVideoFileName = in.readString();
        mFullPath = in.readString();
        mProfileSource = in.readParcelable(ProfileSnippetData.class.getClassLoader());
    }

    public static final Creator<VideoDownloadParams> CREATOR = new Creator<VideoDownloadParams>() {
        @Override
        public VideoDownloadParams createFromParcel(Parcel in) {
            return new VideoDownloadParams(in);
        }

        @Override
        public VideoDownloadParams[] newArray(int size) {
            return new VideoDownloadParams[size];
        }
    };
}
