package com.minglematcher.minglematcherapp.services.responses;

/**
 * Created by AnhAcc on 4/23/2016.
 */
public class FinishUploadVideoResponse extends BaseResponse {

    public static class Data {
        public long _id;
        public String fileName;
        public String thumbnail;
        public boolean uploaded;
        public String storagePath;
        public long updatedAt;
        public long createdAt;
    }

    Data data;

    public Data getData() {
        return data;
    }
}
