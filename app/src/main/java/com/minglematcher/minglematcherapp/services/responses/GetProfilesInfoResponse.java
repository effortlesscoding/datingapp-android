package com.minglematcher.minglematcherapp.services.responses;

import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;

/**
 * Created by AnhAcc on 4/9/2016.
 */
public class GetProfilesInfoResponse extends BaseResponse {
    ProfileSnippetData[] data;

    public ProfileSnippetData[] getData() {
        return data;
    }
}
