package com.minglematcher.minglematcherapp.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.services.parameters.VideoDownloadParams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by AnhAcc on 4/9/2016.
 */
public class VideoDownloadService extends IntentService {

    public static final String PROFILE_DIR = "/profiles/";
    public static final String KEY_DOWNLOAD_PARAMS = "vidDownloadParams";
    public static final String KEY_PROFILE_DOWNLOAD_PARAMS = "downloadParams";
    public static final String RESULT = "result";
    public static final String NOTIFICATION = "com.minglematcher.minglematcherapp.services.receiver";
    // Put this into settings, actually.
    public static final int MAX_DOWNLOAD_QUEUE = 3;

    public VideoDownloadService(){
        super("VideoDownloadService");
    }

    private void downloadVideo(String urlString, File profileAssetsPath, VideoDownloadParams vidParams) {
        File output = new File(profileAssetsPath, "downloading_" + vidParams.localVideoFileName);
        if (output.exists()) {
            output.delete();
        }
        Log.i("VIDEO_DOWNLOAD", output.getAbsolutePath());
        boolean fullyDownloaded = false;
        InputStream istream = null;
        FileOutputStream fos = null;
        try {
            output.createNewFile();
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            fos = new FileOutputStream(output);
            istream = connection.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ( (len1 = istream.read(buffer)) > 0 ) {
                fos.write(buffer,0, len1);
            }
            fullyDownloaded = true;
            Log.i("VIDEO_DOWNLOAD", "Just finished predownloading a video.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (istream != null) {
                try {
                    istream.close();
                    Log.i("VIDEO_DOWNLOAD", "STREAM HAS BEEN CLOSED.");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    Log.i("VIDEO_DOWNLOAD", "FOS HAS BEEN CLOSED.");
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fullyDownloaded) {
                output.renameTo(new File(profileAssetsPath, vidParams.localVideoFileName));
                vidParams.setFullPath(output);
                publishResults(vidParams, Activity.RESULT_OK);
            } else {
                publishResults(vidParams, Activity.RESULT_CANCELED);
            }
        }
    }

    VideoDownloadParams[] getVideoDownloadParameters(Intent intent) {
        Bundle b = intent.getExtras();
        if (b != null) {
            Parcelable[] params = b.getParcelableArray(KEY_DOWNLOAD_PARAMS);
            VideoDownloadParams[] videoParams = new VideoDownloadParams[params.length];
            for (int i = 0; i < params.length; i++) {
                if (params[i] instanceof VideoDownloadParams) {
                    videoParams[i] = (VideoDownloadParams)params[i];
                } else {
                    return null;
                }
            }
            return videoParams;
        } else {
            return null;
        }
    }
    // will be called asynchronously by Android
    @Override
    protected void onHandleIntent(Intent intent) {
        VideoDownloadParams[] videosParams = getVideoDownloadParameters(intent);
        if (videosParams != null) {
            File profileAssetsPath = new File(getFilesDir() + PROFILE_DIR);
            if (!profileAssetsPath.exists()) {
                profileAssetsPath.mkdirs();
            }
            Log.i("VIDEO_DOWNLOAD", "Path to directory : " + profileAssetsPath.getAbsolutePath());
            for (int i = 0; i < videosParams.length; i++) {
                VideoDownloadParams vidParams = videosParams[i];
                Log.i("VIDEO_DOWNLOAD", "Downloading : " + vidParams.localVideoFileName);
                String url = ServerApiService.getInstance().getSignedUrlForResourceSync(vidParams.videoUrl);
                if (url != null) {
                    downloadVideo(url, profileAssetsPath, vidParams);
                } else {
                    Log.e("VIDEO_DOWNLOAD", "Could not get signed url for " + vidParams.videoUrl);
                }
            }
        } else {
            // Do something else here.
            publishResults(null, Activity.RESULT_CANCELED);
        }
    }

    private void publishResults(VideoDownloadParams params, int result) {
        Intent intent = new Intent(NOTIFICATION);
        Bundle b = new Bundle();
        b.putParcelable(KEY_PROFILE_DOWNLOAD_PARAMS, params);
        intent.putExtras(b);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }

    private void deleteFolderRecursively(File dir) {
        if (dir.exists()) {
            File[] files = dir.listFiles();
            if(files!=null) { //some JVMs return null for empty dirs
                for(File f: files) {
                    if(f.isDirectory()) {
                        deleteFolderRecursively(f);
                    } else {
                        f.delete();
                    }
                }
            }
        }
    }
    public void deleteProfiles() {
        File dir = new File(MingleMatcherApplication.getContext().getFilesDir() + PROFILE_DIR);
        deleteFolderRecursively(dir);
    }
}
