package com.minglematcher.minglematcherapp.services.requests;

/**
 * Created by AnhAcc on 4/5/2016.
 */
@Deprecated
public class StartVideoUploadRequest {
    public String fileName;
    public String mimeType;
    public long contentLength;

    public StartVideoUploadRequest(String fileName, String mimeType, long contentLength) {
        this.fileName = fileName;
        this.mimeType = mimeType;
        this.contentLength = contentLength;
    }
}
