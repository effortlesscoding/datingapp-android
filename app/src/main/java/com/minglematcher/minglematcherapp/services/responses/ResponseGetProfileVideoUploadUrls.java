package com.minglematcher.minglematcherapp.services.responses;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class ResponseGetProfileVideoUploadUrls extends BaseResponse {
    public static class Data {
        public UploadUrl video;
        public UploadUrl thumbnail;
    }
    public static class UploadUrl {
        public String mimeType;
        public String url;
    }

    public Data data;


}
