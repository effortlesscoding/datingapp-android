package com.minglematcher.minglematcherapp.services.requests;

import com.minglematcher.minglematcherapp.viewmodels.user.ProfileMatchData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 4/9/2016.
 */
public class GetProfilesInfoRequest {
    List<ProfileSnippetData> ids;

    public GetProfilesInfoRequest() {

    }

    public GetProfilesInfoRequest setIds(List<ProfileMatchData> matches) {
        ids = new ArrayList<>();
        for (ProfileMatchData pmd : matches) {
            ids.add(new ProfileSnippetData(pmd.getId()));
        }
        return this;
    }

    public GetProfilesInfoRequest(List<ProfileSnippetData> facebookIds) {
        ids = facebookIds;
    }
}
