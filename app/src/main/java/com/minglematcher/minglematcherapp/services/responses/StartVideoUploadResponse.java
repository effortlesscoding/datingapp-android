package com.minglematcher.minglematcherapp.services.responses;

import com.minglematcher.minglematcherapp.viewmodels.user.UploadVideoData;

/**
 * Created by AnhAcc on 4/5/2016.
 */
@Deprecated
public class StartVideoUploadResponse extends BaseResponse {

    UploadVideoData data;

    public UploadVideoData getData() {
        return data;
    }
}
