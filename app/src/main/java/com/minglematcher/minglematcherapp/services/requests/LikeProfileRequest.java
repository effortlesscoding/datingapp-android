package com.minglematcher.minglematcherapp.services.requests;

/**
 * Created by AnhAcc on 4/16/2016.
 */
public class LikeProfileRequest {
    long idToLike;

    public LikeProfileRequest(long likedId) {
        this.idToLike = likedId;
    }
}
