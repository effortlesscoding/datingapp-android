package com.minglematcher.minglematcherapp.utils;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class FilenameUtils {
    public static boolean isCorrectExtension(String fileName, String expectedExtension) {
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            String extension = fileName.substring(i+1);
            return extension.equals(expectedExtension);
        } else {
            return false;
        }
    }
}
