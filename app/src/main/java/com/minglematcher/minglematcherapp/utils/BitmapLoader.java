package com.minglematcher.minglematcherapp.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;

/**
 * Created by AnhAcc on 1/2/2016.
 */
public class BitmapLoader {

    public static void saveImageToFile(Bitmap bitmap, File file) {
        if (file.exists()) {
            file.delete();
        }
        FileOutputStream out = null;
        try {
            file.createNewFile();
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static File saveThumbnailForVideo(File videoFile) {
        FileOutputStream out = null;
        File bitmapFile;
        try {
            Bitmap thumbnail = BitmapLoader.getVideoFrameFromVideo(videoFile, 1000);
            String videoFileName = videoFile.getName();
            String thumbnailFileName = videoFileName.substring(0, videoFileName.lastIndexOf('.')) + ".jpg";
            bitmapFile = new File(videoFile.getParent(), thumbnailFileName);
            if (bitmapFile.exists()) {
                bitmapFile.delete();
            }
            bitmapFile.createNewFile();
            out = new FileOutputStream(bitmapFile);
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmapFile;
    }

    public static Bitmap getVideoFrameFromVideo(File pFile, long time)
            throws Throwable {
        if (!pFile.exists()) {
            return null;
        }
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(pFile.getAbsolutePath());
            bitmap = mediaMetadataRetriever.getFrameAtTime(time);
        } catch (Exception m_e) {
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String p_videoPath)"
                            + m_e.getMessage());
        } finally {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    private static final int INITIAL_SAMPLE_SIZE = 2;
    private static final int SAMPLE_SIZE_INCREMENT = 4;
    private static final String ASSET_PATH = "file:///android_asset/";

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = INITIAL_SAMPLE_SIZE;

        Log.i("Sample Size", String.format("Required WxH are: %d x %d. Size is : %d x %d",
                reqWidth, reqHeight, width, height));
        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / SAMPLE_SIZE_INCREMENT;
            final int halfWidth = width / SAMPLE_SIZE_INCREMENT;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= SAMPLE_SIZE_INCREMENT;
            }
        }

        return inSampleSize;
    }

    public static void test() {
        /*
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        final String assetPath = String.format("%s%s", ASSET_PATH, assetFile);
        BitmapFactory.decodeFile(assetPath);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        Log.i("Sample Size", String.format("Required WxH are: %d x %d. InSampleSize is : %d",
                reqWidth, reqHeight, options.inSampleSize));
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(assetPath);
        */
    }

    public static Bitmap getImage(Context c, String assetFile, int reqWidth, int reqHeight) {
        try {
            InputStream bitmap = c.getAssets().open(assetFile);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(bitmap, null, options);
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            Log.i("Sample Size", String.format("Required WxH are: %d x %d. InSampleSize is : %d",
                    reqWidth, reqHeight, options.inSampleSize));
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(bitmap, null, options);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (NullPointerException exc) {
            exc.printStackTrace();
        }
        return null;
    }

    /**
     * Preferrably, do this in a background with AsyncTask
     * @param assetFile
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static Bitmap getImage(String assetFile, int reqWidth, int reqHeight) {
        try {
            InputStream bitmap = MingleMatcherApplication.getContext().getAssets().open(assetFile);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(bitmap, null, options);
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            Log.i("Sample Size", String.format("Required WxH are: %d x %d. InSampleSize is : %d",
                    reqWidth, reqHeight, options.inSampleSize));
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(bitmap, null, options);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (NullPointerException exc) {
            exc.printStackTrace();
        }
        return null;
    }

    public static Bitmap getImage(Resources resources, int resourceId,
                                  int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, resourceId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        Log.i("Sample Size", String.format("Required WxH are: %d x %d. InSampleSize is : %d",
                reqWidth, reqHeight, options.inSampleSize));
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(resources, resourceId, options);
    }
}
