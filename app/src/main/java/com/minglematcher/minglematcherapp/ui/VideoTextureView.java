package com.minglematcher.minglematcherapp.ui;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Surface;
import android.view.TextureView;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by AnhAcc on 2/8/2016.
 */
public class VideoTextureView extends TextureView implements TextureView.SurfaceTextureListener {

    private MediaPlayer mMediaPlayer;
    private Surface mSurface;
    private boolean mPlayerReady;
    private boolean mPlaying;
    private String mVideoUrl;
    private static String TAG = "VIDEO_TEXTURE_VIEW";
    private boolean mVideoSizeSetupDone;

    public VideoTextureView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void release(boolean cleartargetstate) {
        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
            AudioManager am = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
            am.abandonAudioFocus(null);
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mSurface = new Surface(surface);
        if (mVideoUrl == null || mSurface == null) {
            // not ready for playback just yet, will try again later
            return;
        }
        // we shouldn't clear the target state, because somebody might have
        // called start() previously
       // release(false);

        AudioManager am = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        try {
            //ExoPlayer ep = null;
            mMediaPlayer = new MediaPlayer();
            // TODO: create SubtitleController in MediaPlayer, but we need
            // a context for the subtitle renderers
            final Context context = getContext();

            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    Log.i(TAG, "PREPARED!");
                    mMediaPlayer.start();
                }
            });
            mMediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                @Override
                public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                    int k = 0;
                }
            });
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    int i = 0;
                }
            });
            mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });
            mMediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });
            mMediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    int i = 0;
                }
            });
            mMediaPlayer.setSurface(mSurface);
            mMediaPlayer.setDataSource(getContext(), Uri.parse(mVideoUrl), null);
            mMediaPlayer.prepare();
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        } catch (IOException ex) {
            Log.w(TAG, "Unable to open content: " + mVideoUrl, ex);
            return;
        } catch (IllegalArgumentException ex) {
            Log.w(TAG, "Unable to open content: " + mVideoUrl, ex);
            return;
        } finally {
        }
    }
    public void onSurfaceTextureAvailableOld(SurfaceTexture surface, int width, int height) {
        mSurface = new Surface(surface);
        try {
           // mMediaPlayer = MediaPlayer.create(getContext(), Uri.parse(mVideoUrl));
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(getContext(), Uri.parse(mVideoUrl));
            mMediaPlayer.setSurface(mSurface);
            mMediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    Log.i(TAG, percent + "");
                }
            });
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Log.w(TAG, "Video playback finished");
                }
            });
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    Log.i(TAG, "ON prepared!");
                    mPlayerReady = true;
                    startPlayback();
                }
            });
            mMediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                @Override
                public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                    Log.w(TAG, "Video size changed: " + width + "x" + height);
                    if (width > 0 && height > 0 && !mVideoSizeSetupDone) {
                        changeVideoSize(width, height);
                    }
                }
            });
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayerReady = true;

            if (mPlaying) {
                mMediaPlayer.start();
            }
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }



    /** MediaPlayer methods **/

    private void startPlayback() {
        if (mMediaPlayer != null) {
            onLoaded(mMediaPlayer);
            mMediaPlayer.start();
        }
    }

    private void pausePlayback() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
            mMediaPlayer.pause();
    }

    private void resumePlayback() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
            mMediaPlayer.start();
    }

    private void onLoaded(MediaPlayer mp) {
    }

    public void onPrepared(MediaPlayer mp) {
        mPlayerReady = true;
        startPlayback();
    }

    public void dispose() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void changeVideoSize(int width, int height) {
        Log.i(TAG, "CHANGING VIDEO SIZE!!!");
        DisplayMetrics metrics = new DisplayMetrics();
        RelativeLayout.LayoutParams params;
        params = new RelativeLayout.LayoutParams(width, height);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, -1);
        this.setLayoutParams(params);
        this.setRotation(0.0f);
        mVideoSizeSetupDone = true;
    }


    public void initVideo(String videoUrl) {
        mPlayerReady = false;
        mVideoSizeSetupDone = false;
        mPlaying = false;
        mVideoUrl = videoUrl;
        this.setSurfaceTextureListener(this);
    }

    public void play() {
        mPlaying = true;
        Log.i(TAG, "PLAYING!!!");
        if (mMediaPlayer != null) {
            mMediaPlayer.start();
        }
    }
}

