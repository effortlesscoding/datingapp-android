package com.minglematcher.minglematcherapp.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.history.HistoryActivity;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class ActionBarActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        setTitle("");
        return true;
    }

    //NEW
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_history:
                if (this instanceof HistoryActivity) { return false; }
                Intent historyIntent = new Intent(ActionBarActivity.this, HistoryActivity.class);
                startActivity(historyIntent);
                this.finish();
                return true;
            case R.id.action_matches:
                return true;
            default:
                return false;
        }
    }


}
