package com.minglematcher.minglematcherapp.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by AnhAcc on 2/14/2016.
 */
public class AppButton extends Button {
    public AppButton(Context context) {
        super(context);
        setFontFamily();
    }

    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFontFamily();
    }

    public AppButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFontFamily();
    }

    private void setFontFamily() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_medium.ttf");
        setTypeface(font);
    }

}
