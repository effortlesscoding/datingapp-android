package com.minglematcher.minglematcherapp.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.minglematcher.minglematcherapp.R;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by AnhAcc on 4/25/2016.
 */
public class CircularProgressBar extends View {

    private static final int DEFAULT_MAX_VALUE    = 100;
    private static final float ADJUST_FOR_12_OCLOCK = 270f;
    private static final int DEFAULT_PROGRESS_VALUE = 0;
    // properties for the background circle
    private Paint mBgPaint;

    // properties for the progress circle
    private Paint mProgressPaint;

    // text properties for the countdown text
    private boolean mShowText = true;
    private Paint mTextPaint;

    // maximum number of points in the circle default is 100
    private int mMax;

    // current progress between 0 and mMax
    private int  mProgress = DEFAULT_PROGRESS_VALUE;

    // diameter (in dp) of the circle
    private float mDiameter = 0.0f;

    // margin between circle and edges (default is 4dp)
    // NOTE: you will need to include some margin to account for the stroke width, so min padding is strokeWidth/2
    private int mLayoutMargin;

    // area to draw the progress arc
    private RectF mArcBounds;


    // height taken to draw text with the current settings
    private Rect mTextBounds;

    private Bitmap mBgImage;
    private Bitmap mThumbnailImage;

    public CircularProgressBar(Context context, final AttributeSet attrs) {
        super(context, attrs);
        final TypedArray args = context.obtainStyledAttributes(attrs, R.styleable.circularProgressBar);

        final float defaultDiameter = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64, this.getResources()
                .getDisplayMetrics());
        final float defaultStrokeWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, this.getResources()
                .getDisplayMetrics());
        final float defaultMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, this.getResources()
                .getDisplayMetrics());
        final float defaultTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14, this.getResources()
                .getDisplayMetrics());

        try {
            final int progressColor = args.getColor(R.styleable.circularProgressBar_progressColor,
                    ContextCompat.getColor(this.getContext(), R.color.white));
            final int bgColor = args.getColor(R.styleable.circularProgressBar_bgColor,
                    ContextCompat.getColor(this.getContext(), R.color.black));
            final int bgStrokeWidth = args.getDimensionPixelSize(R.styleable.circularProgressBar_bgStrokeWidth,
                    (int) defaultStrokeWidth);
            final int progressStrokeWidth = args.getDimensionPixelSize(
                    R.styleable.circularProgressBar_progressStrokeWidth, (int) defaultStrokeWidth);
            final int textColor = args.getInt(R.styleable.circularProgressBar_android_textColor,
                    R.color.white);
            final int textSize = args.getDimensionPixelSize(R.styleable.circularProgressBar_android_textSize,
                    (int) defaultTextSize);

            this.mLayoutMargin = args.getDimensionPixelSize(R.styleable.circularProgressBar_android_layout_margin,
                    (int) defaultMargin);

            this.mMax = args.getInt(R.styleable.circularProgressBar_max, DEFAULT_MAX_VALUE);
            //this.mDiameter = args.getDimension(R.styleable.circularProgressBar_diameter, defaultDiameter);

            this.mBgPaint = new Paint();
            this.mBgPaint.setColor(bgColor);
            this.mBgPaint.setStyle(Paint.Style.STROKE);
            this.mBgPaint.setAntiAlias(true);
            this.mBgPaint.setStrokeWidth(bgStrokeWidth);

            this.mProgressPaint = new Paint();
            this.mProgressPaint.setColor(progressColor);
            this.mProgressPaint.setStyle(Paint.Style.STROKE);
            this.mProgressPaint.setAntiAlias(true);
            this.mProgressPaint.setStrokeWidth(progressStrokeWidth);

            this.mTextPaint = new Paint();
            this.mTextPaint.setColor(textColor);
            this.mTextPaint.setAntiAlias(true);
            this.mTextPaint.setStyle(Paint.Style.STROKE);
            this.mTextPaint.setTextAlign(Paint.Align.CENTER);
            this.mTextPaint.setTextSize(textSize);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            args.recycle();
        }
    }

    public void setThumbnail(Bitmap thumbnail) {
        int side = (int)mDiameter;
        mBgImage = thumbnail;
        mThumbnailImage = null;
        this.invalidate();
    }

    public void removeThumbnail() {
        mBgImage = null;
        mThumbnailImage = null;
        this.invalidate();
    }
    public void setProgress(double progress) {

        this.mProgress = (int)(this.mMax * progress);
        this.invalidate();
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        Log.i("CIRCULAR", "onMeasure()");

        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.mDiameter = parentWidth > parentHeight ? parentHeight : parentWidth;
        // size will always be diameter + margin on add sides
        final int size = (int) this.mDiameter + (this.mLayoutMargin * 2);
        this.setMeasuredDimension(size, size);
    }

    public Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public Bitmap getCroppedBitmap(Bitmap bmp, int newWidth, int newHeight) {
        return Bitmap.createBitmap(bmp, 0,0,newWidth, newHeight);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    private Bitmap getSquareImage() {
        if (mThumbnailImage == null) {

            int h = mBgImage.getHeight();
            int w = mBgImage.getWidth();
            Log.i("SIZES", "h/w : " + h + " / " + w);
            double whRatio = (double)w / (double)h;
            // New w x h with preserved aspect ratio
            int resizedHeight = 0;
            int resizedWidth = 0;
            int side = (int)mDiameter;
            if (w > h) {
                resizedHeight = side;
                resizedWidth = (int) (whRatio * resizedHeight);
            } else {
                resizedWidth = side;
                resizedHeight = (int) (resizedWidth / whRatio);
            }
            this.mThumbnailImage = getRoundedCornerBitmap(
                    getCroppedBitmap(
                            getResizedBitmap(mBgImage, resizedWidth, resizedHeight
                            ), side, side
                    ), side
            );
        }
        return mThumbnailImage;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        Log.i("CIRCULAR", "onDraw()");
        if (this.mArcBounds == null)
        {
            // set view bounds for arc drawing
            this.mArcBounds = new RectF(this.mLayoutMargin, this.mLayoutMargin, this.mLayoutMargin + this.mDiameter,
                    this.mLayoutMargin + this.mDiameter);
        }

        Paint p = new Paint();
        if (mBgImage == null) {
            int side = (int) this.mDiameter;
            mBgImage = BitmapFactory.decodeResource(getResources(), R.drawable.frog_pile);
            mBgImage = getResizedBitmap(mBgImage, side, side);
            canvas.drawBitmap(mBgImage, this.mLayoutMargin, this.mLayoutMargin, p);
            mBgImage = null;
        } else {
            if (mProgress != mMax) {
                ColorMatrix cm = new ColorMatrix();
                cm.setSaturation(0);
                ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
                p.setColorFilter(f);
            }
            getSquareImage();
            canvas.drawBitmap(mThumbnailImage, this.mLayoutMargin, this.mLayoutMargin, p);
        }
        // draw bg circle in the center
        if (mProgress != 0 && mProgress < mMax) {
            final float radius = this.mDiameter / 2;
            final float center = radius + this.mLayoutMargin;
            canvas.drawCircle(center, center, radius, this.mBgPaint);
            // draw any progress over the top
            // why is this BigDecimal crap even needed? java why?
            // Default value is 10/100
            final BigDecimal percentage = BigDecimal.valueOf(this.mProgress)
                    .divide(BigDecimal.valueOf(this.mMax), 4, RoundingMode.HALF_DOWN);
            Log.i("CIRCULAR", "Percentage : " + percentage.toString());
            final BigDecimal sweepAngle = percentage.multiply(BigDecimal.valueOf(360));
            Log.i("CIRCULAR", "Sweep angle : " + percentage.toString());

            // bounds are same as the bg circle, so diameter width and height moved in by margin
            canvas.drawArc(this.mArcBounds, CircularProgressBar.ADJUST_FOR_12_OCLOCK, sweepAngle.floatValue(), false,
                    this.mProgressPaint);


            if (this.mShowText) {
                if (this.mTextBounds == null) {
                    // Reference: http://stackoverflow.com/questions/3654321/measuring-text-height-to-be-drawn-on-canvas-android
                    // answer #2
                    this.mTextBounds = new Rect();
                    this.mTextPaint.getTextBounds("0", 0, 1, this.mTextBounds); //$NON-NLS-1$
                }

                // draw text in the center
                canvas.drawText(String.valueOf(this.mProgress), center, center + (this.mTextBounds.height() >> 1),
                        this.mTextPaint);
            }
        }

    }
}