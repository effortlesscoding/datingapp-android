package com.minglematcher.minglematcherapp.ui.swiper;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.BuildConfig;
import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfilePersonalData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;
import com.minglematcher.minglematcherapp.ui.VideoTextureView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * Created by AnhAcc on 2/7/2016.
 */
public class FlingAdapter extends BaseAdapter {

    public static class FlingAdapterViewHolder {
        private WeakReference<VideoTextureView> mSurfaceView;
        private WeakReference<TextView> mNameText;
        private WeakReference<TextView> mAgeText;

        private WeakReference<TextView> mOccupationText;
        private WeakReference<TextView> mTaglineText;
        private ProfileSnippetData mModel;

        public WeakReference<VideoTextureView> getSurfaceView() {
            return mSurfaceView;
        }

        public FlingAdapterViewHolder(ProfileSnippetData psd) {
            mModel = psd;
        }
        public void setSurfaceView(WeakReference<VideoTextureView> mSurfaceView) {
            this.mSurfaceView = mSurfaceView;
        }

        public WeakReference<TextView> getNameText() {
            return mNameText;
        }

        public void setNameText(WeakReference<TextView> name) {
            this.mNameText = name;
        }

        public WeakReference<TextView> getAgeText() {
            return mAgeText;
        }

        public void setAgeText(WeakReference<TextView> mAge) {
            this.mAgeText = mAge;
        }

        public WeakReference<TextView> getTaglineText() {
            return mTaglineText;
        }

        public void setTaglineText(WeakReference<TextView> mTaglineText) {
            this.mTaglineText = mTaglineText;
        }

        public ProfileSnippetData getModel() {
            return mModel;
        }

        public WeakReference<TextView> getOccupationText() {
            return mOccupationText;
        }

        public void setOccupationText(WeakReference<TextView> mOccupationText) {
            this.mOccupationText = mOccupationText;
        }
    }

    private ArrayList<FlingAdapterViewHolder> mProfiles;
    private WeakReference<Context> mContext;
    private WeakReference<LayoutInflater> mLayoutInflater;
    private int mLayoutId;

    public FlingAdapter(Context context, int layoutId) {
        mProfiles = new ArrayList<>();
        mContext = new WeakReference<Context>(context);
        mLayoutId = layoutId;
        mLayoutInflater = new WeakReference<LayoutInflater>((LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE));
    }

    public void addItem(ProfileSnippetData pd) {
        mProfiles.add(new FlingAdapterViewHolder(pd));
    }

    @Override
    public int getCount() {
        return mProfiles.size();
    }

    @Override
    public Object getItem(int position) {

        return mProfiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context c = mContext.get();
        LayoutInflater li = mLayoutInflater.get();

        Log.i("Adapter", "getView() at position:" + position);
        if (li != null) {
            FlingAdapterViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = li.inflate(mLayoutId, parent, false);
                viewHolder = mProfiles.get(position);
                ProfileSnippetData p = viewHolder.getModel();
                if (p != null) {
                    ProfilePersonalData pd = p.getProfile();
                    TextView nameText = (TextView) convertView.findViewById(R.id.fProfileName);
                    TextView ageText = (TextView) convertView.findViewById(R.id.fProfileAge);
                    TextView taglineText = (TextView)convertView.findViewById(R.id.fProfileDescription);
                    TextView occupationText = (TextView)convertView.findViewById(R.id.fProfileOccupation);
                    VideoTextureView vtv = (VideoTextureView)convertView.findViewById(R.id.fProfileVideo);

                    nameText.setText(pd.getFirstName() + " " + pd.getLastName());
                    ageText.setText(pd.getAge() + " y.o.");
                    taglineText.setText("");
                    occupationText.setText("");
                    vtv.setBackgroundColor(Color.parseColor("#000000"));
                    viewHolder.setSurfaceView(new WeakReference<VideoTextureView>(vtv));
                    viewHolder.setNameText(new WeakReference<TextView>(nameText));
                    viewHolder.setAgeText(new WeakReference<TextView>(ageText));
                    viewHolder.setOccupationText(new WeakReference<TextView>(occupationText));
                    viewHolder.setTaglineText(new WeakReference<TextView>(taglineText));
                }
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (FlingAdapterViewHolder) convertView.getTag();
                // What to do here?
            }
        }
        return convertView;
    }

    public void dispose() {
        for (FlingAdapterViewHolder vh : mProfiles) {
            if (vh.getSurfaceView() != null) {
                VideoTextureView vtv = vh.getSurfaceView().get();
                if (vtv != null) {
                    vtv.dispose();
                    vtv = null;
                }
            }
        }
    }

    public void dispose(int start, int count) {
        if (mProfiles.size() >= start + count) {
            for (int i = start; i < start + count; i++) {
                FlingAdapterViewHolder favh = mProfiles.get(i);
                WeakReference<VideoTextureView> wvtv = favh.getSurfaceView();
                if (wvtv != null) {
                    VideoTextureView vtv = wvtv.get();
                    // DEBUG ONLY
                    if (BuildConfig.DEBUG) {
                        ProfileSnippetData p = favh.getModel();
                        if (p != null && p.getProfile() != null) {
                            Log.i("DISPOSE", p.getProfile().getName());
                        }
                    }
                    if (vtv != null) {
                        vtv.dispose();
                    }
                }
            }
        }
    }

    public void remove(int index) {
        if (index < mProfiles.size()) {
            mProfiles.remove(0);
            notifyDataSetChanged();
        }
    }

    public void playFirst() {
        if (mProfiles.size() > 0) {
            FlingAdapterViewHolder vh = mProfiles.get(0);
            WeakReference<VideoTextureView> wvv = vh.getSurfaceView();
            WeakReference<TextView> wtv = vh.getNameText();
            if (wvv != null) {
                VideoTextureView vtv = wvv.get();
                if (vtv != null) {
                    Log.i("FLING_ADAPTER", "Playing the video.");
                    ProfileSnippetData psd = vh.getModel();
                    if (psd != null) {
                        vtv.initVideo(psd.getLocalVideoPath());
                    }
                } else {
                    Log.i("FLING_ADAPTER", "Video is empty.");
                }
            } else {
                Log.i("FLING_ADAPTER", "Video reference is empty.");
            }
        }
    }
}
