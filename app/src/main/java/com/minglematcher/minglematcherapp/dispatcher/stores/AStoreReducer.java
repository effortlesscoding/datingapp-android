package com.minglematcher.minglematcherapp.dispatcher.stores;

import android.util.Log;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;

import java.lang.ref.WeakReference;

import rx.functions.Action1;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public abstract class AStoreReducer<T extends IStore> implements Action1<Object> {

    protected WeakReference<T> mStore;

    public AStoreReducer(T store) {
        mStore = new WeakReference<T>(store);
    }

    @Override
    public void call(Object o) {
        if (!(o instanceof Action)) {
            Log.e("StoreReducer", "Not an action : " + o.getClass().toString());
            return;
        }
        Action action = (Action) o;
        if (mStore == null) { return; }
        T store = mStore.get();
        if (store == null) { return; }
        onAction(store, action);
    }

    protected abstract void onAction(T store, Action action);
}
