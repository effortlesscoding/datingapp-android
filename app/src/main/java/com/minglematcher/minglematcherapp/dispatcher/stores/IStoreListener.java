package com.minglematcher.minglematcherapp.dispatcher.stores;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public interface IStoreListener {
    void onNewState(IStore store, Action action);
}
