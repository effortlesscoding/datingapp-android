package com.minglematcher.minglematcherapp.dispatcher.actions;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class ActionUploadDone extends ActionUpload {

    public ActionUploadDone(EUploadType type, long videoId) {
        super(type, videoId);
    }
}
