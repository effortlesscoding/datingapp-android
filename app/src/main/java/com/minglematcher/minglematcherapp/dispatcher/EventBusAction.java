package com.minglematcher.minglematcherapp.dispatcher;

/**
 * Created by AnhAcc on 4/16/2016.
 */
public enum EventBusAction {
    ACTION_GET_PROFILES_PREVIEWS_WITH_VIDEOS,
    ACTION_GET_NEXT_PROFILES_TO_REVIEW_AND_THEIR_VIDEOS,
    ACTION_LIKE_PROFILE,
    ACTION_PASS_PROFILE,
    PROFILE_VIDEO_RECORDING_DONE,
    PROFILE_VIDEO_UPLOAD_DONE
}
