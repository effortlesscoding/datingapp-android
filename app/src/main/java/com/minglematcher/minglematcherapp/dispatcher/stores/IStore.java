package com.minglematcher.minglematcherapp.dispatcher.stores;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public abstract class IStore {

    protected List<WeakReference<IStoreListener>> mListeners = new ArrayList<>();
    protected abstract void onStateReduced(Action action);

    public IStore addListener(IStoreListener listener) {
        mListeners.add(new WeakReference<IStoreListener>(listener));
        return this;
    }

    public void removeListener(IStoreListener listener) {
        mListeners.remove(listener);
    }
}
