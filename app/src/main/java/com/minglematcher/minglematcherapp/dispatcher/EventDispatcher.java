package com.minglematcher.minglematcherapp.dispatcher;

/**
 * Created by AnhAcc on 4/16/2016.
 */
public class EventDispatcher { //implements IDownloadBroadcastDelegate {
    /*
    private static final String TAG = "EVENT_BUS";
    private class DownloadReceiver extends BroadcastReceiver {

        private void broadcastDownloadFailure() {
            onDownloadFailed(null, "Some reason");
        }

        private void broadCastDownloadSuccess(VideoDownloadParams params) {
            onDownloaded(params);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                int resultCode = bundle.getInt(VideoDownloadService.RESULT);
                if (resultCode == Activity.RESULT_OK) {
                    Object params = bundle.getParcelable(VideoDownloadService.KEY_PROFILE_DOWNLOAD_PARAMS);
                    if (params instanceof VideoDownloadParams && resultCode == Activity.RESULT_OK) {
                        VideoDownloadParams downloadParams = (VideoDownloadParams) params;
                        broadCastDownloadSuccess(downloadParams);
                    } else {
                        broadcastDownloadFailure();
                    }
                } else {
                    broadcastDownloadFailure();
                }
            }
        }
    }
    private IDispatcherMatcherActivityStore mMatcherActivityStore;
    private IProfileVideoUploadDelegate mVideoUploadDelegate;
    private static EventDispatcher mInstance;
    private Context mContext;
    public static EventDispatcher getInstance() {
        if (mInstance == null) {
            mInstance = new EventDispatcher();
        }
        return mInstance;
    }

    private EventDispatcher() {
        mContext = MingleMatcherApplication.getContext();
        mContext.registerReceiver(new DownloadReceiver(), new IntentFilter(VideoDownloadService.NOTIFICATION));
        mMatcherActivityStore = MatcherActivityStore.getInstance();
        mVideoUploadDelegate = UserProfileSetupStore.getInstance();
    }
*/


    /**
     * Retrieve video frame image from given video path
     *
     * @param pVideoPath
     *            the video file path
     *
     * @return Bitmap - the bitmap is video frame image
     *
     * @throws Throwable
     */

    /*
    public static Bitmap retriveVideoFrameFromVideo(File pFile)
            throws Throwable {
        if (!pFile.exists()) {
            return null;
        }
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(pFile.getAbsolutePath());
            bitmap = mediaMetadataRetriever.getFrameAtTime(1500);
        } catch (Exception m_e) {
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String p_videoPath)"
                            + m_e.getMessage());
        } finally {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }


    public void dispatch (ActionFetchNextProfilesStart action) {
        // Do nothing
    }

    public void dispatch (ActionPassProfile action) {
        // Do nothing
    }

    public void dispatch (ActionLikeProfile action) {
        final ProfileSnippetData profile = action.getProfile();
        ServerApiService.getInstance().likeProfile(profile.getId(), new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mMatcherActivityStore.onDispatcherAction(new ActionLikeProfileDone(profile));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mMatcherActivityStore.onDispatcherAction(new ActionLikeProfileError(profile));
            }
        });
    }

    public void dispatch(ActionVideoUploadingDone action) {
        // Do nothing
    }
    */

    /**
     * TO_DO : action sucks.
     * @param uploadDoneAction
     */
    /*
    public void dispatch(ActionVideoUploadingStarted uploadDoneAction) {
        // Do nothing
        try {
            uploadDoneAction.thumbnail = retriveVideoFrameFromVideo(uploadDoneAction.file);
            if (uploadDoneAction.thumbnail != null) {
                mVideoUploadDelegate.onNewState(uploadDoneAction);
            } else {
                mVideoUploadDelegate.onNewState(new ActionVideoRecordingError("Could not get the thumbnail"));
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void dispatch(EventBusAction action, final ProfileSnippetData profileSnippet) {
        if (profileSnippet != null) {

            final long id = profileSnippet.getFacebookId();
            switch (action) {
                case ACTION_LIKE_PROFILE:
                    break;
                case ACTION_PASS_PROFILE:
                    ServerApiService.getInstance().passProfile(id, new Callback() {
                        @Override
                        public void onResponse(Call call, Response response) {
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {
                            // Hm...
                        }
                    });
                    break;
                default:
                    break;
            }
        }
    }

    public void dispatch(EventBusAction action, List<ProfileSnippetData> snippetData) {
        switch (action) {
            case ACTION_GET_PROFILES_PREVIEWS_WITH_VIDEOS:
                startPredownloadVideos(snippetData);
                break;
            case ACTION_GET_NEXT_PROFILES_TO_REVIEW_AND_THEIR_VIDEOS:
                // Gotta do this one, too
                // How to implement the callback ?
                break;
            default:
                break;
        }
    }

    private void startPredownloadVideos(List<ProfileSnippetData> snippetData) {
        VideoDownloadParams[] downloadParams = new VideoDownloadParams[snippetData.size()];
        for (int i = 0; i < snippetData.size(); i++) {
            downloadParams[i] = new VideoDownloadParams(snippetData.get(i));
        }
        Context c = MingleMatcherApplication.getContext();
        Intent intent = new Intent(c, VideoDownloadService.class);
        Bundle b = new Bundle();
        b.putParcelableArray(VideoDownloadService.KEY_DOWNLOAD_PARAMS, downloadParams);
        intent.putExtras(b);
        c.startService(intent);
    }

    @Override
    public void onDownloaded(VideoDownloadParams params) {
        mMatcherActivityStore.onVideoDownloaded(params);
    }

    @Override
    public void onDownloadFailed(VideoDownloadParams params, String reason) {
        mMatcherActivityStore.onVideoDownloadFailed(params, new Exception(reason));
    }*/
}
