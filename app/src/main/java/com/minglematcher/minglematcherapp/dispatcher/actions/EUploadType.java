package com.minglematcher.minglematcherapp.dispatcher.actions;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public enum EUploadType {
    PROFILE_VIDEO, CHAT_VIDEO,
    PROFILE_THUMBNAIL, CHAT_THUMBNAIL
}
