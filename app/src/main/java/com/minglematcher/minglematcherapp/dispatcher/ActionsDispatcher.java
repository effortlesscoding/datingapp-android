package com.minglematcher.minglematcherapp.dispatcher;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionVideoDownloadDone;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionVideoDownloadError;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;
import com.minglematcher.minglematcherapp.services.parameters.VideoDownloadParams;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class ActionsDispatcher {
    private class DownloadReceiver extends BroadcastReceiver {

        private void broadcastDownloadFailure() {
           // onDownloadFailed(null, "Some reason");
            post(new ActionVideoDownloadError());
        }

        private void broadCastDownloadSuccess(VideoDownloadParams params) {
           // onDownloaded(params);
            post(new ActionVideoDownloadDone(params));
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                int resultCode = bundle.getInt(VideoDownloadService.RESULT);
                if (resultCode == Activity.RESULT_OK) {
                    Object params = bundle.getParcelable(VideoDownloadService.KEY_PROFILE_DOWNLOAD_PARAMS);
                    if (params instanceof VideoDownloadParams && resultCode == Activity.RESULT_OK) {
                        VideoDownloadParams downloadParams = (VideoDownloadParams) params;
                        broadCastDownloadSuccess(downloadParams);
                    } else {
                        broadcastDownloadFailure();
                    }
                } else {
                    broadcastDownloadFailure();
                }
            }
        }
    }

    private static final ActionsDispatcher mInstance = new ActionsDispatcher();

    private static Subject<Object, Object> mBusSubject = new SerializedSubject<>(PublishSubject.create());

    private ActionsDispatcher() {
        MingleMatcherApplication.getContext()
                .registerReceiver(new DownloadReceiver(), new IntentFilter(VideoDownloadService.NOTIFICATION));
    }

    public static ActionsDispatcher getBus() {
        return mInstance;
    }

    public Subscription subscribe(Action1<Object> onNext) {
        return mBusSubject.subscribe(onNext, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    public void post(Action event) {
        mBusSubject.onNext(event);
    }

}
