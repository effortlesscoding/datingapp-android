package com.minglematcher.minglematcherapp.dispatcher.actions;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class ActionUpload extends Action {
    protected EUploadType mType;
    protected long mVideoId;

    protected ActionUpload(EUploadType type, long videoId) {
        mType = type;
        mVideoId = videoId;
    }

    public long getVideoId() {
        return mVideoId;
    }

    public EUploadType getUploadType() {
        return mType;
    }

}
