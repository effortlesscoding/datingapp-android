package com.minglematcher.minglematcherapp.dispatcher.actions;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class ActionUploadError extends ActionUpload {


    public String reason;

    public ActionUploadError(EUploadType type, long videoId) {
        super(type, videoId);
    }

    public ActionUploadError buildError(String reason) {
        ActionUploadError error = new ActionUploadError(mType, mVideoId);
        error.reason = reason;
        return error;
    }

}
