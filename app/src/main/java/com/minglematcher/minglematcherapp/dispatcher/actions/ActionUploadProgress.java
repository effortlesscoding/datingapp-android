package com.minglematcher.minglematcherapp.dispatcher.actions;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class ActionUploadProgress extends ActionUpload {


    public double progress;

    public ActionUploadProgress(EUploadType type, long videoId) {
        super(type, videoId);
    }

    public ActionUploadProgress buildProgress(double progress) {
        ActionUploadProgress action = new ActionUploadProgress(mType, mVideoId);
        action.progress = progress;
        return action;
    }

}
