package com.minglematcher.minglematcherapp.models.user;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ProfileVideo {
    public long _id;
    public String fileName;
    public String thumbnail;
    public Boolean uploaded;
    public String storagePath;
    public long updatedAt;
    public long createdAt;
}
