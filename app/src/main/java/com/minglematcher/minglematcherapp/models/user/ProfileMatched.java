package com.minglematcher.minglematcherapp.models.user;

import com.minglematcher.minglematcherapp.viewmodels.user.Conversation;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ProfileMatched {
    public long _id;
    public long updatedAt;
    public long matchedAt;
    public String conversationId;
}
