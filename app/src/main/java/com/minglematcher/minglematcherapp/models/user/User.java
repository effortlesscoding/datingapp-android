package com.minglematcher.minglematcherapp.models.user;

import com.minglematcher.minglematcherapp.viewmodels.user.ProfileMatchData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfilePersonalData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;

import java.util.List;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class User<PersonalInfoClass extends PersonalInformation,
        ProfileToReviewClass extends Profile, ProfileMatchClass extends ProfileMatched> {
    public long _id;
    public int gender;
    public long facebookId;
    public long reviewdUpTo;
    public PersonalInfoClass profile;
    public List<ProfileToReviewClass> profilesToReview;
    public List<ProfileMatchClass> profilesMatched;

}
