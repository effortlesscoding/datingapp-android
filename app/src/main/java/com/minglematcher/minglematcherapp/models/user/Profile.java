package com.minglematcher.minglematcherapp.models.user;

import com.minglematcher.minglematcherapp.viewmodels.user.ProfilePersonalData;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class Profile<PersonalClass extends PersonalInformation> {
    public long _id;
    public long facebookId;
    public int gender;
    public PersonalClass profile;
    public long conversationId;
    public long updatedAt;
    public long createdAt;
}
