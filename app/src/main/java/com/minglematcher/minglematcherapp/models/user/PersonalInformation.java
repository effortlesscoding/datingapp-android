package com.minglematcher.minglematcherapp.models.user;

import com.minglematcher.minglematcherapp.viewmodels.user.*;
import com.minglematcher.minglematcherapp.models.user.ProfileVideo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class PersonalInformation<VideoClass extends ProfileVideo> {
    public String firstName;
    public String lastName;
    public String email;
    public List<VideoClass> videos = new ArrayList<>();
}
