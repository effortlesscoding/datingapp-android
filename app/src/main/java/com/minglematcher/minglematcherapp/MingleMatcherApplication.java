package com.minglematcher.minglematcherapp;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.minglematcher.minglematcherapp.services.IDownloadBroadcastDelegate;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;
import com.minglematcher.minglematcherapp.services.parameters.VideoDownloadParams;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by AnhAcc on 2/15/2016.
 */
public class MingleMatcherApplication extends Application {
    private static class PortraitModeEnforcement implements ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }
    public static final int REQUEST_KEY_ACTIVITY = 1;
    public static final int REQUEST_KEY_CAMERA = 0;
    public static final String KEY_EXTRA = "activity_key";
    public static final int RESULT_PASSED = 99;
    public static final int RESULT_MATCH = 100;
    public static final int RESULT_PROFILE_FINISHED = 101;
    public static final int RESULT_PHOTO_PICKED = 101;
    private static MingleMatcherApplication mInstance;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        this.mContext = this.getApplicationContext();
        Log.i(this.getClass().toString(), "Initializing Facebook SDK");
        FacebookSdk.sdkInitialize(getApplicationContext());
        registerActivityLifecycleCallbacks(new PortraitModeEnforcement());
    }

    public static MingleMatcherApplication getInstance() {
        return mInstance;
    }

    public static Context getContext(){
        return mContext;
    }

}
