package com.minglematcher.minglematcherapp.activities.matcher.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileMatchData;

/**
 * Created by AnhAcc on 5/1/2016.
 */
public class ActionLikeProfileMatched extends Action {
    ProfileMatchData mMatchedData;

    public ActionLikeProfileMatched(ProfileMatchData md) {
        mMatchedData = md;
    }

    public ProfileMatchData getMatchData() {
        return mMatchedData;
    }
}
