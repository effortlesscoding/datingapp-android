package com.minglematcher.minglematcherapp.activities.welcome.actionscreators;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.minglematcher.minglematcherapp.Constants;
import com.minglematcher.minglematcherapp.activities.instructions.InstructionsActivity;
import com.minglematcher.minglematcherapp.activities.welcome.actions.ActionLoginDone;
import com.minglematcher.minglematcherapp.activities.welcome.actions.ActionLoginError;
import com.minglematcher.minglematcherapp.dispatcher.ActionsDispatcher;
import com.minglematcher.minglematcherapp.services.ServerApiService;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;
import com.minglematcher.minglematcherapp.services.responses.GetProfilesInfoResponse;
import com.minglematcher.minglematcherapp.services.responses.LoginResponse;
import com.minglematcher.minglematcherapp.viewmodels.user.User;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ACLogin {
    ActionsDispatcher mDispatcher = ActionsDispatcher.getBus();
    UserData mUserData;
    private class UserLoginCallback implements Callback<LoginResponse> {
        @Override
        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
            LoginResponse body = response.body();
            ServerApiService.getInstance().setAccessTokenHeader(mAccessToken);
            mUserData = ((LoginResponse)body).getData();
            ServerApiService.getInstance()
                    .getProfilesInformation(mUserData.getProfilesToReview(),
                            new GetProfilesInformationCallback());
        }

        @Override
        public void onFailure(Call call, Throwable t) {
            mDispatcher.post(new ActionLoginError(t.getMessage()));
        }
    }

    private class GetProfilesInformationCallback implements Callback<GetProfilesInfoResponse> {
        @Override
        public void onResponse(Call<GetProfilesInfoResponse> call,
                               Response<GetProfilesInfoResponse> response) {
            GetProfilesInfoResponse body = response.body();
            VideoDownloadService vds = new VideoDownloadService();
            //vds.deleteProfiles();
            GetProfilesInfoResponse res = (GetProfilesInfoResponse) body;
            mUserData.setProfilesToReview(res.getData());
            if (mUserData.getProfilesMatched().size() > 0) {
                ServerApiService.getInstance()
                        .getProfilesMatchesInformation(mUserData.getProfilesMatched(),
                                new GetProfilesMatchesInformationCallback());
            } else {
                mDispatcher.post(new ActionLoginDone(mUserData));
            }
        }

        @Override
        public void onFailure(Call call, Throwable t) {
            mDispatcher.post(new ActionLoginError(t.getMessage()));
        }
    }

    private class GetProfilesMatchesInformationCallback implements Callback<GetProfilesInfoResponse> {

        @Override
        public void onResponse(Call<GetProfilesInfoResponse> call,
                               Response<GetProfilesInfoResponse> response) {
            GetProfilesInfoResponse responseBody = response.body();
            if (responseBody != null) {
                mUserData.fillMatchesData(responseBody.getData());
                mDispatcher.post(new ActionLoginDone(mUserData));
            } else {
                mDispatcher.post(new ActionLoginError("Could not get any matches profiles information."));
            }
        }

        @Override
        public void onFailure(Call<GetProfilesInfoResponse> call, Throwable t) {
            mDispatcher.post(new ActionLoginError(t.getMessage()));
        }
    }

    private String mAccessToken;

    public ACLogin(String accessToken) {
        mAccessToken = accessToken;
    }

    public void login() {

        ServerApiService service = ServerApiService.getInstance();
        service.loginToFb(mAccessToken, new UserLoginCallback());
    }
}
