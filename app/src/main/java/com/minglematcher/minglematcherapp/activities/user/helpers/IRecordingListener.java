package com.minglematcher.minglematcherapp.activities.user.helpers;

/**
 * Created by AnhAcc on 3/26/2016.
 */
public interface IRecordingListener {
    void recordingStarted();
    void recordingResumed();
}
