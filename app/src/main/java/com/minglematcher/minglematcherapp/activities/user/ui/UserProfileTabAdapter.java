package com.minglematcher.minglematcherapp.activities.user.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.user.UserProfileVideoRecordingActivity;
import com.minglematcher.minglematcherapp.activities.user.actions.ActionProfileVideoFinishConfirmed;
import com.minglematcher.minglematcherapp.activities.user.actions.ActionProfileVideoRecordingDone;
import com.minglematcher.minglematcherapp.activities.user.store.UserProfileSetupStore;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadDone;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadError;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadProgress;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStoreListener;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileVideo;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;
import com.minglematcher.minglematcherapp.ui.CircularProgressBar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class UserProfileTabAdapter extends FragmentStatePagerAdapter {
    public static final String VIDEO_ID_KEY = "VIDEO_ID";
    private final String TAG = "USER_PROFILE_TAB_ADAPTER";
    public static class VideoFragment extends Fragment implements IStoreListener {
        private final String TAG = "VIDEO_FRAGMENT";

        private void setUploadDone(IStore store, ActionUploadDone action) {
            Log.d(TAG, "Looking for a progress bar for a video with id #" + action.getVideoId());
            CircularProgressBar bar = findProgressBarById(action.getVideoId());
            if (bar != null) {
                bar.setProgress(1.0);
            } else {
                Log.e(TAG, "Could not find a progress bar for a video with id #" + action.getVideoId());
            }
        }

        private void showError(IStore store, ActionUploadError action) {
            Log.d(TAG, "Looking for a progress bar for a video with id #" + action.getVideoId());
            CircularProgressBar bar = findProgressBarById(action.getVideoId());
            if (bar != null) {
                bar.removeThumbnail();
            } else {
                Log.e(TAG, "Could not find a progress bar for a video with id #" + action.getVideoId());
            }
        }
        private void updateProgress(IStore store, ActionUploadProgress action) {
            Log.d(TAG, "Looking for a progress bar for a video with id #" + action.getVideoId());
            CircularProgressBar bar = findProgressBarById(action.getVideoId());
            if (bar != null) {
                bar.setProgress(action.progress);
            } else {
                Log.e(TAG, "Could not find a progress bar for a video with id #" + action.getVideoId());
            }
        }

        private CircularProgressBar findProgressBarById(long videoId) {
            for (int i = 0; i < mVideoPlaceholders.size(); i++) {
                CircularProgressBar bar = mVideoPlaceholders.get(i);
                Log.d(TAG, "Comparing " + bar.getTag() + " with " + videoId);
                if (bar.getTag().toString().equals(videoId + "")) {
                    return bar;
                }
            }
            return null;
        }
        private void setThumbnail(IStore store, ActionProfileVideoRecordingDone action) {
            Log.d(TAG, "Looking for a progress bar for a video with id #" + action.videoId);
            CircularProgressBar bar = findProgressBarById(action.videoId);
            if (bar != null) {
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bar.setThumbnail(
                        BitmapFactory
                                .decodeFile(action.thumbnailFile.getAbsolutePath(), bmOptions)
                );
            } else {
                Log.e(TAG, "Could not find a progress bar for a video with id #" + action.videoId);
            }
        }

        @Override
        public void onNewState(IStore storeParam, Action actionParam) {
            final IStore store = storeParam;
            final Action action = actionParam;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (action instanceof ActionUploadDone) {
                        setUploadDone(store, (ActionUploadDone)action);
                    } else if (action instanceof ActionUploadError) {
                        // Later. Just display an error message on top of the icon.
                        showError(store, (ActionUploadError) action);
                    } else if (action instanceof ActionUploadProgress) {
                        updateProgress(store, (ActionUploadProgress) action);
                    } else if (action instanceof ActionProfileVideoRecordingDone) {
                        setThumbnail(store, (ActionProfileVideoRecordingDone) action);
                    } else if (action instanceof ActionProfileVideoFinishConfirmed) {
                        if (mStore.getUser().isProfileComplete()) {
                            mFinishButton.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        }

        private class VideoPlaceholderClickListener implements View.OnClickListener {

            @Override
            public void onClick(View v) {
                CharSequence choices[] = new CharSequence[]{"Record now", "Upload from Gallery"};
                Object tag = v.getTag();
                long tagLongTemp = 0;
                if (!(tag instanceof Integer) && !(tag instanceof Long)) {
                    if (tag instanceof String) {
                        try {
                            tagLongTemp = Long.parseLong((String)tag);
                        } catch(NumberFormatException exc) {
                            Log.e(TAG, "Tag is not an integer? " + tag);
                            return;
                        }
                    } else {
                        Log.e(TAG, "Tag is not an integer? " + tag);
                        return;
                    }
                } else {
                    if (tag instanceof Integer) {
                        tagLongTemp = (Integer)tag;
                    } else if (tag instanceof Long) {
                        tagLongTemp = (Long)tag;
                    }
                }
                final long tagLong = tagLongTemp;
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Create a profile to increase your chances of being liked");
                builder.setItems(choices, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent intent = new Intent(getContext(), UserProfileVideoRecordingActivity.class);
                                Bundle b = new Bundle();
                                b.putLong(VIDEO_ID_KEY, tagLong);
                                intent.putExtras(b);
                                startActivity(intent);
                                break;
                            case 1:
                                Intent intentSelectVideo = new Intent();
                                intentSelectVideo.setType("video/*");
                                intentSelectVideo.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intentSelectVideo, "Select a Video "), 33);
                                break;
                        }
                    }
                });
                builder.show();
            }
        }

        private List<CircularProgressBar> mVideoPlaceholders = new ArrayList<>();
        private Button mFinishButton;

        public VideoFragment() {

        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        UserProfileSetupStore mStore;
        /**
         * The Fragment's UI is just a simple text view showing its
         * instance number.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            UserProfileSetupStore store = UserProfileSetupStore.getInstance();
            mStore = (UserProfileSetupStore)store.addListener(this);
            View v = inflater.inflate(R.layout.f_user_profile_video, container, false);
            mFinishButton = (Button)v.findViewById(R.id.fupaFinishProfile);
            mFinishButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
            mVideoPlaceholders.add(((CircularProgressBar)v.findViewById(R.id.fupaVideoPlaceholder1)));
            mVideoPlaceholders.add(((CircularProgressBar)v.findViewById(R.id.fupaVideoPlaceholder2)));
            mVideoPlaceholders.add(((CircularProgressBar)v.findViewById(R.id.fupaVideoPlaceholder3)));
            mVideoPlaceholders.add(((CircularProgressBar)v.findViewById(R.id.fupaVideoPlaceholder4)));
            mVideoPlaceholders.add(((CircularProgressBar)v.findViewById(R.id.fupaVideoPlaceholder5)));
            mVideoPlaceholders.add(((CircularProgressBar)v.findViewById(R.id.fupaVideoPlaceholder6)));
            /**
             * Set up the view now. Probably, the profile videos will have a field called "position"
             * Or more like, bleh : P
             * **/
            VideoPlaceholderClickListener listener = new VideoPlaceholderClickListener();
            for (CircularProgressBar bar : mVideoPlaceholders) {
                bar.setOnClickListener(listener);
            }
            setupViewFromStore();
            return v;
        }

        public void setupViewFromStore() {
            UserData user = mStore.getUser();
            if (user.isProfileComplete()) {
                mFinishButton.setVisibility(View.VISIBLE);
            }
            List<ProfileVideo> videos = user.getProfile().getVideos();
            // for 0... last videos
            for (int i = 0; i < videos.size(); i++) {
                if (i < mVideoPlaceholders.size()) {
                    ProfileVideo videoParams = videos.get(i);
                    CircularProgressBar videoPlaceholder = mVideoPlaceholders.get(i);
                    Log.d(TAG, "Placeholder #" + i + " gets a video with id : " + videoParams.getId());
                    videoPlaceholder.setTag(videoParams.getId());
                    // Must be set in one of the reducers on "Recording Done" action
                    if (videoParams.getLocalData().thumbnailPath != null) {
                        File f = new File(videoParams.getLocalData().thumbnailPath);
                        if (f.exists()) {
                            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                            videoPlaceholder.setThumbnail(BitmapFactory
                                    .decodeFile(f.getAbsolutePath(), bmOptions));
                            if (videoParams.getLocalData().uploadProgress != -1) {
                                videoPlaceholder.setProgress(videoParams.getLocalData().uploadProgress);
                            }
                        }
                    }
                }
            }
            for (int k = videos.size(); k < mVideoPlaceholders.size(); k++) {
                mVideoPlaceholders.get(k).setTag(k);
            }
        }


        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }
/*
        private void setUploadDone(DAVideoUploadDone params) {
            String tag = params.viewTag;
            User user = UserProfileSetupStore.getInstance().getUser();
            if (user.isProfileComplete()) {
                mFinishButton.setVisibility(View.VISIBLE);
            }
            Log.i("TAG_CHECK", "Tag is " + tag);
            Log.i("TAG_CHECK", "Img 1's tag is " + mVideoPlaceholder1.getTag() );
            Log.i("TAG_CHECK", "Img 2's tag is " + mVideoPlaceholder2.getTag() );
            Log.i("TAG_CHECK", "Img 3's tag is " + mVideoPlaceholder3.getTag() );
            Log.i("TAG_CHECK", "Img 4's tag is " + mVideoPlaceholder4.getTag() );
            Log.i("TAG_CHECK", "Img 5's tag is " + mVideoPlaceholder5.getTag() );
            Log.i("TAG_CHECK", "Img 6's tag is " + mVideoPlaceholder6.getTag() );
            if (mVideoPlaceholder1 != null && mVideoPlaceholder1.getTag().equals(tag)) {
                mVideoPlaceholder1.setProgress(1);
            } else if (mVideoPlaceholder2 != null && mVideoPlaceholder2.getTag().equals(tag)) {
                mVideoPlaceholder2.setProgress(1);
            } else if (mVideoPlaceholder3 != null && mVideoPlaceholder3.getTag().equals(tag)) {
                mVideoPlaceholder3.setProgress(1);
            } else if (mVideoPlaceholder4 != null && mVideoPlaceholder4.getTag().equals(tag)) {
                mVideoPlaceholder4.setProgress(1);
            } else if (mVideoPlaceholder5 != null && mVideoPlaceholder5.getTag().equals(tag)) {
                mVideoPlaceholder5.setProgress(1);
            } else if (mVideoPlaceholder6 != null && mVideoPlaceholder6.getTag().equals(tag)) {
                mVideoPlaceholder6.setProgress(1);
            }
        }

        private void updateUploadProgress(DAVideoUploadProgress params) {
            String tag = params.viewTag;
            double percentage = params.percentage;
            Log.i("TAG_CHECK", "Tag is " + tag);
            Log.i("TAG_CHECK", "Img 1's tag is " + mVideoPlaceholder1.getTag() );
            Log.i("TAG_CHECK", "Img 2's tag is " + mVideoPlaceholder2.getTag() );
            Log.i("TAG_CHECK", "Img 3's tag is " + mVideoPlaceholder3.getTag() );
            Log.i("TAG_CHECK", "Img 4's tag is " + mVideoPlaceholder4.getTag() );
            Log.i("TAG_CHECK", "Img 5's tag is " + mVideoPlaceholder5.getTag() );
            Log.i("TAG_CHECK", "Img 6's tag is " + mVideoPlaceholder6.getTag() );
            if (mVideoPlaceholder1 != null && mVideoPlaceholder1.getTag().equals(tag)) {
                mVideoPlaceholder1.setProgress(percentage);
            } else if (mVideoPlaceholder2 != null && mVideoPlaceholder2.getTag().equals(tag)) {
                mVideoPlaceholder2.setProgress(percentage);
            } else if (mVideoPlaceholder3 != null && mVideoPlaceholder3.getTag().equals(tag)) {
                mVideoPlaceholder3.setProgress(percentage);
            } else if (mVideoPlaceholder4 != null && mVideoPlaceholder4.getTag().equals(tag)) {
                mVideoPlaceholder4.setProgress(percentage);
            } else if (mVideoPlaceholder5 != null && mVideoPlaceholder5.getTag().equals(tag)) {
                mVideoPlaceholder5.setProgress(percentage);
            } else if (mVideoPlaceholder6 != null && mVideoPlaceholder6.getTag().equals(tag)) {
                mVideoPlaceholder6.setProgress(percentage);
            }
        }

        private void setUploadThumbnail(DAVideoUploadStarted params) {
            String tag = params.getTag();
            Bitmap thumbnail = params.getThumbnail();
            Log.i("TAG_CHECK", "Tag is " + tag);
            Log.i("TAG_CHECK", "Img 1's tag is " + mVideoPlaceholder1.getTag() );
            Log.i("TAG_CHECK", "Img 2's tag is " + mVideoPlaceholder2.getTag() );
            Log.i("TAG_CHECK", "Img 3's tag is " + mVideoPlaceholder3.getTag() );
            Log.i("TAG_CHECK", "Img 4's tag is " + mVideoPlaceholder4.getTag() );
            Log.i("TAG_CHECK", "Img 5's tag is " + mVideoPlaceholder5.getTag() );
            Log.i("TAG_CHECK", "Img 6's tag is " + mVideoPlaceholder6.getTag() );
            if (mVideoPlaceholder1 != null && mVideoPlaceholder1.getTag().equals(tag)) {
                mVideoPlaceholder1.setThumbnail(thumbnail);
            } else if (mVideoPlaceholder2 != null && mVideoPlaceholder2.getTag().equals(tag)) {
                mVideoPlaceholder2.setThumbnail(thumbnail);
            } else if (mVideoPlaceholder3 != null && mVideoPlaceholder3.getTag().equals(tag)) {
                mVideoPlaceholder3.setThumbnail(thumbnail);
            } else if (mVideoPlaceholder4 != null && mVideoPlaceholder4.getTag().equals(tag)) {
                mVideoPlaceholder4.setThumbnail(thumbnail);
            } else if (mVideoPlaceholder5 != null && mVideoPlaceholder5.getTag().equals(tag)) {
                mVideoPlaceholder5.setThumbnail(thumbnail);
            } else if (mVideoPlaceholder6 != null && mVideoPlaceholder6.getTag().equals(tag)) {
                mVideoPlaceholder6.setThumbnail(thumbnail);
            }
        }*/
    }

    public static class ProfileSecondFragment extends Fragment {

        public ProfileSecondFragment() {

        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        /**
         * The Fragment's UI is just a simple text view showing its
         * instance number.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.f_user_profile_2, container, false);
            return v;
        }


        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }
    }

    public static class ProfileThirdFragment extends Fragment {

        public ProfileThirdFragment() {

        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        /**
         * The Fragment's UI is just a simple text view showing its
         * instance number.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.f_user_profile_3, container, false);
            return v;
        }


        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

    }

    public static class TabFragment extends Fragment {
        private UserProfileViewModel mVm;

        public static TabFragment newFragment(UserProfileViewModel vm) {
            TabFragment tf = new TabFragment();
            tf.mVm = vm;
            return tf;
        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        /**
         * The Fragment's UI is just a simple text view showing its
         * instance number.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.user_profile_tab, container, false);
            if (mVm != null) {

                TextView textView = (TextView)v.findViewById(R.id.uptHeadline);
                textView.setText(mVm.getTitle());
                ViewGroup choicesContainer = (ViewGroup)v.findViewById(R.id.uptChoices);
                List<UserProfileViewModel.Choice> choices = mVm.getmChoices();
                for (int i = 0; i < choices.size(); i++ ){
                    CheckBox cb = new CheckBox(this.getContext());
                    cb.setText(choices.get(i).getValue());
                    choicesContainer.addView(cb);
                }
            }
            return v;
        }


        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

    }

    public UserProfileTabAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new VideoFragment();
            case 1:
                return new ProfileSecondFragment();
            case 2:
                return new ProfileThirdFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

}

