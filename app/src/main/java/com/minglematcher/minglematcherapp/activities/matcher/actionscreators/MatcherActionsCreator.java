package com.minglematcher.minglematcherapp.activities.matcher.actionscreators;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionLikeProfileDone;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionLikeProfileError;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionLikeProfileMatched;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionPassProfileDone;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionPassProfileError;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionThumbnailAvailable;
import com.minglematcher.minglematcherapp.dispatcher.ActionsDispatcher;
import com.minglematcher.minglematcherapp.utils.BitmapLoader;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileVideo;
import com.minglematcher.minglematcherapp.services.responses.GetSignedUrlResponse;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;
import com.minglematcher.minglematcherapp.services.ServerApiService;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;
import com.minglematcher.minglematcherapp.services.parameters.VideoDownloadParams;
import com.minglematcher.minglematcherapp.services.responses.LikeResponse;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class MatcherActionsCreator {

    private final String TAG = "MATCHER_ACTIONS";
    private class PassProfileCallback implements Callback {
        private ProfileSnippetData mProfile;

        public PassProfileCallback(ProfileSnippetData profile) {
            mProfile = profile;
        }

        @Override
        public void onResponse(Call call, Response response) {
            ActionPassProfileDone action = new ActionPassProfileDone(mProfile);
            ActionsDispatcher.getBus().post(action);
        }

        @Override
        public void onFailure(Call call, Throwable t) {
            ActionPassProfileError action = new ActionPassProfileError(mProfile);
            ActionsDispatcher.getBus().post(action);
        }
    }

    private class LikeProfileCallback implements Callback {
        private ProfileSnippetData mProfile;

        public LikeProfileCallback(ProfileSnippetData profile) {
            mProfile = profile;
        }

        @Override
        public void onResponse(Call call, Response response) {
            if (response.body() instanceof LikeResponse) {
                LikeResponse r = (LikeResponse)response.body();
                if (r.getData().isMutual() && r.getData().getMatchData() != null) {
                    ActionLikeProfileMatched action = new ActionLikeProfileMatched(r.getData().getMatchData());
                    ActionsDispatcher.getBus().post(action);
                } else {
                    ActionLikeProfileDone action = new ActionLikeProfileDone(mProfile, r);
                    ActionsDispatcher.getBus().post(action);
                }
            } else {
                ActionLikeProfileError action = new ActionLikeProfileError(mProfile);
                ActionsDispatcher.getBus().post(action);
            }
        }

        @Override
        public void onFailure(Call call, Throwable t) {
            ActionLikeProfileError action = new ActionLikeProfileError(mProfile);
            ActionsDispatcher.getBus().post(action);
        }
    }

    public MatcherActionsCreator() {}


    public void downloadProfileVideos(List<ProfileSnippetData> profilesNeedingVideos) {
        VideoDownloadParams[] downloadParams = new VideoDownloadParams[profilesNeedingVideos.size()];
        for (int i = 0; i < profilesNeedingVideos.size(); i++) {
            downloadParams[i] = new VideoDownloadParams(profilesNeedingVideos.get(i));
        }
        Context c = MingleMatcherApplication.getContext();
        Intent intent = new Intent(c, VideoDownloadService.class);
        Bundle b = new Bundle();
        b.putParcelableArray(VideoDownloadService.KEY_DOWNLOAD_PARAMS, downloadParams);
        intent.putExtras(b);
        c.startService(intent);
    }

    public void createConversation(ProfileSnippetData withWhom) {

    }

    public void likeProfile(final ProfileSnippetData profile) {
        ServerApiService.getInstance()
                .likeProfile(profile.getId(), new LikeProfileCallback(profile));
    }

    public void passProfile(final ProfileSnippetData profile) {
        ServerApiService.getInstance()
                .passProfile(profile.getId(), new PassProfileCallback(profile));
    }

}
