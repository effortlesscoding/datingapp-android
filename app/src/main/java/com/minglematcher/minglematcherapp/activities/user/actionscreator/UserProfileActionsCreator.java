package com.minglematcher.minglematcherapp.activities.user.actionscreator;

import android.util.Log;

import com.minglematcher.minglematcherapp.activities.user.actions.ActionProfileVideoFinishConfirmed;
import com.minglematcher.minglematcherapp.activities.user.actions.ActionProfileVideoRecordingDone;
import com.minglematcher.minglematcherapp.dispatcher.ActionsDispatcher;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadDone;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadError;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadProgress;
import com.minglematcher.minglematcherapp.dispatcher.actions.EUploadType;
import com.minglematcher.minglematcherapp.services.ServerApiService;
import com.minglematcher.minglematcherapp.services.requests.FinishUploadVideoRequest;
import com.minglematcher.minglematcherapp.services.responses.FinishUploadVideoResponse;
import com.minglematcher.minglematcherapp.services.uploading.UploadParams;
import com.minglematcher.minglematcherapp.services.uploading.VideoUploadTask;
import com.minglematcher.minglematcherapp.services.requests.RequestGetProfileVideoUploadUrls;
import com.minglematcher.minglematcherapp.services.responses.ResponseGetProfileVideoUploadUrls;
import com.minglematcher.minglematcherapp.utils.FilenameUtils;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class UserProfileActionsCreator {

    private ActionsDispatcher mDispatcher = ActionsDispatcher.getBus();
    private String TAG = "UserProfileActionsCreator";

    private class GetProfileVideoUploadUrlsCallback
            implements Callback<ResponseGetProfileVideoUploadUrls> {

        @Override
        public void onResponse(Call<ResponseGetProfileVideoUploadUrls> call,
                               Response<ResponseGetProfileVideoUploadUrls> response) {
            // No need to save these urls, I guess!!!
            if (response.code() == 200) {
                ResponseGetProfileVideoUploadUrls.Data urls = response.body().data;
                if (urls != null && urls.video != null && urls.thumbnail != null) {
                    // Start the uploading service for both the video and the thumbnail
                    // This task will also dispatch progress, error and done events instead of the
                    // action creator.
                    UploadParams thumbnailParams =
                            new UploadParams(mThumbnailFile, urls.thumbnail.mimeType, urls.thumbnail.url);

                    UploadParams videoParams =
                            new UploadParams(mVideoFile, urls.video.mimeType, urls.video.url);
                    VideoUploadTask.build()
                            .setThumbnailActions(new ActionUploadProgress(EUploadType.PROFILE_THUMBNAIL, mVideoId),
                                    new ActionUploadError(EUploadType.PROFILE_THUMBNAIL, mVideoId),
                                    new ActionUploadDone(EUploadType.PROFILE_THUMBNAIL, mVideoId))
                            .setVideoActions(new ActionUploadProgress(EUploadType.PROFILE_VIDEO, mVideoId),
                                    new ActionUploadError(EUploadType.PROFILE_VIDEO, mVideoId),
                                    new ActionUploadDone(EUploadType.PROFILE_VIDEO, mVideoId))
                            .execute(videoParams, thumbnailParams);
                    return;
                }
            }
            mDispatcher.post(new ActionUploadError(EUploadType.PROFILE_VIDEO, mVideoId)
                    .buildError("Could not get upload urls."));

        }

        @Override
        public void onFailure(Call<ResponseGetProfileVideoUploadUrls> call, Throwable t) {
            mDispatcher.post(new ActionUploadError(EUploadType.PROFILE_VIDEO, mVideoId)
                    .buildError(t.getMessage()));
        }
    }

    public UserProfileActionsCreator(long videoId) {
        mVideoId = videoId;
    }

    private File mVideoFile;
    private long mVideoId;
    private File mThumbnailFile;
    public void uploadProfileVideo(final File videoFile, final File thumbnailFile) {

        if (!FilenameUtils.isCorrectExtension(videoFile.getName(), "mp4") ||
                !FilenameUtils.isCorrectExtension(thumbnailFile.getName(), "jpg")) {
            Log.e(TAG, "Problem uploading files because of wrong extensions.");
            return;
        }
        mVideoFile = videoFile;
        mThumbnailFile = thumbnailFile;
        ActionProfileVideoRecordingDone recordingDone = ActionProfileVideoRecordingDone.build(mVideoId)
                .setVideoFile(videoFile).setThumbnailFile(thumbnailFile);
        ActionsDispatcher.getBus().post(recordingDone);
        Log.d(TAG, "Thumbnail size is : " + mThumbnailFile.length());
        Log.d(TAG, "Video size is : " + mVideoFile.length());
        if (mVideoFile.length() > 0 && mThumbnailFile.length() > 0) {
            ServerApiService.getInstance().getProfileVideoUploadUrls(
                    new RequestGetProfileVideoUploadUrls(mVideoId, videoFile.length(), thumbnailFile.length()),
                    new GetProfileVideoUploadUrlsCallback());
        } else {
            mDispatcher.post(new ActionUploadError(EUploadType.PROFILE_VIDEO, mVideoId)
                    .buildError("Could not get upload urls because file sizes were 0."));
        }
    }

    public void finishVideoUpload() {
        ServerApiService.getInstance().finishVideoUpload(new FinishUploadVideoRequest(mVideoId),
                new Callback<FinishUploadVideoResponse>() {
                    @Override
                    public void onResponse(Call<FinishUploadVideoResponse> call,
                                           Response<FinishUploadVideoResponse> response) {
                        if (response.code() == 200) {
                            FinishUploadVideoResponse body = response.body();
                            // This contains the very latest profile video with the id and all
                            if (body.getData()._id == mVideoId) {
                                mDispatcher.post(new ActionProfileVideoFinishConfirmed(body.getData()));
                            } else {
                                mDispatcher.post(new ActionUploadError(EUploadType.PROFILE_VIDEO, mVideoId)
                                        .buildError("Wrong response video id : " + mVideoId +
                                                " vs " + body.getData()._id));
                            }
                        } else {
                            mDispatcher.post(new ActionUploadError(EUploadType.PROFILE_VIDEO, mVideoId)
                                    .buildError("Wrong response code : " + response.code()));
                        }
                    }

                    @Override
                    public void onFailure(Call<FinishUploadVideoResponse> call, Throwable t) {
                        mDispatcher.post(new ActionUploadError(EUploadType.PROFILE_VIDEO, mVideoId)
                                .buildError(t.getMessage()));
                    }
                });
    }
}
