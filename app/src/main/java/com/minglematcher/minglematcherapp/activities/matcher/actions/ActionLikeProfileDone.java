package com.minglematcher.minglematcherapp.activities.matcher.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;
import com.minglematcher.minglematcherapp.services.responses.LikeResponse;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class ActionLikeProfileDone extends Action {
    private ProfileSnippetData profile;
    private boolean mIsLiked;
    private boolean mIsMutual;


    public ActionLikeProfileDone(ProfileSnippetData profile, LikeResponse response) {
        this.profile = profile;
        this.mIsLiked = response.getData().isLiked();
        this.mIsMutual = response.getData().isMutual();
    }


    public boolean isMutual() {
        return mIsMutual;
    }

    public boolean isLiked() {
        return mIsLiked;
    }

    public ProfileSnippetData getProfile() {
        return profile;
    }
}
