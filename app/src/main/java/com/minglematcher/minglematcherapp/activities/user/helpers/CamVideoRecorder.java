package com.minglematcher.minglematcherapp.activities.user.helpers;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.util.Log;
import android.util.Size;
import android.view.SurfaceView;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 3/26/2016.
 */
@SuppressWarnings("deprecation")
public class CamVideoRecorder {
    private RecordingOptions mRecordingOptions;
    private String TAG = "PROFILE_VIDEO_RECORDER";
    private Camera mCamera;
    private ERecordState mCurrentRecordState;
    private WeakReference<SurfaceView> mPreviewSurface;
    private WeakReference<IRecordingListener> mRecordingDelegate;
    private MediaRecorder mRecorder;
    private final int DESIRED_WIDTH = 640, DESIRED_HEIGHT = 480;
    private int mCameraId;
    private List<Camera.Size> mSupportedPreviewSizes = new ArrayList<>();
    private List<String> mSupportedFlashModes = new ArrayList<>();
    private List<Camera.Size> mSupportedVideoSizes = new ArrayList<>();

    /*
    I think the main interface is "callbackListener / delegate"
    Initialise
    Re-initialise
    Start recording
    Pause recording
    Stop
    Dispose
     */
    public CamVideoRecorder(IRecordingListener recordingDelegate, SurfaceView surface,
                            RecordingOptions options) {
        // Maybe just hold it as weak reference, though
        mRecordingOptions = options;
        mPreviewSurface = new WeakReference<SurfaceView>(surface);
        mRecordingDelegate = new WeakReference<IRecordingListener>(recordingDelegate);
    }

    public void startPreview() {
        mCamera = initialiseAndChooseCamera();
        loadAvailableDimensions();
        startCameraPreview();
        mCurrentRecordState = ERecordState.OnStandByReady;
    }

    private void loadAvailableDimensions() {
        Camera.Parameters p = mCamera.getParameters();
        mSupportedPreviewSizes = p.getSupportedPreviewSizes();
        mSupportedFlashModes = p.getSupportedFlashModes();
        mSupportedVideoSizes = p.getSupportedVideoSizes();
        Camera.Size optimalPreviewSize = getOptimalVideoSize(mSupportedPreviewSizes, DESIRED_HEIGHT,
                DESIRED_WIDTH);
        p.setPreviewSize(optimalPreviewSize.width, optimalPreviewSize.height);
        p.setRecordingHint(true);
        mCamera.setParameters(p);
        if (mSupportedFlashModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
            mCamera.setParameters(parameters);
        }
    }

    private void startRecording(boolean unpause) {
        try {
            SurfaceView surfaceView = mPreviewSurface.get();
            IRecordingListener recordingDelegate = mRecordingDelegate.get();
            if (surfaceView != null && recordingDelegate != null) {
                initRecorder();
                File f = mRecordingOptions.getFile();
                if (!f.exists()) {
                    try {
                        f.createNewFile();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Log.i(TAG, "Setting output file to : " + f.getPath());
                mRecorder.setOutputFile(f.getAbsolutePath());
                mRecorder.setPreviewDisplay(surfaceView.getHolder().getSurface());
                mRecorder.prepare();
                mRecorder.start();
                if (!unpause) {
                    recordingDelegate.recordingStarted();
                } else {
                    recordingDelegate.recordingResumed();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void startCameraPreview() {
        try {
            SurfaceView surfaceView = mPreviewSurface.get();
            if (surfaceView != null) {
                mCamera.lock();
                mCamera.setDisplayOrientation(90);
                mCamera.setPreviewDisplay(surfaceView.getHolder());
                mCamera.startPreview();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ERecordState getRecordState() {
        return mCurrentRecordState;
    }

    private void initRecorder() {
        Log.i(TAG, "Initialized a recorder");
        mCamera.stopPreview();
        mCamera.unlock();
        Camera.Size optimalVideoSize = getOptimalVideoSize(mSupportedVideoSizes, DESIRED_HEIGHT,
                DESIRED_WIDTH);
        if (optimalVideoSize == null) { return; }
        mRecorder = new MediaRecorder();
        mRecorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
            @Override
            public void onError(MediaRecorder mr, int what, int extra) {
                int i = 0;
            }
        });
        mRecorder.setCamera(mCamera);
        //mRecorder.setVideoSize(optimalVideoSize.width, optimalVideoSize.height);
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        try {

            //CamcorderProfile cpHigh = CamcorderProfile.get(mCameraId, 0);
            CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
            /*cpHigh.fileFormat = MediaRecorder.OutputFormat.MPEG_4;
            cpHigh.videoCodec = MediaRecorder.VideoEncoder.MPEG_4_SP;
            cpHigh.audioCodec = MediaRecorder.AudioEncoder.AAC;
*/
            mRecorder.setProfile(cpHigh);
            mRecorder.setOrientationHint(270);
            mRecorder.setMaxDuration(mRecordingOptions.getMaxSeconds() * 1000);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void switchToRecordState(ERecordState newState) {
        switch (newState) {
            case Paused:
                if (mCurrentRecordState == ERecordState.Recording) {
                    stopRecording();
                }
                break;
            case OnStandByReady:
                if (mCurrentRecordState == ERecordState.Recording) {
                    stopRecording();
                }
                break;
            case Recorded:
                stopRecording();
                break;
            case Recording:
                if (mCurrentRecordState == ERecordState.OnStandByReady) {
                    Log.i(TAG, "Started recording.");
                    mCamera.stopPreview();
                    startRecording(false);
                } else if (mCurrentRecordState == ERecordState.Paused) {
                    Log.i(TAG, "Unpausing");
                    startRecording(true);
                }
                break;
        }
        mCurrentRecordState = newState;
    }

    public void stopRecording() {
        if (mRecorder != null) {
            try {
                mRecorder.stop();
                mRecorder.reset();
                mRecorder.release();
                mRecorder = null;
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    private Camera.Size getOptimalVideoSize(List<Camera.Size> availableSizes, int desiredHeight,
                                            int desiredWidth) {
        Camera.Size optimalSize = null;

        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) desiredHeight / desiredWidth;

        // Try to find a size match which suits the whole screen minus the menu on the left.
        for (Camera.Size size : availableSizes) {
            if (size.height > desiredHeight || size.width > desiredWidth) continue;
            optimalSize = size;
        }

        // If we cannot find the one that matches the aspect ratio, ignore the requirement.
        if (optimalSize == null) {
            // TODO : Backup in case we don't get a size.
        }

        return optimalSize;
    }

    private Camera initialiseAndChooseCamera() throws RuntimeException {
        Camera cam = null;
        try {
            int cameraCount = 0;
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            cameraCount = Camera.getNumberOfCameras();
            for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
                Camera.getCameraInfo(camIdx, cameraInfo);
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    cam = Camera.open(camIdx);
                    mCameraId = camIdx;
                }
            }

            // Throw an Exception
            if (cam == null) { throw new RuntimeException("Camera could not be found."); }

        } catch (Exception e) {
            Log.e(TAG, "Camera Error : ", e);
            e.printStackTrace();
        }
        return cam;
    }

    public void release() {
        try {
            if (mRecorder != null) {
                Log.d(TAG, "Recorder stop.");
                mRecorder.stop();
                Log.d(TAG, "Recorder reset.");
                mRecorder.reset();
                Log.d(TAG, "Recorder release.");
                mRecorder.release();
                mRecorder = null;
            }
            if (mCamera != null) {
                Log.d(TAG, "Camera unlock.");
                mCamera.unlock();
                Log.d(TAG, "Camera stop preview.");
                mCamera.stopPreview();
                Log.d(TAG, "Camera release.");
                mCamera.release();
                mCamera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}