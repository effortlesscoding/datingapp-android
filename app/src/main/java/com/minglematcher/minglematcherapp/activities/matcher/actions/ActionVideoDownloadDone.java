package com.minglematcher.minglematcherapp.activities.matcher.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.services.parameters.VideoDownloadParams;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class ActionVideoDownloadDone extends Action {
    private VideoDownloadParams mParams;

    public ActionVideoDownloadDone(VideoDownloadParams params) {
        mParams = params;
    }

    public VideoDownloadParams getParams() {
        return mParams;
    }
}
