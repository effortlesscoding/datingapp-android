package com.minglematcher.minglematcherapp.activities.welcome.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ActionLoginDone extends Action {

    public UserData user;

    public ActionLoginDone(UserData user) {
        this.user = user;
    }
}
