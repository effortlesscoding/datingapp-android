package com.minglematcher.minglematcherapp.activities.welcome.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ActionLoginError extends Action {

    private String mReason;

    public ActionLoginError(String reason) {
        mReason = reason;
    }

    public String getReason() {
        return  mReason;
    }
}
