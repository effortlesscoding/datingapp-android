package com.minglematcher.minglematcherapp.activities.user.store;

import com.minglematcher.minglematcherapp.activities.user.reducers.UserProfileSetupReducer;
import com.minglematcher.minglematcherapp.dispatcher.ActionsDispatcher;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStoreListener;
import com.minglematcher.minglematcherapp.viewmodels.user.User;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 4/16/2016.
 */
public class UserProfileSetupStore extends IStore {


    @Override
    public void onStateReduced(Action action) {
        for (WeakReference<IStoreListener> weakListener : mListeners) {
            IStoreListener listener = weakListener.get();
            if (listener != null) {
                listener.onNewState(this, action);
            }
        }
    }

    private UserData mUser;
    private static UserProfileSetupStore mInstance;
    public static UserProfileSetupStore getInstance() {
        if (mInstance == null) {
            mInstance = new UserProfileSetupStore();
        }
        return mInstance;
    }

    private UserProfileSetupStore() {
        mUser = User.getCurrentUser();
        ActionsDispatcher.getBus().subscribe(new UserProfileSetupReducer(this));
    }

    public UserData getUser() {
        return mUser;
    }

}
