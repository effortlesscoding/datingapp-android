package com.minglematcher.minglematcherapp.activities.matcher.reducers;

import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionLikeProfileMatched;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionThumbnailAvailable;
import com.minglematcher.minglematcherapp.activities.matcher.store.MatcherActivityStore;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileMatchData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileVideo;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

import java.lang.ref.WeakReference;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class MatcherStoreReducer implements Action1<Object> {

    private WeakReference<MatcherActivityStore> mStore;

    private void saveNewThumbnail(MatcherActivityStore store, ActionThumbnailAvailable atAction) {
        UserData user = store.getUser();
        for (ProfileMatchData pmd : user.getProfilesMatched()) {
            if (pmd.getId() == atAction._id) {

                ProfileSnippetData psd = pmd.getProfile();
                if (psd == null) return;

                List<ProfileVideo> currentVideos = psd.getProfile().getVideos();
                for (int i = 0; i < currentVideos.size(); i++ ) {
                    ProfileVideo pv = currentVideos.get(i);
                    if (pv.getId() == atAction.video.getId()) {
                        pv.getLocalData().thumbnailPath = atAction.video.
                                getLocalData().thumbnailPath;
                        break;
                    }
                }
            }
        }
    }
    @Override
    public void call(Object action) {
        if (!(action instanceof Action)) { return; }
        if (mStore == null) { return; }
        MatcherActivityStore store = mStore.get();
        if (store == null) { return; }
        if (action instanceof ActionLikeProfileMatched) {
            store.getUser().addMatch(((ActionLikeProfileMatched) action).getMatchData());
        } else if (action instanceof ActionThumbnailAvailable) {
            saveNewThumbnail(store, (ActionThumbnailAvailable)action);
        }
        store.onStateReduced((Action) action);
    }

    public MatcherStoreReducer(MatcherActivityStore store) {
        mStore = new WeakReference<MatcherActivityStore>(store);
    }
}