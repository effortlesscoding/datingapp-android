package com.minglematcher.minglematcherapp.activities.user.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;

import java.io.File;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class ActionProfileVideoRecordingDone extends Action {

    public long videoId;
    public File videoFile;
    public File thumbnailFile;


    private ActionProfileVideoRecordingDone(long id) {
        this.videoId = id;
    }

    public static ActionProfileVideoRecordingDone build(long id) {
        return new ActionProfileVideoRecordingDone(id);
    }

    public ActionProfileVideoRecordingDone setVideoFile(File file) {
        this.videoFile = file;
        return this;
    }

    public ActionProfileVideoRecordingDone setThumbnailFile(File file) {
        this.thumbnailFile = file;
        return this;
    }
}
