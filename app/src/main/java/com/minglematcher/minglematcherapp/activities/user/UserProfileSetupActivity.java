package com.minglematcher.minglematcherapp.activities.user;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.minglematcher.minglematcherapp.Constants;
import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.matcher.MatcherActivity;
import com.minglematcher.minglematcherapp.activities.user.ui.UserProfileTabAdapter;
import com.minglematcher.minglematcherapp.ui.WizardPager;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.File;
import java.util.List;

/**
 * Created by AnhAcc on 2/15/2016.
 */
public class UserProfileSetupActivity extends AppCompatActivity {
    private final String TAG = "PROFILE_DETAILS";
    private ViewPager mPager;
    private CirclePageIndicator mPagerIndicator;
    private Button mProceedButton;
    private Button mDoneButton;
    private boolean mBackAfterComplete;
    private ViewPager.OnPageChangeListener mPageListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (position >= 1) {
                mProceedButton.setVisibility(View.VISIBLE);
                mProceedButton.animate().scaleY(1.0f).setDuration(500);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Complete your profile");
        setContentView(R.layout.f_user_profile_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupUI();
    }

    @Override
    protected void onDestroy() {
        mPager.removeOnPageChangeListener(mPageListener);
        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Ignore this --- confusing as hell
    }

    private void setupUI() {
        mPager = (WizardPager) findViewById(R.id.updPager);
        mPagerIndicator = (CirclePageIndicator) findViewById(R.id.updPagination);
        mProceedButton = (Button) findViewById(R.id.updNextButton);
        mProceedButton.setScaleY(0.0f);
        mPager.setAdapter(new UserProfileTabAdapter(getSupportFragmentManager()));
        mPager.addOnPageChangeListener(mPageListener);
        mPagerIndicator.setViewPager(mPager);
        mProceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (canGoNextTab()) {
                    goNextTab();
                } else if (isLastTab()) {
                    // Actually, this is where I need to start an eventbus action that I am
                    // uploading a video.
                    // This will also allow me to do a lot of cool shit.
                    /*if (mBackAfterComplete) {
                        MatcherActivityStore.getInstance().onProfileJustFilled();
                    }*/
                    Intent intent = new Intent(UserProfileSetupActivity.this, MatcherActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private boolean isLastTab() {
        int count = mPager.getAdapter().getCount();
        int curItem = mPager.getCurrentItem();
        return (curItem + 1) == count;
    }

    private boolean canGoNextTab() {
        int count = mPager.getAdapter().getCount();
        int curItem = mPager.getCurrentItem();
        return (curItem + 1) < count;
    }

    private void goNextTab() {
        int count = mPager.getAdapter().getCount();
        int curItem = mPager.getCurrentItem();
        if (curItem + 1 < count) {
            mPager.setCurrentItem(curItem + 1);
        }
    }

    public void onNewUserProfileState(int state) {
        mBackAfterComplete = state == 1;
    }

}

