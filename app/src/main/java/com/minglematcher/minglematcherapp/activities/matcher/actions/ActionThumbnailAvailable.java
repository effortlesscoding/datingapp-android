package com.minglematcher.minglematcherapp.activities.matcher.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileVideo;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ActionThumbnailAvailable extends Action {

    public ProfileVideo video;
    public long _id;

    public ActionThumbnailAvailable(long profileId, ProfileVideo video) {
        this._id = profileId;
        this.video = video;
    }
    
}
