package com.minglematcher.minglematcherapp.activities.user.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.services.responses.FinishUploadVideoResponse;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ActionProfileVideoFinishConfirmed extends Action {
    FinishUploadVideoResponse.Data profileVideo;

    public FinishUploadVideoResponse.Data getData() {
        return profileVideo;
    }

    public ActionProfileVideoFinishConfirmed(FinishUploadVideoResponse.Data profileVideo) {
        this.profileVideo = profileVideo;
    }
}
