package com.minglematcher.minglematcherapp.activities.conversation;

import android.content.Intent;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.viewmodels.MatchInteractions;
import com.minglematcher.minglematcherapp.viewmodels.MatchesHistory;
import com.minglematcher.minglematcherapp.viewmodels.SingleInteraction;
import com.minglematcher.minglematcherapp.utils.BitmapLoader;

import java.io.File;
import java.io.IOException;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class SnapActivity extends AppCompatActivity {
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private SurfaceView cameraSurface;
    private Button cameraButton;
    private TextView cameraTips;
    private Camera camera = null;
    private SurfaceHolder surfaceHolder;
    private LinearLayout cameraCountdownPanel;
    private LinearLayout cameraTipsPanel;
    private TextView cameraCountdown;
    private int secondsLeft;
    private Handler timerHandler;
    private MediaRecorder recorder;
    private MatchInteractions interaction;
    private Handler handler;
    private boolean isRecording;
    private File fileName;
    private static final String TAG = "SnapActivity";
    private ImageView cameraTalkingToPic;
    private boolean recorderInited = false;

    private class SurfaceCallback implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            releaseCamRecorder();
            try {
                camera = openFrontFacingCamera();
            } catch (RuntimeException e) {
                Log.e("CAMERA", "error", e);
                return;
            }
            camera.setDisplayOrientation(90);
            Camera.Parameters param = camera.getParameters();
            param.setPreviewSize(352, 288);
            camera.setParameters(param);
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (Exception e) {
                Log.e(TAG, "Camera Error : ", e);
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            refreshCamera();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            releaseCamRecorder();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_f_camera);
        if (inject()) {
            bind();
        } else {
            Log.d(TAG, "Finished early! Not enough parameters to initialize the view.");
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseCamRecorder();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("CAMERA", "onResume() start");
    }


    private boolean inject() {
        fileName = getNextFileName();
        cameraSurface = (SurfaceView) findViewById(R.id.cameraSurface);
        cameraTips = (TextView) findViewById(R.id.cameraTips);
        cameraButton = (Button) findViewById(R.id.cameraButton);
        cameraCountdownPanel = (LinearLayout) findViewById(R.id.cameraCountdownPanel);
        cameraTipsPanel = (LinearLayout) findViewById(R.id.cameraTipsPanel);
        cameraCountdown = (TextView) findViewById(R.id.cameraCountdown);
        cameraTalkingToPic = (ImageView) findViewById(R.id.cameraTalkingToPic);
        timerHandler = new Handler();
        recorder = new MediaRecorder();
        recorderInited = false;
        secondsLeft = 15;
        isRecording = false;
        Intent i = getIntent();
        if (i != null) {
            Bundle b = getIntent().getExtras();
            if (b != null) {
                Object extra = b.get(InteractionActivity.KeyTalkingTo);
                if (extra != null) {
                    interaction = MatchesHistory.getInstance().findInteractionById((int) extra);
                    return true;
                }
            }
        }
        return false;
    }

    private void bind() {
        surfaceHolder = cameraSurface.getHolder();
        surfaceHolder.addCallback(new SurfaceCallback());
        cameraTalkingToPic.setImageBitmap(BitmapLoader
                .getImage(interaction.getMatch().getProfilePhoto(), 100, 100));
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cameraTipsPanel.getVisibility() == View.VISIBLE) {
                    startRecording();
                } else {
                    stopRecording();
                }
            }
        });
    }

    private boolean startRecording() {
        if (!recorderInited) {
            initRecorder();
            prepareRecorder();
            recorderInited = true;
        }
        recorder.setPreviewDisplay(surfaceHolder.getSurface());
        cameraTipsPanel.setVisibility(View.GONE);
        cameraCountdownPanel.setVisibility(View.VISIBLE);
        if (isRecording) {
            recorder.stop();
            isRecording = false;

            // Let's initRecorder so we can record again
            initRecorder();
            prepareRecorder();
        } else {
            isRecording = true;
            recorder.start();
        }

        timerHandler.postDelayed(timerRunnable, 0);
        cameraButton.setText("Stop");
        return true;
    }

    private void initRecorder() {
        camera.stopPreview();
        camera.unlock();
        recorder.setCamera(camera);
        recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        CamcorderProfile cpHigh = CamcorderProfile
                .get(CamcorderProfile.QUALITY_LOW);
        cpHigh.fileFormat = MediaRecorder.OutputFormat.MPEG_4;
        cpHigh.videoCodec = MediaRecorder.VideoEncoder.MPEG_4_SP;
        cpHigh.audioCodec = MediaRecorder.AudioEncoder.AAC;
        recorder.setProfile(cpHigh);
        recorder.setOutputFile(fileName.getPath());
        recorder.setOrientationHint(270);
        // Can this conflict with camera ?!
        recorder.setMaxDuration(15000); // 50 seconds
        recorder.setMaxFileSize(5000000); // Approximately 5 megabytes
    }

    private Camera openFrontFacingCamera() throws RuntimeException {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cam = Camera.open(camIdx);
                return cam;
            }
        }
        return cam;
    }


    protected boolean prepareForVideoRecording() {
        // as above
        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            Log.e(TAG, "IllegalStateException when preparing MediaRecorder "
                    + e.getMessage());
            e.getStackTrace();
            releaseCamRecorder();
            return false;
        } catch (IOException e) {
            Log.e(TAG, "IOException when preparing MediaRecorder "
                    + e.getMessage());
            e.getStackTrace();
            releaseCamRecorder();
            return false;
        }
        return true;
    }

    // http://developer.android.com/guide/topics/media/camera.html
    private void stopRecording() {
        timerHandler.removeCallbacks(timerRunnable);
        releaseCamRecorder();
        // Add it to Match Interactions...
        Intent goToConversation = new Intent(SnapActivity.this, InteractionActivity.class);
        interaction.add(new SingleInteraction(true, false, fileName.getPath()));
        goToConversation.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        goToConversation.putExtra(InteractionActivity.KeyTalkingTo,
                interaction.getMatch().getFacebookId());
        startActivity(goToConversation);
        this.finish();
    }


    private void prepareRecorder() {
        try {
            recorder.prepare();
            Log.d(TAG, "PREPARED RECORDER!");
        } catch (IllegalStateException e) {
            e.printStackTrace();
            finish();
        } catch (IOException e) {
            e.printStackTrace();
            finish();
        }
    }


    private void refreshCamera() {
        if (surfaceHolder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
        } catch (Exception e) {
            Log.e("CAMERA", "Preview stop failed!", e);
        }
    }


    private void releaseCamRecorder() {
        if (recorder != null && isRecording) {
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;
        }
        isRecording = false;
        if (camera != null) {
            camera.lock();
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    private Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            cameraCountdown.setText(String.valueOf(secondsLeft));
            secondsLeft--;
            if (secondsLeft > 0) {
                timerHandler.postDelayed(this, 1000);
            } else {
                stopRecording();
            }
        }
    };

    private File getNextFileName() {
        File file = new File(getApplicationInfo().dataDir +
                File.separator + "reply_" + System.currentTimeMillis() + ".mp4" );
        Log.d(TAG, "FileName : " + file.toString() + " --- " + file.getPath());
        return file;
    }

}
