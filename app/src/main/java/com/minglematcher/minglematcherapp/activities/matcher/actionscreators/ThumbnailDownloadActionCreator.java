package com.minglematcher.minglematcherapp.activities.matcher.actionscreators;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionThumbnailAvailable;
import com.minglematcher.minglematcherapp.dispatcher.ActionsDispatcher;
import com.minglematcher.minglematcherapp.services.ServerApiService;
import com.minglematcher.minglematcherapp.services.responses.GetSignedUrlResponse;
import com.minglematcher.minglematcherapp.utils.BitmapLoader;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileVideo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class ThumbnailDownloadActionCreator {
    private final String TAG = "THUMBNAIL_DOWNLOAD";
    private ProfileVideo mProfileVideo;
    private long _id;

    public ThumbnailDownloadActionCreator(long id) {
        _id = id;
    }

    private class BitmapTarget implements Target {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            // Time to save it to somewhere... like a file?
            File f = new File(mProfileVideo.getLocalData().thumbnailPath);
            BitmapLoader.saveImageToFile(bitmap, f);
            ActionsDispatcher.getBus().post(
                    new ActionThumbnailAvailable(_id, mProfileVideo));
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            Log.e(TAG, "Error downloading an image");
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    public void downloadThumbnail(ProfileVideo video) {
        mProfileVideo = video;
        // Get download links for all different sorts of paths :)
        // Then, we will download them
        if (!video.getLocalThumbnailPath().endsWith(".jpg")) {
            Log.e(TAG, "Wrong extension for thumbnail : " + video.thumbnail);
            return;
        }
        String resourcePath = video.storagePath + video.thumbnail;
        ServerApiService.getInstance().getSignedUrlForResource(resourcePath, new Callback<GetSignedUrlResponse>() {
            @Override
            public void onResponse(Call<GetSignedUrlResponse> call, Response<GetSignedUrlResponse> response) {
                // Download link received - Time to download it!
                if (response.code() == 200) {
                    GetSignedUrlResponse body = response.body();
                    String downloadUrl = body.getData();
                    Picasso.with(MingleMatcherApplication.getContext())
                            .load(downloadUrl)
                            .into(new BitmapTarget());
                } else {
                    Log.e(TAG, "Could not get signed download url : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<GetSignedUrlResponse> call, Throwable t) {
                Log.e(TAG, "Could not get signed download url : " + t.getMessage());
            }
        });
    }
}
