package com.minglematcher.minglematcherapp.activities.matcher.tabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.conversation.InteractionActivity;
import com.minglematcher.minglematcherapp.activities.conversation.SnapActivity;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionLikeProfileMatched;
import com.minglematcher.minglematcherapp.activities.matcher.adapters.ConversationsAdapter;
import com.minglematcher.minglematcherapp.activities.matcher.store.MatcherActivityStore;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStoreListener;
import com.minglematcher.minglematcherapp.viewmodels.MatchInteractions;
import com.minglematcher.minglematcherapp.viewmodels.MatchesHistory;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileMatchData;
import com.minglematcher.minglematcherapp.viewmodels.user.User;

/**
 * Created by AnhAcc on 2/1/2016.
 */

public class ConversationsTabFragment extends Fragment implements IStoreListener {

    @Override
    public void onNewState(IStore store, Action action) {
        if (action instanceof ActionLikeProfileMatched) {
            ActionLikeProfileMatched doneAction = (ActionLikeProfileMatched)action;
            synchronized (mAdapter) {
                ProfileMatchData md = doneAction.getMatchData();
                if (md != null) {
                    mAdapter.addItem(md);
                    mAdapter.notifyDataSetChanged();
                }
            }
        } else {
            Log.e("ConversationsTab:", "Unhandled action : " + action.getClass());
        }
    }

    private class OnAdapterItemClicked implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            MatchInteractions.State s = MatchesHistory.getInstance().getMatches().get(position).getState();
            if (s == MatchInteractions.State.Matched) {
                if (User.getCurrentUser().isFemale()) {
                    showInteraction(position);
                } else {
                    // A man has to make the first move...
                    showSayHello(position);
                }
            } else if (s != MatchInteractions.State.Liked) {
                showInteraction(position);
            }
        }
    }

    private ConversationsAdapter mAdapter;
    private ListView mConversationsList;

    public ConversationsTabFragment() {

    }

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.matcher_fragment_conversations, container, false);
        MatcherActivityStore.getInstance().addListener(this);
        mConversationsList = (ListView)v.findViewById(R.id.mafcConversationsList);
        mConversationsList.setOnItemClickListener(new OnAdapterItemClicked());
        mAdapter = new ConversationsAdapter(getContext());
        mConversationsList.setAdapter(mAdapter);
        return v;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            // Set a new adapter ?!
            // Notify update to the adapter ?!
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("TabFragment", "Destroy conversations tab view");
        // Free images
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void showInteraction(final int position) {
        Intent openConversation = new Intent(getContext(), InteractionActivity.class);
        openConversation.putExtra(InteractionActivity.KeyTalkingTo,
                MatchesHistory.getInstance().getMatches().get(position).getMatch().getFacebookId());
        startActivity(openConversation);
        // Maybe just have a store for CurrentInteractionActivity ?!
    }

    private void showSayHello(final int position) {
        Intent intent = new Intent(getContext(), SnapActivity.class);
        intent.putExtra(InteractionActivity.KeyTalkingTo,
                MatchesHistory.getInstance().getMatches().get(position).getMatch().getFacebookId());
        startActivity(intent);
    }

}
