package com.minglematcher.minglematcherapp.activities.matcher;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.matcher.adapters.MatcherAdapter;
import com.minglematcher.minglematcherapp.activities.user.UserProfileSetupActivity;
import com.minglematcher.minglematcherapp.ui.WizardPager;

/**
 * Created by AnhAcc on 2/18/2016.
 */
public class MatcherActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String[] mMenuItems;
    private ActionBarDrawerToggle mDrawerToggle;
    private WizardPager mTabsPager;

    private static class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            //Log.i("Drawer f_matcher_profile clicked", position + "--" + id);
            if (position == 1) {
                Context c = parent.getContext();
                Intent intent = new Intent(c, UserProfileSetupActivity.class);
                c.startActivity(intent);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        setTitle("");
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        bind();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_matches:
                mTabsPager.setCurrentItem(0);
                break;
            case R.id.action_history:
                mTabsPager.setCurrentItem(1);
                break;

            default: break;
        }
        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        return super.onPrepareOptionsMenu(menu);
    }
    private void inject() {
        setContentView(R.layout.matcher_activity);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.maDrawer);
        mDrawerList = (ListView) findViewById(R.id.maLeftMenu);
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayShowTitleEnabled(false);
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setHomeButtonEnabled(true);
            bar.setDisplayUseLogoEnabled(false);
            bar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mTabsPager = (WizardPager) findViewById(R.id.maTabs);
    }

    // Custom Action Bar, though...
    private void bind() {
        mMenuItems = new String[]{"Search Criteria", "My Profile", "Account", "Sign out"};
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.menu_drawer_list_item, mMenuItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mTabsPager.setAdapter(new MatcherAdapter(this));
    }

    public void goConversations() {
        mTabsPager.setCurrentItem(1);
    }



}
