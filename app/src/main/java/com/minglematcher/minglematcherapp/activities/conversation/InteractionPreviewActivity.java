package com.minglematcher.minglematcherapp.activities.conversation;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.viewmodels.SingleInteraction;

import java.io.IOException;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class InteractionPreviewActivity extends AppCompatActivity {
    public static final String KeyVideo = "VID";
    private final String TAG = "VIDEO PREVIEW";
    private TextView countdown;
    private SurfaceView surface;
    private SingleInteraction singleInteraction;
    private int secondsLeft;
    private Handler timerHandler;

    private Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            countdown.setText(String.valueOf(secondsLeft));
            secondsLeft--;
            if (secondsLeft > 0) {
                timerHandler.postDelayed(this, 1000);
            } else {
                stopPreview();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Conversation");
        setContentView(R.layout.a_f_interaction_preview);
        if (inject()) {
            bind();
        }
    }

    private boolean inject() {
        surface = (SurfaceView) findViewById(R.id.interactionPreviewSurface);
        countdown = (TextView) findViewById(R.id.interactionPreviewCountdown);
        Intent intent = getIntent();
        timerHandler = new Handler();
        if (intent != null) {
            Bundle extra = intent.getExtras();
            if (extra != null) {
                if (extra.get(KeyVideo) != null) {
                    singleInteraction = (SingleInteraction)extra.get(KeyVideo);
                    return true;
                }
            }
        }
        return false;
    }

    private void bind() {
        final MediaPlayer mp = new MediaPlayer();
        SurfaceHolder holder = surface.getHolder();
        if (holder != null) {
            holder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    if (singleInteraction.isInAssetsFolder()) {
                        playFromAssets(mp);
                    } else {
                        playFromLocal(mp);
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    if (mp != null) {
                        mp.stop();
                        mp.reset();
                        mp.release();
                    }
                }
            });
        } else {
            Log.e("PREVIEW", "SurfaceHolder is null.");
        };
    }

    private void playFromLocal(MediaPlayer mp) {
        try {
            Log.d(TAG, "File : " + singleInteraction.getMediaSource());
            mp.setDataSource(singleInteraction.getMediaSource());
            mp.setDisplay(surface.getHolder());
            mp.setScreenOnWhilePlaying(true);
            mp.prepare();
            secondsLeft = (int) (mp.getDuration() / 1000);
            startCountdown();
            mp.start();
            Log.d(TAG, "Activity started");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void playFromAssets(MediaPlayer mp) {
        try {
            Log.d(TAG, "Recording from" + singleInteraction.getMediaSource());
            AssetFileDescriptor afd = this.getAssets().openFd(singleInteraction.getMediaSource());
            mp.setDataSource(
                    afd.getFileDescriptor(),
                    afd.getStartOffset(),
                    afd.getLength()
            );
            afd.close();
            mp.setDisplay(surface.getHolder());
            mp.setScreenOnWhilePlaying(true);
            mp.prepare();
            secondsLeft = (int) (mp.getDuration() / 1000);
            startCountdown();
            mp.start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startCountdown() {
        timerHandler.postDelayed(timerRunnable, 0);
    }

    private void stopPreview() {
        timerHandler.removeCallbacks(timerRunnable);
        finish();
    }

}
