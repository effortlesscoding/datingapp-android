package com.minglematcher.minglematcherapp.activities.user.ui;

import com.minglematcher.minglematcherapp.viewmodels.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class UserProfileViewModel {
    public class Choice {

        private String mValue;
        private String mMeaning;

        public Choice(String value, String meaning) {
            this.mValue = value;
            this.mMeaning = meaning;
        }

        public String getValue() {
            return mValue;
        }

        public String getMeaning() {
            return mMeaning;
        }
    }
    public enum ProfileType {
        // PHOTOS, VIDEO, HEIGHT, LIKES, IDEAL_DATE, ABOUT_ME, SECRET_ME, QUALIFICATION
        VIDEO, HEIGHT, RELIGION, WHO_I_AM, IDEAL_DATE
    };

    private ProfileType mType;

    private String mTitle;
    private List<Choice> mChoices;
    private boolean mIsComplete;

    public String getTitle() {
        return mTitle;
    }

    public List getmChoices() {
        return mChoices;
    }

    public ProfileType getmType() {
        return mType;
    }

    public UserProfileViewModel(ProfileType type, User user, String title) {
        mIsComplete = false;
        this.mType = type;
        mChoices = new ArrayList();
        mTitle = title;
        switch (this.mType) {
            case VIDEO:
            case HEIGHT:
                break;
            case RELIGION:
                mChoices.add(new Choice("Christianity", ""));
                mChoices.add(new Choice("Islam", ""));
                mChoices.add(new Choice("Hinduism", ""));
                mChoices.add(new Choice("Judaism", ""));
                mChoices.add(new Choice("Other", ""));
                mChoices.add(new Choice("Atheist", ""));
                break;
            case WHO_I_AM:
                mChoices.add(new Choice("Athletic", "Active"));
                mChoices.add(new Choice("Love sports", "Active"));
                mChoices.add(new Choice("Love spending time with friends", "Extravert"));
                mChoices.add(new Choice("Always open for a fun conversation", "Extravert"));
                mChoices.add(new Choice("Avid book reader", "Introvert"));
                mChoices.add(new Choice("Contemplate about life a lot", "Introvert"));
                mChoices.add(new Choice("Very professional", "Mature"));
                mChoices.add(new Choice("Am a very focused person", "Mature"));
                mChoices.add(new Choice("A club diva", "Emotional"));
                mChoices.add(new Choice("A wildcard", "Emotional"));
                mChoices.add(new Choice("A daredevil", "Risk-taker"));
                mChoices.add(new Choice("An adventurer", "Risk-taker"));
                break;
            case IDEAL_DATE:
                mChoices.add(new Choice("Honest and upfront", "Risk-taker"));
                mChoices.add(new Choice("Thoughtful", "Resourceful"));
                mChoices.add(new Choice("Likes sports and being active", "Proactive"));
                mChoices.add(new Choice("Does not take things too seriously", "Fun and social"));
                mChoices.add(new Choice("Likes to do something different each time", "Risk-taker"));
                mChoices.add(new Choice("Is kind and thoughtful", "Empathetic"));
                mChoices.add(new Choice("is not lazy", "Proactive"));
                mChoices.add(new Choice("An adventurer", "Risk-taker"));
                mChoices.add(new Choice("Compassionate", "Empathetic"));
                mChoices.add(new Choice("Laughs at my (terrible) jokes", "Fun and social"));
                mChoices.add(new Choice("Smart", "Resourceful"));
                mChoices.add(new Choice("Makes me laugh", "Fun and social"));
                mChoices.add(new Choice("Respectful to me", "Empathetic"));
                mChoices.add(new Choice("Is employed", "Resourceful"));
                mChoices.add(new Choice("Makes arrangements", "Proactive"));
                break;
        }
    }


    public void setComplete(boolean b) {
        mIsComplete = b;
    }

    public boolean isComplete() {
        return mIsComplete;
    }

}
