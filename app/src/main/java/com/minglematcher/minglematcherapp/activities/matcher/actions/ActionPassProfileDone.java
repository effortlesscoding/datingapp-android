package com.minglematcher.minglematcherapp.activities.matcher.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class ActionPassProfileDone extends Action {

    private ProfileSnippetData mProfile;

    public ActionPassProfileDone(ProfileSnippetData profile) {
        mProfile = profile;
    }

    public ProfileSnippetData getProfile() {
        return mProfile;
    }
}
