package com.minglematcher.minglematcherapp.activities.matcher.actions;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class ActionLikeProfileError extends Action {
    private ProfileSnippetData profile;

    public ActionLikeProfileError(ProfileSnippetData profile) {
        this.profile = profile;
    }

    public ProfileSnippetData getProfile() {
        return profile;
    }
}
