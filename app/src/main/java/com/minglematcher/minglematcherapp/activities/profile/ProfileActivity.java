package com.minglematcher.minglematcherapp.activities.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.viewmodels.ProfileData;
import com.minglematcher.minglematcherapp.ui.ActionBarActivity;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class ProfileActivity extends ActionBarActivity {
    private GalleryPagerAdapter adapter;
    private ViewPager profileGallery;
    private Button likeButton;
    private Button passButton;
    private Button cameraMode;
    private Button photosMode;
    private ProfileData profileData;
    private VideoView videoView;
    private TextView nameText;
    private TextView ageText;
    private TextView occupationText;
    private TextView locationText;
    private TextView raceText;
    private TextView religionText;
    private TextView likesText;
    private TextView datePreferencesText;
    private TextView taglineText;

    private void injectControls() {
        setContentView(R.layout.a_f_profile);
        likeButton = (Button) findViewById(R.id.likeButton);
        passButton = (Button) findViewById(R.id.passButton);
        cameraMode = (Button) findViewById(R.id.videoProfileButton);
        photosMode = (Button) findViewById(R.id.photoProfileButton);
        profileGallery = (ViewPager) findViewById(R.id.profileGallery);
        videoView = (VideoView) findViewById(R.id.videoProfile);
        //profileData = (ProfilePersonalData)getIntent().getExtras().getSerializable(ProfilePersonalData.KEY);

        locationText = (TextView) findViewById(R.id.locationText);
        raceText = (TextView) findViewById(R.id.raceText);
        religionText = (TextView) findViewById(R.id.religionText);
        nameText = (TextView) findViewById(R.id.nameText);
        ageText = (TextView)findViewById(R.id.ageText);
        occupationText = (TextView) findViewById(R.id.occupationText);
        likesText = (TextView) findViewById(R.id.likesText);
        datePreferencesText = (TextView) findViewById(R.id.datePreferencesText);
        taglineText = (TextView) findViewById(R.id.taglineText);
    }

    private void initialize() {
        /*adapter = new GalleryPagerAdapter(this, profileData.getPhotos());
        profileGallery.setAdapter(adapter);
        final CirclePageIndicator pageIndicator = (CirclePageIndicator)findViewById(R.id.profilePhotosPaginator);
        pageIndicator.setViewPager(profileGallery);
        LikeEventListener likeEventListener = new LikeEventListener(this, null);

        videoView.setVideoURI(profileData.getVideoUri());
        likeButton.setOnClickListener(likeEventListener);


        locationText.setText(profileData.getLocation());
        raceText.setText(profileData.getRace().toString());
        religionText.setText(profileData.getReligion().toString());
        nameText.setText(profileData.getName());
        int age = profileData.getAge();
        ageText.setText(age + "");
        occupationText.setText(profileData.getOccupation());
        likesText.setText(android.text.TextUtils.join(", ", profileData.getLikes()));
        datePreferencesText.setText(android.text.TextUtils.join(", ", profileData.getDatePreferences()));
        taglineText.setText(profileData.getTagLine());

        cameraMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileGallery.setVisibility(View.GONE);
                pageIndicator.setVisibility(View.GONE);
                videoView.setVisibility(View.VISIBLE);
                videoView.start();
            }
        });

        photosMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                videoView.setVisibility(View.GONE);
                profileGallery.setVisibility(View.VISIBLE);
                pageIndicator.setVisibility(View.VISIBLE);
            }
        });*/
    }

    private void bind() {
        injectControls();
        initialize();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        videoView.stopPlayback();
        videoView.setVideoURI(null);
        videoView = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from viewpager_main.xml
        bind();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MingleMatcherApplication.REQUEST_KEY_ACTIVITY) {
            if(resultCode == MingleMatcherApplication.RESULT_PROFILE_FINISHED){
                Intent returnIntent = new Intent();
                setResult(MingleMatcherApplication.RESULT_MATCH, returnIntent);
                finish();
            }
        }
    }

    public void showMatch(ProfileData profileData) {
        // Create / not create a match depending on the data in the object NavDrawerItem
        Intent returnIntent = new Intent();
        setResult(MingleMatcherApplication.RESULT_MATCH, returnIntent);
        finish();
    }

    public void showNeedProfile() {
        // Intent intent = new Intent(ProfileActivity.this, UserProfileActivity.class);
        // startActivityForResult(intent, 1);
    }

    public void passed() {
        Intent returnIntent = new Intent();
        setResult(MingleMatcherApplication.RESULT_PASSED, returnIntent);
        finish();
    }

}
