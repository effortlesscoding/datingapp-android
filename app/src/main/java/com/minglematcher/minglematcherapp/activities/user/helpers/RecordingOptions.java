package com.minglematcher.minglematcherapp.activities.user.helpers;

import java.io.File;

/**
 * Created by AnhAcc on 3/26/2016.
 */
public class RecordingOptions {
    private File mFile;
    private int mMaxRecordSeconds;

    public RecordingOptions(File file, int maxSeconds) {
        this.mMaxRecordSeconds = maxSeconds;
        this.mFile = file;
    }

    public File getFile() {
        return mFile;
    }

    public int getMaxSeconds() {
        return mMaxRecordSeconds;
    }
}
