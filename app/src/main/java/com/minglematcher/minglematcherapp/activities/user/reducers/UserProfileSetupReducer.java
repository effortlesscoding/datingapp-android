package com.minglematcher.minglematcherapp.activities.user.reducers;

import com.minglematcher.minglematcherapp.activities.user.actions.ActionProfileVideoFinishConfirmed;
import com.minglematcher.minglematcherapp.activities.user.actions.ActionVideoUploadingStarted;
import com.minglematcher.minglematcherapp.activities.user.actionscreator.UserProfileActionsCreator;
import com.minglematcher.minglematcherapp.activities.user.store.UserProfileSetupStore;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadDone;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadError;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUploadProgress;
import com.minglematcher.minglematcherapp.dispatcher.actions.EUploadType;
import com.minglematcher.minglematcherapp.dispatcher.stores.AStoreReducer;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileVideo;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

import java.util.List;

/**
 * Created by AnhAcc on 5/5/2016.
 */
public class UserProfileSetupReducer extends AStoreReducer<UserProfileSetupStore> {

    private void setUploadDone(ActionUploadDone action) {
        EUploadType type = action.getUploadType();
        long videoId = action.getVideoId();
        if (type == EUploadType.PROFILE_THUMBNAIL) {
            // Find the right profile video with the right video ID
            // Change the model in it
            UserData user = mStore.get().getUser();
            List<ProfileVideo> videos = user.getProfile().getVideos();
            for (ProfileVideo pv : videos) {
                if (pv.getId() == videoId) {
                    pv.getLocalData().thumbnailUploadProgress = 1.0;
                }
            }
        } else if (type == EUploadType.PROFILE_VIDEO) {
            UserData user = mStore.get().getUser();
            List<ProfileVideo> videos = user.getProfile().getVideos();
            for (ProfileVideo pv : videos) {
                if (pv.getId() == videoId) {
                    pv.getLocalData().uploadProgress = 1.0;
                }
            }
        }
    }

    private void setUploadError(ActionUploadError error) {

    }


    private void setUploadProgress(ActionUploadProgress action) {
        EUploadType type = action.getUploadType();
        long videoId = action.getVideoId();
        if (type == EUploadType.PROFILE_THUMBNAIL) {
            // Find the right profile video with the right video ID
            // Change the model in it
            UserData user = mStore.get().getUser();
            List<ProfileVideo> videos = user.getProfile().getVideos();
            for (ProfileVideo pv : videos) {
                if (pv.getId() == videoId) {
                    pv.getLocalData().thumbnailUploadProgress = action.progress;
                }
            }
        } else if (type == EUploadType.PROFILE_VIDEO) {
            UserData user = mStore.get().getUser();
            List<ProfileVideo> videos = user.getProfile().getVideos();
            for (ProfileVideo pv : videos) {
                if (pv.getId() == videoId) {
                    pv.getLocalData().uploadProgress = action.progress;
                }
            }
        }
    }

    private void addNewVideo(ActionVideoUploadingStarted action) {
        /*User user = mStore.get().getUser();
        user.getProfile().addProfileVideo(action.getVideoId(), action.videoFile,
                action.thumbnailFile);*/
    }

    private void videoUploadConfirmed(ActionProfileVideoFinishConfirmed action) {
        mStore.get().getUser().getProfile().confirmProfileVideo(action);
    }

    public UserProfileSetupReducer(UserProfileSetupStore store) {
        super(store);
    }

    @Override
    protected void onAction(UserProfileSetupStore store, Action action) {
        /**
         * Depending on the action you should update the store's data appropriately
         */
        if (action instanceof ActionUploadDone) {
            setUploadDone((ActionUploadDone) action);
            EUploadType type = ((ActionUploadDone) action).getUploadType();
            if (type == EUploadType.PROFILE_VIDEO) {
                new UserProfileActionsCreator(((ActionUploadDone) action).getVideoId()).finishVideoUpload();
            }
        } else if (action instanceof ActionUploadError) {
            setUploadError((ActionUploadError) action);
        } else if (action instanceof ActionUploadProgress) {
            setUploadProgress((ActionUploadProgress) action);
        } else if (action instanceof ActionVideoUploadingStarted) {
            // Do nothing
        } else if (action instanceof ActionProfileVideoFinishConfirmed) {
            videoUploadConfirmed((ActionProfileVideoFinishConfirmed)action);
        }
        store.onStateReduced(action);
    }

}
