package com.minglematcher.minglematcherapp.activities.profile;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.minglematcher.minglematcherapp.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class GalleryPagerAdapter extends PagerAdapter {
    private Context context;
    private List<String> images;
    private LayoutInflater inflater;

    public GalleryPagerAdapter(Context context, List<String> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.f_profile_photo, container,
                false);
        ImageView image = (ImageView)itemView.findViewById(R.id.galleryProfilePhoto);
        try {
            InputStream is = context.getAssets().open(images.get(position));
            Drawable d = Drawable.createFromStream(is, null);
            image.setImageDrawable(d);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ((ViewPager) container).addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}

