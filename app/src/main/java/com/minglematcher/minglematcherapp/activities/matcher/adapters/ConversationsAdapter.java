package com.minglematcher.minglematcherapp.activities.matcher.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.MingleMatcherApplication;
import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionLikeProfileMatched;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionStoreRegistered;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionThumbnailAvailable;
import com.minglematcher.minglematcherapp.activities.matcher.actionscreators.MatcherActionsCreator;
import com.minglematcher.minglematcherapp.activities.matcher.actionscreators.ThumbnailDownloadActionCreator;
import com.minglematcher.minglematcherapp.activities.matcher.store.MatcherActivityStore;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStoreListener;
import com.minglematcher.minglematcherapp.ui.RoundedTransformation;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileMatchData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfilePersonalData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileVideo;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by AnhAcc on 2/1/2016.
 */
public class ConversationsAdapter extends BaseAdapter implements IStoreListener {

    private void addNewMatches(MatcherActivityStore store) {
        UserData user = store.getUser();
        if (user != null && mConversingWith != null) {
            List<ProfileMatchData> matches = user.getProfilesMatched();
            mConversingWith.clear();
            mConversingWith.addAll(matches);
            this.notifyDataSetChanged();
        }
    }

    @Override
    public void onNewState(IStore store, Action action) {
        if (!(store instanceof MatcherActivityStore)) { return; }
        if (action instanceof ActionStoreRegistered) {
            addNewMatches((MatcherActivityStore) store);
        } else if (action instanceof ActionThumbnailAvailable) {
            addNewMatches((MatcherActivityStore) store);
        } else if (action instanceof ActionLikeProfileMatched) {
            addNewMatches((MatcherActivityStore) store);
        }
    }

    public static class ViewHolder {
        TextView firstLine;
        TextView secondLine;
        ImageView photo;

        private ViewHolder() {

        }

        public static ViewHolder build() {
            return new ViewHolder();
        }

        public ViewHolder setFirstLine(TextView textView) {
            firstLine = textView;
            return this;
        }

        public ViewHolder setSecondLine(TextView textView) {
            secondLine = textView;
            return this;
        }

        public ViewHolder setPhoto(ImageView photo) {
            this.photo = photo;
            return this;
        }

        public void setDatasource(ProfileMatchData pmd) {
            //pmd.setConversation();
            if (pmd == null) return;
            ProfileSnippetData psd = pmd.getProfile();
            if (psd == null) return;
            ProfilePersonalData ppd = psd.getProfile();
            if (ppd != null && ppd.getFirstName() != null) {
                firstLine.setText("Tap to say hi to " + ppd.getFirstName());
            } else {
                firstLine.setText("Tap to say hi.");
            }
            File thumbnail = pmd.getFirstProfileLocal();
            if (thumbnail == null) {
                if (ppd == null) return;
                List<ProfileVideo> videos = ppd.getVideos();
                if (videos != null && videos.size() > 0) {
                    new ThumbnailDownloadActionCreator(pmd.getId())
                            .downloadThumbnail(videos.get(0));
                } else {
                    return;
                }
            } else {
                Picasso.with(MingleMatcherApplication.getContext())
                        .load(thumbnail.getAbsoluteFile())
                        .transform(new RoundedTransformation())
                        .into(photo);
            }
        }
    }

    private List<ProfileMatchData> mConversingWith;
    private WeakReference<Context> mContext;

    public ConversationsAdapter(Context context) {
        this.mConversingWith = new ArrayList<>();
        this.mContext = new WeakReference<Context>(context);
        MatcherActivityStore.getInstance().addListener(this);
    }

    public void addItem(ProfileMatchData psd) {
        mConversingWith.add(psd);
    }
    @Override
    public int getCount() {
        return mConversingWith.size();
    }

    @Override
    public Object getItem(int position) {
        return mConversingWith.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Context context = mContext.get();
        if (context == null) { return convertView; }

        View rowView = convertView;
        ProfileMatchData pmd = mConversingWith.get(position);
        ViewHolder holder;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
            rowView = inflater.inflate(R.layout.f_history_item, parent, false);
            holder = ViewHolder.build()
                    .setFirstLine((TextView) rowView.findViewById(R.id.firstLine))
                    .setSecondLine((TextView) rowView.findViewById(R.id.secondLine))
                    .setPhoto((ImageView) rowView.findViewById(R.id.icon));
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder)rowView.getTag();
        }
        if (pmd != null) {
            holder.setDatasource(pmd);
        }
        return rowView;
    }

}
