package com.minglematcher.minglematcherapp.activities.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.VideoView;

import com.minglematcher.minglematcherapp.Constants;
import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.viewmodels.ProfileData;
import com.minglematcher.minglematcherapp.viewmodels.ProfileVideoData;

/**
 * Created by AnhAcc on 3/25/2016.
 */
public class VideoProfileActivity extends AppCompatActivity {

    private static class ProfileDetailsAdapter extends BaseAdapter {

        private abstract class ViewHolder {
            abstract void populateViewHolder(View v);
        }

        private class DetailsLineItemViewHolder extends ViewHolder {
            ImageView expandIcon;
            TextView shortDescription;
            TextView longDescription;

            @Override
            void populateViewHolder(View v) {
                expandIcon = (ImageView)v.findViewById(R.id.fvpExpansionIcon);
                shortDescription = (TextView)v.findViewById(R.id.fvpShortDescription);
                longDescription = (TextView)v.findViewById(R.id.fvpLongDescription);
            }
        }

        private class ThumbnailLineItemViewHolder extends ViewHolder {
            ImageView thumbnail;
            TextView title;
            TextView lastUpdated;

            @Override
            void populateViewHolder(View v) {
                thumbnail = (ImageView) v.findViewById(R.id.fvpVidThumbnail);
                title = (TextView)v.findViewById(R.id.fvpVidThumbnailTitle);
                lastUpdated = (TextView)v.findViewById(R.id.fvpVidThumbnailSecondaryText);
            }
        }

        private ProfileData mProfileData;
        private LayoutInflater mInflater;

        private ProfileDetailsAdapter(Context c, ProfileData profileData) {
            mProfileData = profileData;
            mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            int size = mProfileData.getProfileVideos().size();
            return size + 1;
        }

        @Override
        public Object getItem(int position) {
            if (mProfileData == null || mProfileData.getProfileVideos() == null
                    || mProfileData.getProfileVideos().size() <= position) {
                return null;
            }
            return mProfileData.getProfileVideos().get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private void populateLineItem(int position, DetailsLineItemViewHolder vh) {
            if (position == 0) {
                ProfileVideoData pd = mProfileData.getProfileVideos().get(position);
                vh.shortDescription.setText(pd.getShortDescription());
                vh.longDescription.setText(pd.getDescription());
            }
        }

        private void populateLineItem(int position, ThumbnailLineItemViewHolder vh) {
            if (position >= 1) {
                ProfileVideoData pd = mProfileData.getProfileVideos().get(position - 1);
                vh.title.setText(pd.getShortDescription());
                // Leave the thumbnail alone, for now.
                vh.lastUpdated.setText("Uploaded 1 day ago");
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh = null;
            if (convertView == null) {
                if (position == 0) {
                    convertView = mInflater.inflate(R.layout.f_vid_profile_description, parent, false);
                    vh = new DetailsLineItemViewHolder();
                    vh.populateViewHolder(convertView);
                    convertView.setTag(R.id.TAG_ONLINE_ID, vh);
                } else {
                    convertView = mInflater.inflate(R.layout.f_vid_profile_thumbnail, parent, false);
                    vh = new ThumbnailLineItemViewHolder();
                    vh.populateViewHolder(convertView);
                    convertView.setTag(R.id.TAG_ONLINE_ID, vh);
                }
            } else {
                vh = (ViewHolder)convertView.getTag(R.id.TAG_ONLINE_ID);
            }

            if (vh instanceof ThumbnailLineItemViewHolder) {
                populateLineItem(position, (ThumbnailLineItemViewHolder)vh);
            } else if (vh instanceof DetailsLineItemViewHolder) {
                populateLineItem(position, (DetailsLineItemViewHolder)vh);
            }
            return convertView;
        }
    }
    private ProfileDetailsAdapter mAdapter;
    private VideoView mCurrentVideo;
    private ListView mProfileDetails;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = this.getIntent().getExtras();
        Object o = b.getSerializable(Constants.keyProfileData);
        if (o instanceof ProfileData) {
            bindUI((ProfileData)o);
        }
    }


    private void bindUI(ProfileData pd) {
        setContentView(R.layout.a_f_video_profile);
        mCurrentVideo = (VideoView)findViewById(R.id.afvpCurrentVideo);
        mProfileDetails = (ListView)findViewById(R.id.afvpProfileInfo);
        mAdapter = new ProfileDetailsAdapter(this, pd);
        mProfileDetails.setAdapter(mAdapter);

        ProfileVideoData pvd = (ProfileVideoData)mAdapter.getItem(0);
        mCurrentVideo.setVideoURI(pvd.getVideoUri());
        mCurrentVideo.start();

        mProfileDetails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    View longDescription = view.findViewById(R.id.fvpLongDescriptionPanel);
                    if (longDescription.getVisibility() == View.GONE) {
                        longDescription.setVisibility(View.VISIBLE);
                    } else {
                        longDescription.setVisibility(View.GONE);
                    }
                } else {
                    /*ProfilePersonalData.ProfileVideoData pvd = (ProfilePersonalData.ProfileVideoData) mAdapter.getItem(position);
                    mCurrentVideo.stopPlayback();
                    mCurrentVideo.setVideoURI(pvd.getVideoUri());*/

                    new AlertDialog.Builder(VideoProfileActivity.this)
                            .setMessage("Clicking on this shall play the video you clicked on. The feature is disabled in the demo app.")
                            .setTitle("Not available in the demo")
                            .setPositiveButton("OK", null)
                            .show();
                }
            }
        });

    }




}
