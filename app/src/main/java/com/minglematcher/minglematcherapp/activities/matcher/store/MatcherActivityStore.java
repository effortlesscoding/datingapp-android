package com.minglematcher.minglematcherapp.activities.matcher.store;

import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionStoreRegistered;
import com.minglematcher.minglematcherapp.activities.matcher.reducers.MatcherStoreReducer;
import com.minglematcher.minglematcherapp.dispatcher.ActionsDispatcher;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStoreListener;
import com.minglematcher.minglematcherapp.viewmodels.user.User;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 4/16/2016.
 */
public class MatcherActivityStore extends IStore {

//    private WeakReference<IProfilesTabStoreDelegate> mProfileTabStoreDelegate;
//    private WeakReference<IConversationsTabStoreDelegate> mConversationsTabStoreDelegate;


    private UserData mUser;
    private static MatcherActivityStore mInstance;

    public static MatcherActivityStore getInstance() {
        if (mInstance == null) {
            mInstance = new MatcherActivityStore();
        }
        return mInstance;
    }

    private MatcherActivityStore() {
        mUser = User.getCurrentUser();
        ActionsDispatcher.getBus().subscribe(new MatcherStoreReducer(this));
    }

    @Override
    public IStore addListener(IStoreListener listener) {
        mListeners.add(new WeakReference<IStoreListener>(listener));
        listener.onNewState(this, new ActionStoreRegistered());
        return this;
    }

    public UserData getUser() {
        return mUser;
    }

    @Override
    public void onStateReduced(Action action) {
        // No Reducer ?? Ok...
        for (WeakReference<IStoreListener> weakListener : mListeners) {
            IStoreListener listener = weakListener.get();
            listener.onNewState(this, action);
        }
    }
}
