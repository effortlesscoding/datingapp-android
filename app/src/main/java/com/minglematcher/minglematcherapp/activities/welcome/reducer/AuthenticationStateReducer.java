package com.minglematcher.minglematcherapp.activities.welcome.reducer;

import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionStoreRegistered;
import com.minglematcher.minglematcherapp.activities.welcome.actions.ActionLoginDone;
import com.minglematcher.minglematcherapp.activities.welcome.actions.ActionLoginError;
import com.minglematcher.minglematcherapp.activities.welcome.store.AuthenticationStore;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.stores.AStoreReducer;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.viewmodels.user.User;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class AuthenticationStateReducer extends AStoreReducer<AuthenticationStore> {

    public AuthenticationStateReducer(AuthenticationStore store) {
        super(store);
    }

    @Override
    protected void onAction(AuthenticationStore store, Action action) {
        AuthenticationStore s = mStore.get();
        if (s != null) {

            if (action instanceof ActionLoginDone) {
                User.setUser(((ActionLoginDone)action).user);
            } else if (action instanceof ActionLoginError) {

            } else if (action instanceof ActionStoreRegistered) {
                // ??
            }

            s.onStateReduced(action);
        }
    }
}
