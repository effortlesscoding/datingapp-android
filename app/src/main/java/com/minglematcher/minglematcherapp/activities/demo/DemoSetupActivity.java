package com.minglematcher.minglematcherapp.activities.demo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.welcome.WelcomeActivity;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class DemoSetupActivity  extends Activity {

    private Button mMaleButton;
    private Button mFemaleButton;
    private Button mMaleButtonWithPreview;
    private Button mFemaleButtonWithPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        bind();
    }

    /** Other methods **/

    private void inject() {
        setContentView(R.layout.demo_setup);
        mMaleButton = (Button) findViewById(R.id.dsMaleButton);
        mFemaleButton = (Button) findViewById(R.id.dsFemaleButton);
        mFemaleButtonWithPreview = (Button) findViewById(R.id.dsFemaleButtonWithPreview);
        mMaleButtonWithPreview = (Button) findViewById(R.id.dsMaleButtonWithPreview);
    }

    private void bind() {
        mMaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //User.getUser().setGender(EGender.Male, false);
                goToWelcome();
            }
        });

        mMaleButtonWithPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //User.getUser().setGender(EGender.Male, true);
                goToWelcome();
            }
        });

        mFemaleButtonWithPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //User.getUser().setGender(EGender.Female, true);
                goToWelcome();
            }
        });
        mFemaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //User.getUser().setGender(EGender.Female, false);
                goToWelcome();
            }
        });
    }

    private void goToWelcome() {
        Intent i = new Intent(DemoSetupActivity.this, WelcomeActivity.class);
        startActivity(i);
        finish();
    }
}
