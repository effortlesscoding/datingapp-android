package com.minglematcher.minglematcherapp.activities.conversation;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.viewmodels.MatchInteractions;
import com.minglematcher.minglematcherapp.viewmodels.MatchesHistory;
import com.minglematcher.minglematcherapp.viewmodels.SingleInteraction;
import com.minglematcher.minglematcherapp.viewmodels.user.User;
import com.minglematcher.minglematcherapp.utils.BitmapLoader;

import java.lang.ref.WeakReference;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class InteractionActivity extends AppCompatActivity {
    public static final String KeyTalkingTo = "to";
    private MatchInteractions talkingTo;
    private ListView interactionChats;
    private TextView timeLeft;
    private Button interactionSendButton;
    //private EditText interactionInput;
    private ViewGroup interactionEnabledPanel;
    private ViewGroup interactionDisabledPanel;
    private ImageView interactionCenterImage;
    private TextView interactionCenterComment;
    private TextView interactionMatchInstructions;
    private ChatsAdapter adapter;

    private static class ChatsAdapter extends BaseAdapter {
        private MatchInteractions interactions;
        private LayoutInflater mInflater;

        public ChatsAdapter(Context context, MatchInteractions interactions) {
            this.interactions = interactions;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return interactions.getInteractions().size();
        }

        @Override
        public Object getItem(int position) {
            return interactions.getInteractions().get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            SingleInteraction interaction = interactions.getInteractions().get(position);
            if (interaction.isByCurrentUser()) {
                v = mInflater.inflate(R.layout.f_convo_out_item, null);
                ((TextView)v.findViewById(R.id.convoOutChatText))
                        .setText(interaction.getChat());
                ((TextView)v.findViewById(R.id.convoOutChatDate))
                        .setText("Sent " + interaction.getSentTimeAgo() + " ago");
            } else {
                v = mInflater.inflate(R.layout.f_convo_in_item, null);
                ((TextView)v.findViewById(R.id.convoInChatText))
                        .setText(interaction.getChat());
                ((TextView)v.findViewById(R.id.convoInChatDate))
                        .setText("Sent " + interaction.getSentTimeAgo() + " ago");
                SingleInteraction.Mode mode = interactions.getInteractions().get(position).getMode();
                if (mode == SingleInteraction.Mode.Video) {
                    Bitmap bi = BitmapLoader.getImage(interactions.getMatch().getProfilePhoto(), 100, 100);
                    if (bi != null) {
                        int w = bi.getWidth();
                        ((ImageView)v.findViewById(R.id.convoInIcon))
                                .setImageBitmap(bi);
                    }
                } else {
                    ((ImageView)v.findViewById(R.id.convoInPlayOverlay))
                            .setVisibility(View.GONE);
                }
            }
            return v;
        }

        public void addInteraction(SingleInteraction si) {
            interactions.add(si);
            notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshInteractions();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Conversation");
        setContentView(R.layout.a_f_interaction);
        if (inject()) {
            bind();
        }
    }



    private boolean inject() {
        Object sentExtra = (Object)getIntent().getExtras().get(KeyTalkingTo);
        if (sentExtra != null) {
            int id = (int) sentExtra;
            talkingTo = MatchesHistory.getInstance().findInteractionById(id);
            interactionChats = (ListView) findViewById(R.id.interactionChats);
            timeLeft = (TextView) findViewById(R.id.interactionTimeLeft);
            interactionSendButton = (Button) findViewById(R.id.interactionSendButton);
            //interactionInput = (EditText) findViewById(R.id.interactionInput);
            interactionEnabledPanel = (ViewGroup) findViewById(R.id.interactionEnabledPanel);
            interactionDisabledPanel = (ViewGroup) findViewById(R.id.interactionDisabledPanel);
            interactionCenterImage = (ImageView) findViewById(R.id.interactionCenterImage);
            interactionCenterComment =
                    (TextView) findViewById(R.id.interactionMatchComment);
            interactionMatchInstructions =
                    (TextView) findViewById(R.id.interactionMatchInstructions);
            getSupportActionBar().setHomeButtonEnabled(true);
            return true;
        }
        return false;
    }

    private void bind() {
        if (User.getCurrentUser().isFemale() &&
                talkingTo.getState() == MatchInteractions.State.Matched) {
            showInteractionDisabled();
            mockResponseFromMale(0);
        } else {
            showInteractionEnabled();
        }
    }

    private void showInteractionDisabled() {
        interactionDisabledPanel.setVisibility(View.VISIBLE);
        interactionEnabledPanel.setVisibility(View.GONE);
        interactionCenterImage.setImageBitmap(BitmapLoader
                .getImage(talkingTo.getMatch().getProfilePhoto(), 100, 100));
        interactionCenterComment.setText("Matched");
        interactionMatchInstructions.setText("Men have to make the first move");
    }

    private void showInteractionEnabled() {
        interactionDisabledPanel.setVisibility(View.GONE);
        interactionEnabledPanel.setVisibility(View.VISIBLE);

        adapter = new ChatsAdapter(this, talkingTo);
        interactionChats.setAdapter(adapter);
        interactionChats.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent openPreview = new Intent(InteractionActivity.this,
                        InteractionPreviewActivity.class);
                openPreview.putExtra(
                        InteractionPreviewActivity.KeyVideo,
                        talkingTo.getInteractions().get(position)
                );
                startActivity(openPreview);
            }
        });
       /* interactionInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    SingleInteraction si =
                            new SingleInteraction(
                                    true, SingleInteraction.Mode.Text,
                                    interactionInput.getText().toString()
                            );
                    adapter.addInteraction(si);
                    interactionInput.setText("");
                    refreshInteractions();
                }
                return false;
            }
        });*/
        interactionSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Go record a new interaction...
                Intent intent = new Intent(InteractionActivity.this, SnapActivity.class);
                intent.putExtra(InteractionActivity.KeyTalkingTo,
                        talkingTo.getMatch().getFacebookId());
                startActivity(intent);
            }
        });
    }

    private static class ConversationMock implements Runnable {

        private WeakReference<InteractionActivity> activity;
        private int responseNum;

        public ConversationMock(int responseNum, InteractionActivity activity) {
            this.activity = new WeakReference<InteractionActivity>(activity);
            this.responseNum = responseNum;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(5000);
                if (activity != null) {
                    final InteractionActivity ia = activity.get();
                    switch (responseNum) {
                        case 0:
                            ia.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ia.addInteractions(0);
                                }
                            });
                            break;
                        case 1:
                            ia.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ia.addInteractions(1);
                                }
                            });
                            break;
                        case 2:
                            ia.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    ia.addInteractions(2);
                                }
                            });
                            break;
                        default:
                            break;
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void addInteractions(int number) {
        /*if (talkingTo.getMatch().getMockResponse(number) != null) {
            talkingTo.add(new SingleInteraction(false, true,
                    talkingTo.getMatch().getMockResponse((number))));
            showInteractionEnabled();
            refreshInteractions();
        }*/
    }

    private void mockResponseFromFemale(int i) {
        Log.e("MOCK INTERACTION", "Started mocking interactions");
        new Thread(new ConversationMock(i, this)).start();
    }

    // Mocks female interaction
    private void mockResponseFromMale(int i) {
        Log.e("MOCK INTERACTION", "Started mocking interactions");
        new Thread(new ConversationMock(i, this)).start();
    }

    private void refreshInteractions() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
            if (User.getCurrentUser().isFemale()) {
                if (adapter.getCount() == 2) {
                    mockResponseFromMale(1);
                } else if (adapter.getCount() == 4) {
                    mockResponseFromMale(2);
                }
            } else {
                if (adapter.getCount() == 1) {
                    mockResponseFromFemale(0);
                } else if (adapter.getCount() == 3) {
                    mockResponseFromFemale(1);
                }
            }
        }

    }

}
