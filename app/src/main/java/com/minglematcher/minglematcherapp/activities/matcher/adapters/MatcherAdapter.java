package com.minglematcher.minglematcherapp.activities.matcher.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.minglematcher.minglematcherapp.activities.matcher.MatcherActivity;
import com.minglematcher.minglematcherapp.activities.matcher.tabs.ConversationsTabFragment;
import com.minglematcher.minglematcherapp.activities.matcher.tabs.ProfilesReviewsTabFragment;

import java.lang.ref.WeakReference;


/**
 * Created by AnhAcc on 1/31/2016.
 */
public class MatcherAdapter extends FragmentStatePagerAdapter {

    private WeakReference<MatcherActivity> mParent;

    public MatcherAdapter(MatcherActivity parent) {
        super(parent.getSupportFragmentManager());
        mParent = new WeakReference<MatcherActivity>(parent);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            ProfilesReviewsTabFragment fragment = new ProfilesReviewsTabFragment();
            fragment.setParent(mParent);
            return fragment;
        } else {
            return new ConversationsTabFragment();
        }
    }
}
