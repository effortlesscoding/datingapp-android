package com.minglematcher.minglematcherapp.activities.welcome.store;

import com.minglematcher.minglematcherapp.activities.user.reducers.UserProfileSetupReducer;
import com.minglematcher.minglematcherapp.activities.welcome.reducer.AuthenticationStateReducer;
import com.minglematcher.minglematcherapp.dispatcher.ActionsDispatcher;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStoreListener;

import java.lang.ref.WeakReference;

/**
 * Created by AnhAcc on 5/7/2016.
 */
public class AuthenticationStore extends IStore {

    private static AuthenticationStore mInstance;

    public static AuthenticationStore getInstance() {
        if (mInstance == null) {
            mInstance = new AuthenticationStore();
        }
        return mInstance;
    };

    private AuthenticationStore() {
        ActionsDispatcher.getBus().subscribe(new AuthenticationStateReducer(this));
    }

    @Override
    public void onStateReduced(Action action) {
        for (WeakReference<IStoreListener> listener : mListeners) {
            IStoreListener list = listener.get();
            if (list != null) {
                list.onNewState(this, action);
            }
        }
    }

}
