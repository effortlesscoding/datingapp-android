package com.minglematcher.minglematcherapp.activities.user;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.user.actionscreator.UserProfileActionsCreator;
import com.minglematcher.minglematcherapp.activities.user.helpers.ERecordState;
import com.minglematcher.minglematcherapp.activities.user.helpers.IRecordingListener;
import com.minglematcher.minglematcherapp.activities.user.helpers.CamVideoRecorder;
import com.minglematcher.minglematcherapp.activities.user.helpers.RecordingOptions;
import com.minglematcher.minglematcherapp.activities.user.ui.UserProfileTabAdapter;
import com.minglematcher.minglematcherapp.utils.BitmapLoader;

import java.io.File;

/**
 * Created by AnhAcc on 2/15/2016.
 */
public class UserProfileVideoRecordingActivity extends Activity implements IRecordingListener {

    private CamVideoRecorder mRecorder;
    private SurfaceView mSurfaceView;

    @Override
    public void recordingStarted() {
        mSecondsLeft = SECONDS_START;
        mTimerHandler = new Handler();
        mTimerHandler.postDelayed(mTimerRunnable, 0);
    }

    @Override
    public void recordingResumed() {
    }

    private class StartRecordingListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switchToRecordState(ERecordState.Recording);
        }
    }

    private class PauseRecordingListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switchToRecordState(ERecordState.Paused);
        }
    }

    private class RetryRecordingListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switchToRecordState(ERecordState.OnStandByReady);
        }
    }

    private class AcceptRecordingListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "Accept tapped!");
            releaseAll();
            try {
                File thumbnailFile = BitmapLoader.saveThumbnailForVideo(mVideoFile);
                new UserProfileActionsCreator(mVideoId).uploadProfileVideo(mVideoFile, thumbnailFile);
                finish();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }


    private class RecordTimerRunnable implements Runnable {
        @Override
        public void run() {
            Log.i(TAG, "Timer runnable is still running!");
            ERecordState state = mRecorder.getRecordState();
            if (state != ERecordState.Paused && state != ERecordState.OnStandByReady) {
                mSecondsLeft--;
            }
            mCountdown.setText(String.valueOf(mSecondsLeft));
            if (mSecondsLeft > 0) {
                mTimerHandler.postDelayed(this, 1000);
            } else if (state != ERecordState.Recorded){
                switchToRecordState(ERecordState.Recorded);
            }
        }
    }
    /** Member fields **/

    private String TAG = "VIDEO_PROFILE";

    // Start / Pause / Approve
    private Button mActionButton;

    // Restart / Cancel
    private Button mRetryButton;
    private Handler mTimerHandler;
    private File mVideoFile;
    private int mSecondsLeft;
    private final int SECONDS_START = 15;
    private ViewGroup mTipsPanel;
    private TextView mTips;
    private ViewGroup mCountdownPanel;
    private TextView mCountdown;
    private Runnable mTimerRunnable;

    private StartRecordingListener mStartRecordingHandler;
    private RetryRecordingListener mRetryRecordingHandler;
    private AcceptRecordingListener mAcceptRecordingHandler;
    private PauseRecordingListener mPauseRecordingHandler;
    private Long mVideoId;
    /** Activity lifecycle overrides **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mVideoId = extras.getLong(UserProfileTabAdapter.VIDEO_ID_KEY);
        } else {
            Log.e("PROFILE_VIDEO", "INVALID TAGS!");
        }
        mTimerRunnable = new RecordTimerRunnable();
        setContentView(R.layout.user_profile_video);
        mRetryRecordingHandler = new RetryRecordingListener();
        mStartRecordingHandler = new StartRecordingListener();
        mAcceptRecordingHandler = new AcceptRecordingListener();
        mPauseRecordingHandler = new PauseRecordingListener();
        bindUI();
        mVideoFile = getNextFileName();

        if (mSurfaceView != null) {
            mSurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    mRecorder = new CamVideoRecorder(UserProfileVideoRecordingActivity.this, mSurfaceView,
                            new RecordingOptions(mVideoFile, 15));
                    mRecorder.startPreview();
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    mRecorder.release();
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseAll();
    }

    /** Other methods **/
    private void bindUI() {
        setContentView(R.layout.user_profile_video);
        mActionButton = (Button) findViewById(R.id.pvActionButton);
        mRetryButton = (Button) findViewById(R.id.pvRetryButton);
        mSurfaceView = (SurfaceView) findViewById(R.id.pvVideoPreview);
        mCountdownPanel = (ViewGroup) findViewById(R.id.pvCountdownPanel);
        mCountdown = (TextView) findViewById(R.id.pvCountdown);
        mTipsPanel = (ViewGroup) findViewById(R.id.pvTipsPanel);
        mTips = (TextView) findViewById(R.id.pvTips);
        mActionButton.setOnClickListener(mStartRecordingHandler);
        mRetryButton.setOnClickListener(mRetryRecordingHandler);
    }


    public void switchToRecordState(ERecordState newState) {
        Log.i(TAG, newState.toString());
        mRecorder.switchToRecordState(newState);
        switch (newState) {
            case OnStandByReady:
                mActionButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle));
                mActionButton.setOnClickListener(mStartRecordingHandler);
                mRetryButton.setVisibility(View.GONE);
                mSecondsLeft = SECONDS_START;
                mCountdown.setText(String.valueOf(mSecondsLeft));
                if (mTimerHandler != null) {
                    mTimerHandler.removeCallbacksAndMessages(null);
                }
                mTimerHandler = null;
                break;
            case Recording:
                mRetryButton.setVisibility(View.VISIBLE);
                mTipsPanel.setVisibility(View.GONE);
                mCountdownPanel.setVisibility(View.VISIBLE);
                mActionButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_pause_over_video));
                mRetryButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_replay));
                mActionButton.setOnClickListener(mPauseRecordingHandler);
                break;
            case Paused:
                mActionButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle));
                mRetryButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_replay));
                mActionButton.setOnClickListener(mStartRecordingHandler);
                break;
            case Recorded:
                mActionButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_accept));
                mRetryButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_cancel));
                mCountdownPanel.setVisibility(View.GONE);
                mTips.setText("Congratulations! You have recorded your profile! Tap on 'accept' icon before you can see singles near you.");
                mTipsPanel.setVisibility(View.VISIBLE);
                mActionButton.setOnClickListener(mAcceptRecordingHandler);
                break;
        }
    }

    /***
     * Releases all handlers like timers and MediaRecorder
     * because these need manual memory release.
     */
    private void releaseAll() {
        if (mTimerHandler != null) {
            Log.d(TAG, "Handler destruction.");
            mTimerHandler.removeCallbacksAndMessages(null);
            mTimerHandler = null;
        }
        Log.d(TAG, "Camera destruction.");
        mRecorder.release();
    }

    private File getNextFileName() {
        String path = Environment.getExternalStorageDirectory() + File.separator + "dating";
        String fileName = File.separator + "reply_" + System.currentTimeMillis() + ".mp4";
        File dirPath = new File(path);
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }
        File file = new File(path + File.separator + fileName);
        Log.d(TAG, "FileName : " + file.toString() + " --- " + file.getPath());
        return file;
    }

}

