package com.minglematcher.minglematcherapp.activities.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.conversation.InteractionActivity;
import com.minglematcher.minglematcherapp.activities.conversation.SnapActivity;
import com.minglematcher.minglematcherapp.viewmodels.MatchInteractions;
import com.minglematcher.minglematcherapp.viewmodels.user.User;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class HistoryTabFragment extends Fragment {

    private List<MatchInteractions> interactions;
    private WeakReference<Context> context;
    private boolean isMatch;
    public HistoryTabFragment() {
        super();
    }

    public static HistoryTabFragment newInstance(WeakReference<Context> context,
                                                 List<MatchInteractions> interactions,
                                                 boolean isMatch) {
        HistoryTabFragment f = new HistoryTabFragment();
        f.setInteractions(interactions);
        f.setContext(context);
        f.setIsMatch(isMatch);
        return f;
    }

    public void setContext(WeakReference<Context> context) {
        this.context = context;
    }

    public void setInteractions(List<MatchInteractions> interactions) {
        this.interactions = interactions;
    }

    public void setIsMatch(boolean isMatch) {
        this.isMatch = isMatch;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_history_listview, container, false);
        ListView list = (ListView)v.findViewById(R.id.fHistoryListView);
        HistoryArrayAdapter adapter = new HistoryArrayAdapter(context, this.interactions);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isMatch && context != null) {
                    Context c = context.get();
                    if (c != null) {
                        MatchInteractions.State s = interactions.get(position).getState();
                        if (s == MatchInteractions.State.Matched) {
                            if (User.getCurrentUser().isFemale()) {
                                showInteraction(c, position);
                            } else {
                                // A man has to make the first move...
                                showSayHello(c, position);
                            }
                        } else if (s != MatchInteractions.State.Liked) {
                            showInteraction(c, position);
                        }
                    }
                }
            }
        });
        return v;
    }

    private void showInteraction(final Context context, final int position) {
        Intent openConversation = new Intent(context, InteractionActivity.class);
        openConversation.putExtra(InteractionActivity.KeyTalkingTo,
                interactions.get(position).getMatch().getFacebookId());
        startActivity(openConversation);
    }

    private void showSayHello(final Context context, final int position) {
        Intent intent = new Intent(context, SnapActivity.class);
        intent.putExtra(InteractionActivity.KeyTalkingTo,
                interactions.get(position).getMatch().getFacebookId());
        startActivity(intent);
    }
}