package com.minglematcher.minglematcherapp.activities.matcher.tabs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionLikeProfileDone;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionVideoDownloadDone;
import com.minglematcher.minglematcherapp.activities.matcher.actionscreators.MatcherActionsCreator;
import com.minglematcher.minglematcherapp.activities.matcher.actions.ActionStoreRegistered;
import com.minglematcher.minglematcherapp.activities.matcher.store.MatcherActivityStore;
import com.minglematcher.minglematcherapp.activities.matcher.MatcherActivity;
import com.minglematcher.minglematcherapp.activities.profile.VideoProfileActivity;
import com.minglematcher.minglematcherapp.activities.user.UserProfileSetupActivity;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStoreListener;
import com.minglematcher.minglematcherapp.viewmodels.user.ProfileSnippetData;
import com.minglematcher.minglematcherapp.viewmodels.user.User;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;
import com.minglematcher.minglematcherapp.services.parameters.VideoDownloadParams;
import com.minglematcher.minglematcherapp.ui.RoundedTransformation;
import com.minglematcher.minglematcherapp.ui.swiper.FlingAdapter;
import com.minglematcher.minglematcherapp.ui.swiper.SwipeFlingAdapterView;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnhAcc on 2/1/2016.
 */
public class ProfilesReviewsTabFragment extends Fragment implements IStoreListener {

    private class GoToProfileDetailsListener implements SwipeFlingAdapterView.OnItemClickListener {

        @Override
        public void onItemClicked(int itemPosition, Object dataObject) {
            Intent goProfileDetails = new Intent(getContext(), VideoProfileActivity.class);
            Bundle b = new Bundle();
            //b.putSerializable(Constants.keyProfileData, mProfiles.get(0));
            //goProfileDetails.putExtras(b);
            //startActivity(goProfileDetails);
        }
    }

    private class ChatWithMatchClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            MatcherActivity mActivity = mParentActivity.get();
            if (mActivity != null) {
                mActivity.goConversations();
            }
        }
    }

    private class CloseMatchPopupClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            mMatchBackground.setVisibility(View.GONE);
        }
    }

    private class CardSwingListener implements SwipeFlingAdapterView.onFlingListener {

        @Override
        public void onCardExited() {
            Log.i("PROFILES", "Card exited()");
        }

        @Override
        public void onLeftCardExit(Object dataObject) {
            Log.i("PROFILES", "Left Card exited()");
            if (dataObject instanceof FlingAdapter.FlingAdapterViewHolder) {
                FlingAdapter.FlingAdapterViewHolder vh = (FlingAdapter.FlingAdapterViewHolder) dataObject;
                ProfileSnippetData pd = vh.getModel();
                if (pd != null) {
                    mActionsCreator.passProfile(pd);
                }
            }
        }

        @Override
        public void onRightCardExit(Object dataObject) {
            Log.i("PROFILES", "Right Card exited()");
            if (User.getCurrentUser().isProfileComplete()) {
                if (dataObject instanceof FlingAdapter.FlingAdapterViewHolder) {
                    FlingAdapter.FlingAdapterViewHolder vh = (FlingAdapter.FlingAdapterViewHolder) dataObject;
                    if (vh != null) {
                        ProfileSnippetData pd = vh.getModel();
                        if (pd != null) {
                            mActionsCreator.likeProfile(pd);
                        }
                    }
                }
            } else {
                askToCreateProfile();
            }

        }

        private void askToCreateProfile() {
            // Ask to create a profile
            CharSequence choices[] = new CharSequence[] {"Create a profile", "Continue Browsing"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Create a profile to increase your chances of being liked");
            builder.setItems(choices, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0:
                            Intent goProfile = new Intent(getContext(), UserProfileSetupActivity.class);
                            goProfile.putExtra(ProfilesReviewsTabFragment.PROFILE_FILLED_REQUEST_KEY, true);
                            startActivity(goProfile);
                            break;
                        case 1:
                            break;
                    }
                }
            });
            builder.show();
        }

        @Override
        public void onAdapterAboutToEmpty(int itemsInAdapter) {
            // Ask for more data here
            // TO_FIX : THIS IS NOT WORKING AT ALL!
            synchronized (mDownloadedVideosBuffer) {
                if (mAdapter.getCount() == 0) {
                    mLoadingPanel.setVisibility(View.VISIBLE);
                }
            }
            Log.d("LIST", "notified");
        }

        @Override
        public void onScroll(float scrollProgressPercent) {
            View view = mSwipeView.getSelectedView();
            view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
            view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
        }
    };

    public static String PROFILE_FILLED_REQUEST_KEY = "13";
    public static int PROFILE_FILLED_RESULT_KEY = 13;
    private WeakReference<MatcherActivity> mParentActivity;
    private ImageView mYourPhoto;
    private SwipeFlingAdapterView mSwipeView;
    private ImageView mMatchPhoto;
    private FlingAdapter mAdapter;
    private List<ProfileSnippetData> mProfiles;
    private ViewGroup mMatchBackground;
    private Button mMatchAcceptButton;
    private ViewGroup mLoadingPanel;
    private ArrayList<ProfileSnippetData> mDownloadedVideosBuffer;
    private MatcherActionsCreator mActionsCreator = new MatcherActionsCreator();

    public ProfilesReviewsTabFragment() {
    }

    public void setParent(WeakReference<MatcherActivity> parentActivity) {
        mParentActivity = parentActivity;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("TabFragment", "Destroy profiles tab view");
        // Free images
    }

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Check how many profile videos we have pre-downloaded
        // Put the remaining ones in the queue of downloads.
    }

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.matcher_fragment_profiles, container, false);
        mLoadingPanel = (ViewGroup)v.findViewById(R.id.mafpLoadingPanel);
        mSwipeView = (SwipeFlingAdapterView)v.findViewById(R.id.mfpSwipeView);
        mMatchPhoto = (ImageView)v.findViewById(R.id.mfpMatchPhoto);
        mYourPhoto = (ImageView)v.findViewById(R.id.mvpYourPhoto);
        mMatchAcceptButton = (Button)v.findViewById(R.id.mafpMatchButton);
        mProfiles =  User.getCurrentUser().getProfilesToReview();
        mDownloadedVideosBuffer = new ArrayList<>();

        Picasso.with(getContext())
                .load("file:///android_asset/female_user.jpg")
                .transform(new RoundedTransformation())
                .into(mYourPhoto);
        mMatchBackground = (ViewGroup) v.findViewById(R.id.mafpMatchBackground);
        mMatchAcceptButton.setOnClickListener(new ChatWithMatchClickListener());
        mSwipeView.setOnItemClickListener(new GoToProfileDetailsListener());
        mSwipeView.setFlingListener(new CardSwingListener());
        mMatchBackground.setOnClickListener(new CloseMatchPopupClickListener());
        mAdapter = new FlingAdapter(getContext(), R.layout.f_matcher_profile);
        mSwipeView.setAdapter(mAdapter);

        MatcherActivityStore.getInstance().addListener(this);
        return v;
    }

    @Override
    public void onNewState(IStore store, Action action) {

        if (action instanceof ActionStoreRegistered) {
            bindToCurrentStoreState();
        } else if (action instanceof ActionLikeProfileDone) {
            ActionLikeProfileDone aLike = (ActionLikeProfileDone) action;
            if (aLike.isMutual()) {
                // Show a MATCH popup. (ONLY if the activity is not "hibernated ?!"
                mMatchBackground.setVisibility(View.VISIBLE);
                /*File thumbnail = aLike.getProfile().get;
                if (thumbnail != null) {
                    Picasso.with(getContext())
                            .load(thumbnail)
                            .transform(new RoundedTransformation())
                            .into(mMatchPhoto);
                }*/
            }
        } else if (action instanceof ActionVideoDownloadDone) {
            ActionVideoDownloadDone vdAction = (ActionVideoDownloadDone)action;
            showDownloadedProfileVideo(vdAction.getParams());
        } else {
            Log.e("PROFILES_TAB_FRAGMENT", action.getClass() + " Unhandled action.");
        }
    }

    private void bindToCurrentStoreState() {
        // Check if any profiles are available for preview
        UserData user = MatcherActivityStore.getInstance().getUser();

        List<ProfileSnippetData> profilesToReview = user.getProfilesToReview();
        ArrayList<ProfileSnippetData> availableProfiles = new ArrayList<>();
        ArrayList<ProfileSnippetData> videosToDownload = new ArrayList<>();
        for (int i = 0; i < profilesToReview.size(); i++) {
            ProfileSnippetData psd = profilesToReview.get(i);
            if (psd.isVideoLocallyDownloaded(getContext())) {
                availableProfiles.add(psd);
            } else {
                videosToDownload.add(psd);
            }
        }
        if (availableProfiles.size() > 0) {
            // Bind them
            showProfiles(availableProfiles);
        }
        if (videosToDownload.size() > 0) {
            mActionsCreator.downloadProfileVideos(videosToDownload);
        }
    }

    private void showProfiles(List<ProfileSnippetData> profiles) {
        for (ProfileSnippetData psd : profiles) {
            mAdapter.addItem(psd);
        }
        mAdapter.notifyDataSetChanged();
        mLoadingPanel.setVisibility(View.GONE);
        mSwipeView.setVisibility(View.VISIBLE);
    }


    private void showDownloadedProfileVideo(VideoDownloadParams params) {
        ProfileSnippetData psd = params.getProfileSnippetData();
        if (psd != null) {
            synchronized (mDownloadedVideosBuffer) {
                mLoadingPanel.setVisibility(View.GONE);
                //if (mAdapter.isEmpty()) {
                    // Add + notify
                mAdapter.addItem(psd);
                mAdapter.notifyDataSetChanged();
                //} else {
                //    mDownloadedVideosBuffer.add(psd);
                //}
            }
        }
    }

    private void recycleMainImage() {
        Log.i("MATCH", "Recycling the match photo.");
        Drawable imageDrawable = mMatchPhoto.getDrawable();
        mMatchPhoto.setImageDrawable(null); //this is necessary to prevent getting Canvas: can not draw recycled bitmap exception

        if (imageDrawable!=null && imageDrawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = ((BitmapDrawable) imageDrawable);
            Bitmap bi = bitmapDrawable.getBitmap();
            if (bi != null && !bi.isRecycled()) {
                bi.recycle();
            }
        }
    }

    private void showToast(String text) {
        Toast toast = Toast.makeText(getContext(), text, Toast.LENGTH_LONG);
        toast.show();
    }

}