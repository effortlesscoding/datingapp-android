package com.minglematcher.minglematcherapp.activities.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.viewmodels.MatchInteractions;
import com.minglematcher.minglematcherapp.utils.BitmapLoader;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class HistoryArrayAdapter extends BaseAdapter {

    public static class ViewHolder {
        TextView firstLine;
        TextView secondLine;
        ImageView photo;
    }

    private List<MatchInteractions> interactions;
    private WeakReference<Context> context;

    public HistoryArrayAdapter(WeakReference<Context> context, List<MatchInteractions> conversations) {
        this.interactions = conversations;
        this.context = context;
    }

    @Override
    public int getCount() {
        return interactions.size();
    }

    @Override
    public Object getItem(int position) {
        return interactions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (context == null || context.get() == null) { return convertView; }

        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater)context.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE );
            holder = new ViewHolder();
            rowView = inflater.inflate(R.layout.f_history_item, parent, false);
            holder.firstLine = (TextView) rowView.findViewById(R.id.firstLine);
            holder.secondLine = (TextView) rowView.findViewById(R.id.secondLine);
            holder.photo = (ImageView) rowView.findViewById(R.id.icon);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder)rowView.getTag();
        }
        MatchInteractions data = interactions.get(position);
        holder.firstLine.setText(data.getMatch().getProfile().getName());
        holder.secondLine.setText("Mingled with this match on some other date ... >.>");
        holder.photo.setImageBitmap(BitmapLoader.getImage(data.getMatch().getProfilePhoto(),
                100, 100));
        return rowView;
    }

}
