package com.minglematcher.minglematcherapp.activities.user.actions;

import android.graphics.Bitmap;

import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.actions.ActionUpload;
import com.minglematcher.minglematcherapp.dispatcher.actions.EUploadType;

import java.io.File;

/**
 * Created by AnhAcc on 4/24/2016.
 */
public class ActionVideoUploadingStarted extends ActionUpload {

    public File videoFile;
    public File thumbnailFile;

    public ActionVideoUploadingStarted(long id, File videoFile, File thumbnailFile) {
        super(EUploadType.PROFILE_VIDEO, id);
        this.videoFile = videoFile;
        this.thumbnailFile = thumbnailFile;
    }
}
