package com.minglematcher.minglematcherapp.activities.instructions;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.matcher.MatcherActivity;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by AnhAcc on 2/21/2016.
 */
public class InstructionsActivity extends AppCompatActivity {
    private final String TAG = "mPageTag";
    public class InstructionsPagerAdapter extends PagerAdapter {
        private int mScreenWidth;
        private int mScreenHeight;
        public InstructionsPagerAdapter() {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            mScreenWidth = size.x;
            mScreenHeight = size.y;
            Log.i("INSTRUCTIONS", "Screen size is : " + mScreenWidth + " x " + mScreenHeight);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            Log.i("INSTRUCTIONS", "Belongs : " + ((object instanceof RelativeLayout) && (view == ((RelativeLayout) object))) );
            return (object instanceof RelativeLayout) && (view == ((RelativeLayout) object));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) InstructionsActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.f_instruction, container,
                    false);
            TextView text = (TextView)itemView.findViewById(R.id.fiInstructionText);
            ImageView image = (ImageView)itemView.findViewById(R.id.fiInstruction);
            switch (position) {
                case 0:
                    text.setText(R.string.instruction_one);
                    break;
                case 1:
                    image.setRotation(20.0f);
                    image.setTranslationX(mScreenWidth / 3);
                    ImageView arrowRight = (ImageView)itemView.findViewById(R.id.fiArrowRight);
                    arrowRight.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                            R.drawable.arrow_direction_right, null));
                    text.setText(R.string.instruction_two);
                    itemView.setAlpha(0.0f);
                    break;
                case 2:
                    image.setRotation(-20.0f);
                    image.setTranslationX(-mScreenWidth / 3);
                    ImageView arrowLeft = (ImageView)itemView.findViewById(R.id.fiArrowLeft);
                    arrowLeft.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                            R.drawable.arrow_direction_left, null));
                    text.setText(R.string.instruction_three);
                    itemView.setAlpha(0.0f);
                    break;
                case 3:
                    image.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                            R.drawable.conversation_art, null));
                    text.setText(R.string.instruction_four);
                    itemView.setAlpha(0.0f);
                    break;
            }
            ((ViewPager) container).addView(itemView);
            itemView.setTag(TAG + position);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ViewGroup) object);
            Log.i("MATCH", "Recycling the match photo.");
        }
    }

    private Button mActionButton;
    private ViewPager mPager;
    private CirclePageIndicator mIndicator;
    private InstructionsPagerAdapter mAdapter;
    private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            View view = mPager.findViewWithTag(TAG + position);
            if (view != null) {
                Log.i(TAG, "Started animation" + view.getAlpha());
                view.animate().alpha(1.0f).setDuration(500);
                if (position > 2) {
                    mInstructionsInstructions.animate().alpha(0.0f).setDuration(250);
                }
                if (position == 3) {
                    mActionButton.animate().scaleY(1.0f).setDuration(500).setStartDelay(250);
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    private TextView mInstructionsInstructions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_f_instructions);
        mPager = (ViewPager)findViewById(R.id.iInstructionsViewPager);
        mPager.addOnPageChangeListener(mPageChangeListener);
        mIndicator = (CirclePageIndicator) findViewById(R.id.iInstructionsPagination);
        mActionButton = (Button) findViewById(R.id.iInstructionsActionButton);
        mActionButton.setScaleY(0.0f);
        mInstructionsInstructions = (TextView) findViewById(R.id.iInstructionsInstruction);
        mAdapter = new InstructionsPagerAdapter();
        mPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mPager);
        mActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InstructionsActivity.this, MatcherActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        mPager.removeOnPageChangeListener(mPageChangeListener);
        super.onDestroy();
    }

}
