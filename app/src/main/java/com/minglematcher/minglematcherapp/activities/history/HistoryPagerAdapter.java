package com.minglematcher.minglematcherapp.activities.history;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.minglematcher.minglematcherapp.viewmodels.MatchInteractions;
import com.minglematcher.minglematcherapp.viewmodels.MatchesHistory;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class HistoryPagerAdapter extends FragmentStatePagerAdapter {

    private WeakReference<Context> mContext;

    public HistoryPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = new WeakReference<Context>(context);
    }

    @Override
    public Fragment getItem(int position) {
        List<MatchInteractions> history = MatchesHistory.getInstance().getActivityHistory();
        if (position == 0) {
            // Get the MATCHES
            return HistoryTabFragment.newInstance(mContext,
                    history,
                    true);
        } else {
            // Get the "Activity History"
            return HistoryTabFragment.newInstance(mContext,
                    history,
                    false);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
