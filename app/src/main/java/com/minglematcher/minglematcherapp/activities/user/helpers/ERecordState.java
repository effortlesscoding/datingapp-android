package com.minglematcher.minglematcherapp.activities.user.helpers;

/**
 * Created by AnhAcc on 3/26/2016.
 */
public enum ERecordState {
    OnStandByReady, Recording, Paused, Recorded
}
