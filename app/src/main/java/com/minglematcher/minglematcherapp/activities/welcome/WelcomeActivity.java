package com.minglematcher.minglematcherapp.activities.welcome;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.minglematcher.minglematcherapp.Constants;
import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.activities.instructions.InstructionsActivity;
import com.minglematcher.minglematcherapp.activities.matcher.MatcherActivity;
import com.minglematcher.minglematcherapp.activities.welcome.actions.ActionLoginDone;
import com.minglematcher.minglematcherapp.activities.welcome.actions.ActionLoginError;
import com.minglematcher.minglematcherapp.activities.welcome.actionscreators.ACLogin;
import com.minglematcher.minglematcherapp.activities.welcome.store.AuthenticationStore;
import com.minglematcher.minglematcherapp.dispatcher.actions.Action;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStore;
import com.minglematcher.minglematcherapp.dispatcher.stores.IStoreListener;
import com.minglematcher.minglematcherapp.viewmodels.user.User;
import com.minglematcher.minglematcherapp.services.VideoDownloadService;
import com.minglematcher.minglematcherapp.services.responses.GetProfilesInfoResponse;
import com.minglematcher.minglematcherapp.services.responses.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.Arrays;
import java.util.List;

import com.minglematcher.minglematcherapp.services.ServerApiService;
import com.minglematcher.minglematcherapp.viewmodels.user.UserData;


/**
 * Created by AnhAcc on 12/14/2015.
 */
public class WelcomeActivity extends AppCompatActivity implements IStoreListener {

    @Override
    public void onNewState(IStore store, final Action action) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (action instanceof ActionLoginError) {
                    mPd.hide();
                } else if ( action instanceof ActionLoginDone) {
                    mPd.hide();
                    Intent newActivity = new Intent(WelcomeActivity.this, MatcherActivity.class);
                    startActivity(newActivity);
                    finish();
                }
            }
        });
    }

    /**
     * TO_DO:
     * Add a check that login has already been done and that token is stored in
     * SharedSettings
     *
     */

    private CallbackManager mCallbackManager;
    private FacebookCallback<LoginResult> mFacebookCallback;

    private List<String> mPermissions = Arrays.asList("email");
    private LoginManager mLoginMgr;

    private LoginResult mLoginResult;
    private ImageView mImageView;
    private Button mSignInButton;
    private ProgressDialog mPd;

    private void showAlert(String title, String message) {
        new AlertDialog.Builder(WelcomeActivity.this)
                .setMessage(title)
                .setTitle(message)
                .setPositiveButton("OK", null)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode,
                resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPd != null) {
            mPd.dismiss();
            mPd = null;
        }
        AuthenticationStore.getInstance().removeListener(this);
        recycleMainImage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AuthenticationStore.getInstance().addListener(this);
        AppEventsLogger.activateApp(this);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }

        inject();
        bind();
    }

    /** Other methods **/

    private void initFacebookLogin() {
        mFacebookCallback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.v("LoginActivity login", loginResult.toString());
                mPd = new ProgressDialog(WelcomeActivity.this);
                mPd.setMax(100);
                mPd.setTitle("Signing in with Facebook");
                mPd.setMessage("Please wait while we sign you in with Facebook...");
                mPd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mPd.setIndeterminate(true);
                mPd.setCancelable(false);
                mPd.show();
                mLoginResult = loginResult;
                new ACLogin(loginResult.getAccessToken().getToken()).login();
            }
            @Override
            public void onCancel() {
                Log.e("LoginActivity", "facebook login canceled");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e("LoginActivity", "facebook login failed error");
                Log.e("LoginActivity", e.toString());
            }
        };
        mCallbackManager = CallbackManager.Factory.create();
        mLoginMgr = LoginManager.getInstance();
        mLoginMgr.registerCallback(mCallbackManager, mFacebookCallback);
    }

    private void inject() {
        setContentView(R.layout.welcome);
        mSignInButton = (Button) findViewById(R.id.wSignInButton);
        mImageView = (ImageView) findViewById(R.id.wMainImage);
        initFacebookLogin();
    }

    private void bind() {
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginMgr.logInWithReadPermissions(WelcomeActivity.this, mPermissions);
            }
        });
    }

    private void recycleMainImage() {
        Drawable imageDrawable = mImageView.getDrawable();
        mImageView.setImageDrawable(null); //this is necessary to prevent getting Canvas: can not draw recycled bitmap exception

        if (imageDrawable!=null && imageDrawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = ((BitmapDrawable) imageDrawable);

            if (!bitmapDrawable.getBitmap().isRecycled()) {
                bitmapDrawable.getBitmap().recycle();
            }
        }
    }

    /** Api Call Callbacks **/
}
