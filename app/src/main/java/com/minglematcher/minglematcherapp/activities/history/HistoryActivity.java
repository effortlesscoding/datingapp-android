package com.minglematcher.minglematcherapp.activities.history;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import com.minglematcher.minglematcherapp.R;
import com.minglematcher.minglematcherapp.ui.ActionBarActivity;
import com.minglematcher.minglematcherapp.ui.NonSwipeableViewPager;

/**
 * Created by AnhAcc on 2/19/2016.
 */
public class HistoryActivity extends ActionBarActivity {

    private NonSwipeableViewPager tabsPager;
    private Button activityModeButton;
    private Button matcherModeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Match Interactions");
        setContentView(R.layout.a_f_matches_history);
        inject();
        bind();
    }

    private void inject() {
        tabsPager = (NonSwipeableViewPager)findViewById(R.id.historyTabsPager);
        activityModeButton = (Button) findViewById(R.id.historyActivityButton);
        matcherModeButton = (Button) findViewById(R.id.historyMatchesButton);
    }

    private void bind() {
        HistoryPagerAdapter adapter = new HistoryPagerAdapter(getSupportFragmentManager(), this);
        tabsPager.setAdapter(adapter);
        activityModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabsPager.setCurrentItem(1);
                activityModeButton.setBackgroundColor(ContextCompat.getColor(HistoryActivity.this,
                        R.color.colorPrimary));
                matcherModeButton.setBackgroundColor(ContextCompat.getColor(HistoryActivity.this,
                        R.color.colorPrimaryDark));
            }
        });

        matcherModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabsPager.setCurrentItem(0);
                matcherModeButton.setBackgroundColor(ContextCompat.getColor(HistoryActivity.this,
                        R.color.colorPrimary));
                activityModeButton.setBackgroundColor(ContextCompat.getColor(HistoryActivity.this,
                        R.color.colorPrimaryDark));
            }
        });
    }
}